package ca.ulaval.glo2004;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Metric;
import ca.ulaval.glo2004.domain.measure.Pixels;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MeasureTests {

    private final Imperial imperial = Imperial.get();
    private final Metric metric = Metric.get();
    private final Pixels pixels = Pixels.get();

    ///region Imperial

    @Test
    public void doubleToImperial() {
        double initial = 50.7;
        String expected = "50 45/64";
        String value = imperial.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void doubleToImperialButNoFraction() {
        double initial = 34;
        String expected = "34";
        String value = imperial.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void negativeDoubleToImperial() {
        double initial = -16.75;
        String expected = "-16 3/4";
        String value = imperial.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void onlyFractionDoubleToImperial() {
        double initial = 0.75;
        String expected = "3/4";
        String value = imperial.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void negativeOnlyFractionDoubleToImperial() {
        double initial = -0.75;
        String expected = "-3/4";
        String value = imperial.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void imperialToDouble() {
        String initial = "12 2/8";
        double expected = 12.25;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void zeroImperialToDouble() {
        String initial = "0 2/8";
        double expected = 0.25;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void onlyFractionImperialToDouble() {
        String initial = "7/64";
        double expected = 0.109375;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void onlyFractionNumeratorDivisorImperialToDouble() {
        String initial = "7/";
        double expected = 0;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void negativeOnlyFractionImperialToDouble() {
        String initial = "-6/12";
        double expected = -0.5;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void imperialWithoutFractionToDouble() {
        String initial = "12";
        double expected = 12;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void negativeImperialToDouble() {
        String initial = "-12 1/2";
        double expected = -12.5;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void noDenominatorImperialFractionToDouble() {
        String initial = "12 1/";
        double expected = 12;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void noDivisorImperialFractionToDouble() {
        String initial = "12 1";
        double expected = 12;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void noFractionImperialFractionToDouble() {
        String initial = "12 ";
        double expected = 12;

        Double value = imperial.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    @Test
    public void decimalToImperial() {
        String initial = "1.9";
        double expected = 1.9;
        String strExpected = "1 115/128";

        Double value = imperial.toDoubleFromUnitString(initial);
        String strValue = imperial.toStringValue(value);

        assertEquals(expected, value);
        assertEquals(strExpected, strValue);
    }

    ///endregion Imperial

    ///region Metric

    @Test
    public void doubleToMetric() {
        double initial = 23.5;
        String expected = "23.50";
        String value = metric.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void metricToDouble() {
        String initial = "23.5";
        double expected = 23.5;
        Double value = metric.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    ///endregion Metric

    ///region Pixels

    @Test
    public void doubleToPixels() {
        double initial = 50;
        String expected = "50";
        String value = pixels.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void doubleWithDecimalToPixels() {
        double initial = 15.5;
        String expected = "15";
        String value = pixels.toStringValue(initial);

        assertEquals(expected, value);
    }

    @Test
    public void pixelsToDouble() {
        String initial = "1050";
        Double expected = 1050.0;
        Double value = pixels.toDoubleFromUnitString(initial);

        assertEquals(expected, value);
    }

    ///endregion Pixels
}
