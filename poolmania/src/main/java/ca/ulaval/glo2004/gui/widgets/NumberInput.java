package ca.ulaval.glo2004.gui.widgets;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.controller.ControllerObserver;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.enums.ControllerObserverType;

import javax.swing.*;
import javax.swing.text.PlainDocument;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class NumberInput extends JPanel implements ControllerObserver {
    private final JLabel unitLabel;
    private final JTextField input;
    private final Double min;
    private final boolean restrictToInt;
    private final Unit unitToUse;
    private final boolean useUnitSymbol;

    /**
     * Constructeur
     *
     * @param controller    Pour être notifier quand l'utilisateur change l'unité de mesure
     * @param columns       Le nombre de caractères à afficher
     * @param min           La valeur minimal, si null pas de valeur minimale
     * @param restrictToInt Si true n'accepte pas les décimales sinon accepte les décimales
     * @param unitToUse     L'unité d'affichage à utiliser, si null n’utilise l'unité d'affichage de l'application utiliser
     * @param useUnitSymbol Si true affiche le symbole de l'unité voulue après la valeur sinon ajoute rien
     */
    public NumberInput(Controller controller, int columns, Double min, boolean restrictToInt, Unit unitToUse, boolean useUnitSymbol) {
        this.min = min;
        this.restrictToInt = restrictToInt;
        this.unitToUse = unitToUse;
        this.useUnitSymbol = useUnitSymbol;

        if (useUnitSymbol) {
            unitLabel = new JLabel(unitToUse != null ? unitToUse.getSymbol() : Controller.getDisplayUnit().getSymbol());
        } else {
            unitLabel = null;
        }

        input = new JTextField(columns);

        input.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateText();
            }
        });

        if (unitToUse == null && useUnitSymbol) {
            controller.registerObserver(this, ControllerObserverType.DISPLAY_UNIT);
        }

        buildUp();
    }

    public NumberInput(Controller controller, int columns, Double min, boolean restrictToInt) {
        this(controller, columns, min, restrictToInt, null, true);
    }

    private void buildUp() {
        setOpaque(false);

        input.setHorizontalAlignment(JTextField.CENTER);

        PlainDocument doc = (PlainDocument) input.getDocument();
        doc.setDocumentFilter(new NumberFilter(min, restrictToInt, unitToUse));

        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {}

            @Override
            public void focusLost(FocusEvent e) {
                updateText();
            }
        });

        add(input);
        if (useUnitSymbol) {
            add(unitLabel);
        }
    }

    private void updateText() {
        Unit displayUnit = unitToUse;
        if (displayUnit == null) {
            displayUnit = Controller.getDisplayUnit();
        }

        if (input.getText().isEmpty()) {
            if (min == null) {
                if (restrictToInt) {
                    input.setText("0");
                } else {
                    input.setText(displayUnit.toStringValue(0.0));
                }
            } else {
                setToMin(displayUnit);
            }
        } else {
            Double val = displayUnit.toDoubleFromUnitString(input.getText());
            if (val != null && min != null && val < min) {
                setToMin(displayUnit);
            }

            input.setText(displayUnit.toStringValue(displayUnit.toDoubleFromUnitString(input.getText())));
        }
    }

    private void setToMin(Unit displayUnit) {
        if (restrictToInt) {
            String str = Integer.toString(min.intValue());
            input.setText(str);
        } else {
            input.setText(displayUnit.toStringValue(min));
        }
    }

    public JTextField getInput() {
        return input;
    }

    public String getValue() {
        return input.getText();
    }

    public void setValue(String value) {
        input.setText(value);
    }

    @Override
    public void notifyListeners() {
        if (unitToUse == null && useUnitSymbol) {
            unitLabel.setText(Controller.getDisplayUnit().getSymbol());
        }
    }
}
