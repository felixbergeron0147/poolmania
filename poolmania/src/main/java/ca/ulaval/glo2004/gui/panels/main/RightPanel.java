package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.controller.ControllerObserver;
import ca.ulaval.glo2004.domain.drawing.ui.GridUIDrawer;
import ca.ulaval.glo2004.domain.drawing.ui.UIDrawer;
import ca.ulaval.glo2004.domain.drawing.ui.elements.ElementUIDrawer;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.factories.ElementUIFactory;
import ca.ulaval.glo2004.domain.handlers.GridHandler;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.gui.frames.MainWindow;

import javax.swing.*;
import java.awt.*;

public class RightPanel extends JPanel implements ControllerObserver {
    private final Controller controller;
    private final ElementUIFactory elementUIFactory;
    private final GridUIDrawer gridUIDrawer;
    private UIDrawer uiDrawer;

    public RightPanel(MainWindow mainWindow) {
        controller = mainWindow.getController();
        controller.registerObserver(this, ControllerObserverType.RIGHT_PANEL);
        elementUIFactory = new ElementUIFactory(controller);
        gridUIDrawer = new GridUIDrawer(controller);

        buildUp();
    }

    private void buildUp() {
        setBackground(new Color(18, 58, 3));
        setMinimumSize(new Dimension(352, 0));
    }

    public void setUiDrawer(UIDrawer uiDrawer) {
        this.uiDrawer = uiDrawer;
    }

    public void setUIDrawerForElement(Element element) {
        if (element != null) {
            ElementUIDrawer elementUIDrawer = elementUIFactory.getElementUIDrawer(element);
            elementUIDrawer.setElementReference(element);
            setUiDrawer(elementUIDrawer);
        } else {
            setUiDrawer(null);
        }
    }

    @Override
    public void notifyListeners() {
        removeAll();

        if (uiDrawer != null) {
            uiDrawer.isInternalUpdate(true);
            uiDrawer.updateUIValues();
            uiDrawer.isInternalUpdate(false);
            add(uiDrawer);
            add(Box.createVerticalStrut(10));
        }

        GridHandler gridHandler = controller.getGridHandler();
        if (gridHandler.snapToGrid() && controller.getCurrentMode() == Mode.EDITION) {
            gridUIDrawer.isInternalUpdate(true);
            gridUIDrawer.updateUIValues();
            gridUIDrawer.isInternalUpdate(false);
            add(gridUIDrawer);
        }

        validate();
        repaint();
    }
}
