package ca.ulaval.glo2004.gui.widgets;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class WidgetBuilder {
    public static void buildUpButton(final JButton btn) {
        buildUpButton(btn, 16);
    }

    public static void buildUpButton(final JButton btn, int fontSize) {
        Color backgroundColor = new Color(99, 225, 124);
        Color borderColor = new Color(0, 0, 0);
        Color clickColor = new Color(79, 192, 145);
        Color hoverColor = new Color(28, 114, 21);

        buildUpButton(btn, fontSize, backgroundColor, borderColor, clickColor, hoverColor, Color.black);
    }

    public static void buildUpButton(final JButton btn, int fontSize, final Color backgroundColor, final Color borderColor, final Color clickColor, final Color hoverColor, final Color textColor) {
        btn.setFont(new Font("Monaco", Font.BOLD, fontSize));

        btn.setContentAreaFilled(false);
        btn.setOpaque(true);

        btn.setFocusPainted(false);
        btn.setBackground(backgroundColor);
        btn.setForeground(textColor);
        btn.setBorder(new CompoundBorder(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(backgroundColor, 4)));

        btn.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                btn.setBackground(clickColor);
                btn.setBorder(new CompoundBorder(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(clickColor, 4)));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                btn.setBackground(backgroundColor);
                btn.setBorder(new CompoundBorder(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(backgroundColor, 4)));
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (!btn.isEnabled()) {
                    return;
                }

                btn.setBackground(hoverColor);
                btn.setBorder(new CompoundBorder(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(hoverColor, 4)));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btn.setBackground(backgroundColor);
                btn.setBorder(new CompoundBorder(BorderFactory.createLineBorder(borderColor, 2), BorderFactory.createLineBorder(backgroundColor, 4)));
            }
        });
    }

    public static JPanel buildLabelInputPanel(String label, JComponent component) {
        JPanel panel = new JPanel();
        panel.setOpaque(false);
        panel.add(new JLabel(label));
        panel.add(component);
        return panel;
    }

    public static JPanel buildSliderWithLabel(JSlider slider, JLabel valueLabel, String name) {
        JPanel sliderPanel = new JPanel();
        sliderPanel.setOpaque(false);
        sliderPanel.add(new JLabel(name));
        valueLabel.setMinimumSize(new Dimension(30, 10));
        valueLabel.setPreferredSize(new Dimension(30, 10));
        valueLabel.setMaximumSize(new Dimension(30, 10));
        sliderPanel.add(valueLabel);
        sliderPanel.add(slider);
        slider.setOpaque(false);
        sliderPanel.add(Box.createGlue());
        return sliderPanel;
    }
}
