package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.AppToolType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeToolAction extends AbstractAction implements ActionListener {
    private final Controller controller;
    private final AppToolType wanted;

    public ChangeToolAction(Controller controller, AppToolType wanted) {
        this.controller = controller;
        this.wanted = wanted;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.setCurrentTool(wanted);
    }
}
