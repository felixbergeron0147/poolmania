package ca.ulaval.glo2004.gui.widgets;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.measure.Unit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

/**
 * Inspirer par <a href="https://stackoverflow.com/a/11093360">https://stackoverflow.com/a/11093360</a>
 */
public class NumberFilter extends DocumentFilter {
    private final Double min;
    private final boolean restrictToInt;
    private final Unit unitToUse;

    public NumberFilter(Double min, boolean restrictToInt, Unit unitToUse) {
        this.min = min;
        this.restrictToInt = restrictToInt;
        this.unitToUse = unitToUse;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        Document document = fb.getDocument();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(document.getText(0, document.getLength()));
        stringBuilder.insert(offset, string);

        if (isValidString(stringBuilder.toString())) {
            super.insertString(fb, offset, string, attr);
        }
    }

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        Document document = fb.getDocument();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(document.getText(0, document.getLength()));
        stringBuilder.delete(offset, offset + length);

        if (isValidString(stringBuilder.toString())) {
            super.remove(fb, offset, length);
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        Document document = fb.getDocument();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(document.getText(0, document.getLength()));
        stringBuilder.replace(offset, offset + length, text);

        if (isValidString(stringBuilder.toString())) {
            super.replace(fb, offset, length, text, attrs);
        }
    }

    private boolean isValidString(String str) {
        try {
            if (str.isEmpty() || ((min == null || min < 0) && str.equals("-"))) {
                return true;
            }

            if (str.contains("-") && min != null && min > 0) {
                return false;
            }

            if (restrictToInt) {
                Integer.parseInt(str);
                return true;
            } else {
                Unit displayUnit = unitToUse;
                if (displayUnit == null) {
                    displayUnit = Controller.getDisplayUnit();
                }

                Double val = displayUnit.toDoubleFromUnitString(str);
                return val != null;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
