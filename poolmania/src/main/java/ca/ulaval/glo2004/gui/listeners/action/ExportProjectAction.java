package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.FileType;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExportProjectAction implements ActionListener {
    private final Controller controller;
    private final FileType fileType;

    public ExportProjectAction(Controller controller, FileType fileType) {
        this.controller = controller;
        this.fileType = fileType;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.exportProjectAs(fileType);
    }
}
