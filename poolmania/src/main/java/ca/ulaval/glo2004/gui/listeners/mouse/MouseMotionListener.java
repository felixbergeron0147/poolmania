package ca.ulaval.glo2004.gui.listeners.mouse;

import ca.ulaval.glo2004.domain.controller.Controller;

import java.awt.event.MouseEvent;

public class MouseMotionListener implements java.awt.event.MouseMotionListener {
    private final Controller controller;

    public MouseMotionListener(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        controller.getCurrentTool().mouseMoved(e);
    }
}
