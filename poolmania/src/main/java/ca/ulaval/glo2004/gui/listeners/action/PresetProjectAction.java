package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.gui.frames.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PresetProjectAction implements ActionListener {

    private final MainWindow mainWindow;

    public PresetProjectAction(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        mainWindow.getController().createNewPresetTable();
    }
}
