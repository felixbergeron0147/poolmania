package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.factories.ElementFactory;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.ElementType;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateElementAction implements ActionListener {
    private final ElementType typeWanted;
    private final Controller controller;

    public CreateElementAction(ElementType type, Controller controller) {
        this.typeWanted = type;
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Element element = ElementFactory.getInstance().createElementAtCenter(typeWanted, controller);
        controller.getUndoRedoHandler().pushEditionChange();
        controller.getBilliardTable().addElement(element);
        controller.notifyObservers(ControllerObserverType.DRAWING);
    }
}
