package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.enums.ControllerObserverType;

public class UpdateTableLoop implements Runnable {
    private final BilliardTable tableToUpdate;
    private final Controller controller;
    private final SimulationHandler simulationHandler;

    private boolean continueUpdates = true;

    public UpdateTableLoop(Controller controller, SimulationHandler simulationHandler, BilliardTable table) {
        this.controller = controller;
        this.simulationHandler = simulationHandler;
        this.tableToUpdate = table;
    }

    @Override
    public void run() {
        double last_time = System.nanoTime();
        while (continueUpdates) {
            double time = System.nanoTime();
            double delta_time = ((time - last_time) / 1000000000);
            last_time = time;
            if (simulationHandler.getPhysicsHandler().update(tableToUpdate, delta_time)) {
                stop();
            }

            controller.notifyObservers(ControllerObserverType.DRAWING);
        }

        simulationHandler.setToIsSimulating(false);
    }

    public void stop() {
        continueUpdates = false;
    }
}
