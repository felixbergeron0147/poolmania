package ca.ulaval.glo2004.gui.listeners.mouse;

import ca.ulaval.glo2004.domain.controller.Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;

public class MouseWheelListener extends MouseAdapter {
    private final Controller controller;

    public MouseWheelListener(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        controller.getDrawingPanelViewHandler().setZoom(e.getWheelRotation(), e.getX(), e.getY());
    }
}
