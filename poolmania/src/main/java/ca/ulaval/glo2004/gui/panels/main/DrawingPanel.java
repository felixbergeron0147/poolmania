package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.controller.ControllerObserver;
import ca.ulaval.glo2004.domain.drawing.EditionMainDrawer;
import ca.ulaval.glo2004.domain.drawing.MainDrawer;
import ca.ulaval.glo2004.domain.drawing.SimulationMainDrawer;
import ca.ulaval.glo2004.domain.handlers.DrawingPanelViewHandler;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.gui.listeners.mouse.MouseDragListener;
import ca.ulaval.glo2004.gui.listeners.mouse.MouseMotionListener;
import ca.ulaval.glo2004.gui.listeners.mouse.MousePressedListener;
import ca.ulaval.glo2004.gui.listeners.mouse.MouseWheelListener;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class DrawingPanel extends JPanel implements ControllerObserver {
    private final Controller controller;
    private final Map<Mode, MainDrawer> mainDrawers;

    private MainDrawer mainDrawer;

    public static final Quantity<Imperial> DRAWING_PANEL_SIZE = new Quantity<>(1000, Imperial.get());

    public DrawingPanel(Controller controller) {
        this.controller = controller;

        mainDrawers = new HashMap<>();

        mainDrawers.put(Mode.EDITION, new EditionMainDrawer(controller));
        mainDrawers.put(Mode.SIMULATION, new SimulationMainDrawer(controller));

        controller.registerObserver(this, ControllerObserverType.DRAWING);

        buildUp();
    }

    private void buildUp() {
        addMouseListener(new MousePressedListener(controller));
        addMouseMotionListener(new MouseDragListener(controller));
        addMouseWheelListener(new MouseWheelListener(controller));
        addMouseMotionListener(new MouseMotionListener(controller));

        setBackground(Color.WHITE);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        DrawingPanelViewHandler drawingPanelViewHandler = controller.getDrawingPanelViewHandler();

        double zoom = drawingPanelViewHandler.getZoom();
        double topLeftXPos = drawingPanelViewHandler.getTopLeftXPos();
        double topLeftYPos = drawingPanelViewHandler.getTopLeftYPos();

        g2d.scale(zoom, zoom);
        g2d.translate(topLeftXPos, topLeftYPos);

        g2d.setStroke(new BasicStroke(10));
        g2d.drawRect(0, 0, (int) DRAWING_PANEL_SIZE.toPixels(), (int) DRAWING_PANEL_SIZE.toPixels());

        mainDrawer.draw(g2d);
    }

    @Override
    public void notifyListeners() {
        repaint();
    }

    public void setMainDrawer(Mode mode) {
        mainDrawer = mainDrawers.get(mode);
    }
}
