package ca.ulaval.glo2004.gui.listeners.mouse;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.CursorType;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MousePressedListener extends MouseAdapter {
    private final Controller controller;

    public MousePressedListener(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        controller.getCurrentTool().mousePressed(e);

        if (SwingUtilities.isRightMouseButton(e)) {
            controller.resetToDefaultTool();
        }

        if (SwingUtilities.isMiddleMouseButton(e)) {
            controller.getCursorHandler().setCursor(CursorType.MOVE_VIEW);
            controller.getDrawingPanelViewHandler().setMouseMovePos(e.getX(), e.getY());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        controller.getCurrentTool().mouseReleased(e);

        if (SwingUtilities.isMiddleMouseButton(e)) {
            controller.getCursorHandler().setCursor(CursorType.DEFAULT);
        }
    }
}
