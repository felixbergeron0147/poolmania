package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SaveProjectAction extends AbstractAction implements ActionListener {
    private final Controller controller;
    private final boolean forceAskFilePath;

    public SaveProjectAction(Controller controller, boolean forceAskFilePath) {
        this.controller = controller;
        this.forceAskFilePath = forceAskFilePath;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.saveCurrentProject(forceAskFilePath);
    }
}
