package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.listeners.action.ChangeToolAction;
import ca.ulaval.glo2004.gui.listeners.action.RedoAction;
import ca.ulaval.glo2004.gui.listeners.action.SwitchMode;
import ca.ulaval.glo2004.gui.listeners.action.UndoAction;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CenterPanel extends JPanel {
    private final MainWindow mainWindow;

    private final JButton editModeBtn;
    private final JButton simModeBtn;

    private final JButton undoBtn;
    private final JButton redoBtn;

    private final JButton breakWallBtn;
    private final JButton tableDimensionsBtn;

    private final JButton resetZoomBtn;
    private final JButton centerTableBtn;

    public CenterPanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        editModeBtn = new JButton("Édition");
        simModeBtn = new JButton("Simulation");

        undoBtn = new JButton("Undo");
        redoBtn = new JButton("Redo");

        resetZoomBtn = new JButton("Réinitialiser le zoom");
        centerTableBtn = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/CenterTableIcon.png"))));
        tableDimensionsBtn = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/TableDimensionsIcon.png"))));
        breakWallBtn = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/BreakWallCursor.png"))));

        buildUp();
    }

    private void buildUp() {
        setLayout(new BorderLayout());

        editModeBtn.addActionListener(new SwitchMode(mainWindow.getController(), Mode.EDITION));
        WidgetBuilder.buildUpButton(editModeBtn);
        simModeBtn.addActionListener(new SwitchMode(mainWindow.getController(), Mode.SIMULATION));
        WidgetBuilder.buildUpButton(simModeBtn);

        JPanel bottomBar = new JPanel();
        bottomBar.setLayout(new BorderLayout());
        bottomBar.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0), 4));
        bottomBar.setBackground(new Color(18, 58, 3));

        JPanel modeBar = new JPanel();
        modeBar.setLayout(new GridBagLayout());
        modeBar.add(editModeBtn);
        modeBar.add(editModeBtn);
        modeBar.add(Box.createHorizontalStrut(10));
        modeBar.add(simModeBtn);
        modeBar.setOpaque(false);

        breakWallBtn.setToolTipText("Outil pour briser les murs (B)");
        breakWallBtn.addActionListener(new ChangeToolAction(mainWindow.getController(), AppToolType.BREAK_WALL));
        WidgetBuilder.buildUpButton(breakWallBtn);

        tableDimensionsBtn.setToolTipText("Outil pour redimensionner la table (D)");
        tableDimensionsBtn.addActionListener(new ChangeToolAction(mainWindow.getController(), AppToolType.TABLE_DIMENSION));
        WidgetBuilder.buildUpButton(tableDimensionsBtn);


        undoBtn.setToolTipText("Revenir à la précédente modification (CTRL-Z)");
        undoBtn.addActionListener(new UndoAction(mainWindow.getController()));
        WidgetBuilder.buildUpButton(undoBtn);

        redoBtn.setToolTipText("Rétablir la modification (CTRL-SHIFT-Z)");
        redoBtn.addActionListener(new RedoAction(mainWindow.getController()));
        WidgetBuilder.buildUpButton(redoBtn);

        JPanel undoRedoPanel =new JPanel();
        undoRedoPanel.add(undoBtn);
        undoRedoPanel.add(redoBtn);
        undoRedoPanel.setOpaque(false);

        resetZoomBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainWindow.getController().getDrawingPanelViewHandler().resetZoom();
            }
        });
        WidgetBuilder.buildUpButton(resetZoomBtn);

        centerTableBtn.setToolTipText("Recentre la table à l'écran (C)");
        centerTableBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainWindow.getController().getDrawingPanelViewHandler().centerTable();
            }
        });
        WidgetBuilder.buildUpButton(centerTableBtn);

        JPanel toolsPanel = new JPanel();
        toolsPanel.setOpaque(false);
        toolsPanel.setLayout(new FlowLayout());
        toolsPanel.add(resetZoomBtn);
        toolsPanel.add(centerTableBtn);
        toolsPanel.add(breakWallBtn);
        toolsPanel.add(tableDimensionsBtn);
        toolsPanel.setOpaque(false);

        bottomBar.add(undoRedoPanel, BorderLayout.WEST);
        bottomBar.add(modeBar, BorderLayout.CENTER);
        bottomBar.add(toolsPanel, BorderLayout.EAST);

        add(bottomBar, BorderLayout.SOUTH);
    }
}
