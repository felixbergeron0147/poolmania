package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.Mode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwitchMode implements ActionListener {
    private final Controller controller;
    private final Mode wantedMode;

    public SwitchMode(Controller controller, Mode wantedMode) {
        this.controller = controller;
        this.wantedMode = wantedMode;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.changeMode(wantedMode);
    }
}
