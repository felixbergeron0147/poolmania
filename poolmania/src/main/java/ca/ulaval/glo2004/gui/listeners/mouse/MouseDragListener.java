package ca.ulaval.glo2004.gui.listeners.mouse;

import ca.ulaval.glo2004.domain.controller.Controller;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class MouseDragListener extends MouseMotionAdapter {
    private final Controller controller;

    public MouseDragListener(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        controller.getCurrentTool().mouseDragged(e);

        if (SwingUtilities.isMiddleMouseButton(e)) {
            controller.getDrawingPanelViewHandler().moveView(e.getX(), e.getY());
        }
    }
}
