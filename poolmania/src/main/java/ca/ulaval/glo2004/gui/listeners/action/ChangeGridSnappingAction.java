package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.handlers.GridHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeGridSnappingAction implements ActionListener {
    private final JCheckBoxMenuItem checkBox;
    private final GridHandler gridHandler;

    public ChangeGridSnappingAction(JCheckBoxMenuItem checkBox, GridHandler gridHandler) {
        this.checkBox = checkBox;
        this.gridHandler = gridHandler;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        gridHandler.setSnapToGrid(checkBox.isSelected());
    }
}
