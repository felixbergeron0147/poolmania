package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.menu.TopMenu;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {
    private final MainWindow mainWindow;
    private final TopMenu topMenu;
    private final CenterPanel centerPanel;
    private final DrawingPanel drawingPanel;
    private final JSplitPane centerSplitPane;
    private final RightPanel rightPanel;

    public MainPanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        topMenu = new TopMenu(mainWindow);
        centerPanel = new CenterPanel(mainWindow);
        drawingPanel = new DrawingPanel(mainWindow.getController());
        rightPanel = new RightPanel(mainWindow);
        centerSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, centerPanel, rightPanel);

        buildUp();
    }

    private void buildUp() {
        centerPanel.add(drawingPanel, BorderLayout.CENTER);

        centerSplitPane.setResizeWeight(0.85);
        centerSplitPane.setOneTouchExpandable(true);
        centerSplitPane.setContinuousLayout(true);

        setLayout(new BorderLayout());
        add(centerSplitPane);

        changeMode(mainWindow.getController().getCurrentMode());
    }

    public void changeMode(Mode mode) {
        drawingPanel.setMainDrawer(mode);
        topMenu.changeMode(mode);
    }

    public TopMenu getTopMenu() {
        return topMenu;
    }

    public DrawingPanel getDrawingPanel() {
        return drawingPanel;
    }

    public RightPanel getRightPanel() {
        return rightPanel;
    }
}
