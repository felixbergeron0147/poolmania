package ca.ulaval.glo2004.gui.panels.welcome;

import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.listeners.action.NewProjectAction;
import ca.ulaval.glo2004.gui.listeners.action.OpenProjectAction;
import ca.ulaval.glo2004.gui.listeners.action.PresetProjectAction;

import javax.swing.*;
import java.awt.*;

public class WelcomePanel extends JPanel {
    private final MainWindow mainWindow;
    private final JPanel centerPanel;
    private final JLabel logo;
    private final JButton presetProjectBtn;
    private final JButton newProjectBtn;
    private final JButton openProjectBtn;

    public WelcomePanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        centerPanel = new JPanel();

        logo = new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/PoolManiaLogo.png"))));

        presetProjectBtn = new JButton("Gabarits");
        newProjectBtn = new JButton("Nouveau projet");
        openProjectBtn = new JButton("Ouvrir un projet");

        buildUp();
    }

    private void buildUp() {
        JPanel btnPanel = new JPanel();
        btnPanel.setOpaque(false);

        buildUpBtn(presetProjectBtn);
        presetProjectBtn.addActionListener(new PresetProjectAction(mainWindow));
        btnPanel.add(presetProjectBtn);

        buildUpBtn(newProjectBtn);
        newProjectBtn.addActionListener(new NewProjectAction(mainWindow));
        btnPanel.add(newProjectBtn);

        buildUpBtn(openProjectBtn);
        openProjectBtn.addActionListener(new OpenProjectAction(mainWindow.getController()));
        btnPanel.add(openProjectBtn);

        setLayout(new GridBagLayout());
        setBackground(new Color(93, 220, 125));

        centerPanel.setLayout(new BorderLayout());
        centerPanel.setOpaque(false);
        centerPanel.add(logo, BorderLayout.CENTER);
        centerPanel.add(btnPanel, BorderLayout.SOUTH);

        add(centerPanel);
    }

    private void buildUpBtn(JButton btn) {
        btn.setFocusPainted(false);
        btn.setBackground(new Color(65, 65, 65));
        btn.setForeground(Color.WHITE);
        btn.setPreferredSize(new Dimension(150, 30));
    }
}
