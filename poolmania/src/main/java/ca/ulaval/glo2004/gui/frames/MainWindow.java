package ca.ulaval.glo2004.gui.frames;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.ApplicationPanel;
import ca.ulaval.glo2004.gui.panels.main.MainPanel;
import ca.ulaval.glo2004.gui.panels.welcome.WelcomePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Classe représentant la fenêtre sur laquelle sera affichée notre application.
 */
public class MainWindow extends JFrame {
    private final WelcomePanel welcomePanel;
    private final MainPanel mainPanel;

    private final Controller controller;

    private ApplicationPanel currentPanel;

    public MainWindow() {
        controller = new Controller(this);
        welcomePanel = new WelcomePanel(this);
        mainPanel = new MainPanel(this);

        controller.setUp();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (controller.canChangeProject()) {
                    controller.getSimulationHandler().stopSimulation();
                    dispose();
                }
            }
        });

        initializeWindow();
    }

    private void initializeWindow() {
        setTitle("PoolMania");
        setIconImage(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/appicon.png"))).getImage());
        int screenWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
        setSize(screenWidth, screenHeight);
        setResizable(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new Dimension(1123, 540));

        switchToPanel(ApplicationPanel.WELCOME);
    }

    public void switchToPanel(ApplicationPanel panel) {
        if (currentPanel != null && currentPanel == panel) {
            return;
        }

        currentPanel = panel;

        switch (panel) {
            case WELCOME: {
                setJMenuBar(null);
                setContentPane(welcomePanel);
                break;
            }
            case MAIN: {
                setJMenuBar(mainPanel.getTopMenu());
                setContentPane(mainPanel);
                break;
            }
        }
        validate();
    }

    public Controller getController() {
        return controller;
    }

    public MainPanel getMainPanel() {
        return mainPanel;
    }
}
