package ca.ulaval.glo2004.gui.menu;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Metric;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.enums.FileType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.enums.PresetTable;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.listeners.action.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TopMenu extends JMenuBar {
    private final MainWindow mainWindow;

    private final JMenu fileMenu;
    private final JMenu optionsMenu;
    private final JMenu addMenu;

    private final JMenuItem welcome;
    private final JMenu newProject;
    private final JMenuItem newBlankProject;
    private final JMenu presets;
    private final JMenuItem open;
    private final JMenuItem save;
    private final JMenuItem saveAs;
    private final JMenu export;
    private final JMenuItem exportAsJpg;
    private final JMenuItem exportAsSvg;

    private final JMenu gridOptions;
    private final JCheckBoxMenuItem gridSnap;


    private final JMenu unitPicker;
    private final ButtonGroup radioBtnManager;
    private final JRadioButtonMenuItem metric;
    private final JRadioButtonMenuItem imperial;

    private final JMenuItem addBall;
    private final JMenuItem addWhiteBall;
    private final JMenuItem addPocket;
    private final JMenuItem addObstacle;

    public TopMenu(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        Toolkit toolkit = Toolkit.getDefaultToolkit();

        fileMenu = new JMenu("Fichier");
        optionsMenu = new JMenu("Options");
        addMenu = new JMenu("Ajouter");

        welcome = new JMenuItem("Menu d'accueil");
        newProject = new JMenu("Nouveau projet");
        newBlankProject = new JMenuItem("Nouveau projet vide", new ImageIcon(toolkit.getImage(getClass().getResource("/images/NewProjectIcon.png"))));
        presets = new JMenu("Prédéfini");
        open = new JMenuItem("Ouvrir un projet", new ImageIcon(toolkit.getImage(getClass().getResource("/images/LoadIcon.png"))));
        save = new JMenuItem("Sauvegarder (Ctrl-S)", new ImageIcon(toolkit.getImage(getClass().getResource("/images/SaveIcon.png"))));
        saveAs = new JMenuItem("Sauvegarder sous", new ImageIcon(toolkit.getImage(getClass().getResource("/images/SaveAsIcon.png"))));
        export = new JMenu("Exporter en tant que...");

        exportAsJpg = new JMenuItem("JPG", new ImageIcon(toolkit.getImage(getClass().getResource("/images/JpgIcon.png")))); // https://www.flaticon.com/free-icon/jpg-file-format-variant_29264
        exportAsSvg = new JMenuItem("SVG", new ImageIcon(toolkit.getImage(getClass().getResource("/images/SvgIcon.png")))); // https://www.flaticon.com/free-icon/svg-file-format-variant_29495

        gridOptions = new JMenu("Grille");
        gridSnap = new JCheckBoxMenuItem("Afficher/cacher la grille", new ImageIcon(toolkit.getImage(getClass().getResource("/images/GridIcon.png"))));


        unitPicker = new JMenu("Unité");
        radioBtnManager = new ButtonGroup();
        metric = new JRadioButtonMenuItem("Métrique (" + Metric.get().getSymbol() + ")");
        imperial = new JRadioButtonMenuItem("Imperial (" + Imperial.get().getSymbol() + ")");

        addBall = new JMenuItem("Balle", new ImageIcon(toolkit.getImage(getClass().getResource("/images/BallIcon.png"))));
        addWhiteBall = new JMenuItem("Balle blanche", new ImageIcon(toolkit.getImage(getClass().getResource("/images/WhiteBallIcon.png"))));
        addPocket = new JMenuItem("Poche", new ImageIcon(toolkit.getImage(getClass().getResource("/images/PocketIcon.png"))));
        addObstacle = new JMenuItem("Obstacle", new ImageIcon(toolkit.getImage(getClass().getResource("/images/ObstacleIcon.png"))));

        buildUp();
    }

    private void buildUp() {
        welcome.addActionListener(new ReturnToWelcomePanelAction(mainWindow.getController()));

        fileMenu.add(welcome);
        fileMenu.add(new JSeparator());

        newProject.add(newBlankProject);
        newBlankProject.addActionListener(new NewProjectAction(mainWindow));

        for (final PresetTable presetTable : PresetTable.values()) {
            JMenuItem preset = new JMenuItem(presetTable.getName());
            preset.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    mainWindow.getController().loadPreset(presetTable.getFilePath());
                }
            });
            presets.add(preset);
        }

        newProject.add(presets);
        fileMenu.add(newProject);

        open.addActionListener(new OpenProjectAction(mainWindow.getController()));
        fileMenu.add(open);

        fileMenu.add(new JSeparator());

        save.addActionListener(new SaveProjectAction(mainWindow.getController(), false));
        fileMenu.add(save);

        saveAs.addActionListener(new SaveProjectAction(mainWindow.getController(), true));
        fileMenu.add(saveAs);

        fileMenu.add(new JSeparator());

        exportAsJpg.addActionListener(new ExportProjectAction(mainWindow.getController(), FileType.JPG));
        export.add(exportAsJpg);
        exportAsSvg.addActionListener(new ExportProjectAction(mainWindow.getController(), FileType.SVG));
        export.add(exportAsSvg);
        fileMenu.add(export);

        gridSnap.addActionListener(new ChangeGridSnappingAction(gridSnap, mainWindow.getController().getGridHandler()));
        gridOptions.add(gridSnap);
        optionsMenu.add(gridOptions);

        optionsMenu.add(new JSeparator());

        metric.setSelected(true);
        radioBtnManager.add(imperial);
        imperial.addActionListener(new ChangeUnitAction(mainWindow, Imperial.get()));
        imperial.setSelected(true);
        radioBtnManager.add(metric);
        metric.addActionListener(new ChangeUnitAction(mainWindow, Metric.get()));
        unitPicker.add(metric);
        unitPicker.add(imperial);
        optionsMenu.add(unitPicker);

        addBall.addActionListener(new CreateElementAction(ElementType.BALL, mainWindow.getController()));
        addWhiteBall.addActionListener(new CreateElementAction(ElementType.WHITE_BALL, mainWindow.getController()));
        addPocket.addActionListener(new CreateElementAction(ElementType.POCKET, mainWindow.getController()));
        addObstacle.addActionListener(new CreateElementAction(ElementType.OBSTACLE, mainWindow.getController()));
        addMenu.add(addBall);
        addMenu.add(addWhiteBall);
        addMenu.add(addPocket);
        addMenu.add(addObstacle);

        add(fileMenu);
        add(optionsMenu);
        add(addMenu);
    }

    public void changeMode(Mode mode) {
        switch (mode) {
            case EDITION: {
                addMenu.setEnabled(true);
                break;
            }
            case SIMULATION: {
                addMenu.setEnabled(false);
                break;
            }
        }
    }

    public void changeDisplayUnit(Unit unit) {
        if (unit instanceof Imperial) {
            imperial.setSelected(true);
        } else if (unit instanceof Metric) {
            metric.setSelected(true);
        }
    }
}
