package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.widgets.NumberInput;

import javax.swing.*;
import java.awt.*;

import static ca.ulaval.glo2004.domain.elements.BilliardTable.MIN_NB_TABLE_WALL_NODES;

public class NewProjectPopUp {
    private final JOptionPane optionPane;
    private final JDialog dialog;
    private final NumberInput widthInput;
    private final static int DEFAULT_NB_WALLS = 4;
    private final NumberInput nbWallsInput;

    public NewProjectPopUp(MainWindow mainWindow) {
        JPanel content = new JPanel();
        JLabel widthLabel = new JLabel("Largeur des murs");
        JLabel heightLabel = new JLabel("Nombres de mur");
        widthInput = new NumberInput(mainWindow.getController(), 8, 1.0, false, Imperial.get(), true);
        widthInput.setValue(Imperial.get().toStringValue(40.0));
        nbWallsInput = new NumberInput(mainWindow.getController(), 8, (double) MIN_NB_TABLE_WALL_NODES, true, null, false);
        nbWallsInput.setValue(Integer.toString(DEFAULT_NB_WALLS));

        content.setLayout(new BorderLayout());

        JPanel inputs = new JPanel();
        inputs.setLayout(new FlowLayout(FlowLayout.LEADING));

        inputs.add(widthLabel);
        inputs.add(widthInput);
        inputs.add(heightLabel);
        inputs.add(nbWallsInput);

        JLabel info = new JLabel("Minimum 1 pouce de largeur avec un minimum de 3 murs");
        info.setAlignmentX(JLabel.CENTER_ALIGNMENT);

        content.add(info, BorderLayout.NORTH);
        content.add(inputs, BorderLayout.CENTER);

        optionPane = new JOptionPane(content, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
        dialog = optionPane.createDialog(mainWindow, "Nouveau projet");
    }

    public boolean showAndGetAnswer() {
        dialog.setVisible(true);
        dialog.dispose();

        Object selectedValue = optionPane.getValue();

        if (selectedValue == null) {
            return false; // Closed
        }

        return (int) selectedValue == JOptionPane.OK_OPTION;
    }

    public Quantity<Imperial> getWidth() {
        double width = Imperial.get().toDoubleFromUnitString(widthInput.getValue());
        return new Quantity<>(width, Imperial.get());
    }

    public int getHeight() {
        return Integer.parseInt(nbWallsInput.getValue());
    }
}
