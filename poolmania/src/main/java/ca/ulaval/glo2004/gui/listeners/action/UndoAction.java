package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.controller.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UndoAction extends AbstractAction implements ActionListener {
    private final Controller controller;

    public UndoAction(Controller controller) {
        this.controller = controller;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        controller.undo();
    }
}
