package ca.ulaval.glo2004.gui.panels.main;

import ca.ulaval.glo2004.enums.PresetTable;
import ca.ulaval.glo2004.gui.frames.MainWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Fenêtre de dialogue qui s'affiche lorsque l'utilisateur choisit de démarrer un nouveau projet à partir d'un gabarit.
 */
public class PresetProjectPopUp {
    private final JDialog dialog;
    private final JList<String> list;
    private final JOptionPane optionPane;
    private final static PresetTable DEFAULT_PRESET = PresetTable.AMERICAN8FT;

    /**
     * Constructeur de la classe PresetProjectPopUp
     *
     * @param mainWindow La fenêtre principale de l'application
     */
    public PresetProjectPopUp(MainWindow mainWindow) {
        JPanel content = new JPanel();
        content.setLayout(new BorderLayout());

        JLabel presetsLabel = new JLabel("Choisir le gabarit de départ");

        DefaultListModel<String> model = new DefaultListModel<>();
        for (PresetTable preset : PresetTable.values())
            model.addElement(preset.getName());

        list = new JList<>(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);

        JScrollPane scrollPane = new JScrollPane(list);

        content.add(presetsLabel, BorderLayout.NORTH);
        content.add(scrollPane, BorderLayout.CENTER);

        optionPane = new JOptionPane(content, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
        dialog = optionPane.createDialog(mainWindow, "Démarrer à partir d'un gabarit");
    }

    /**
     * Affiche la fenêtre de dialogue ainsi que sa sélection et attends la réponse de l'utilisateur
     *
     * @return false si l'utilisateur annule, vrai si l'utilisateur comfirme sa sélection
     */
    public boolean ChoosePreset() {
        dialog.setVisible(true);
        dialog.dispose();

        Object selectedValue = optionPane.getValue();

        if (selectedValue == null) {
            return false; // Closed
        }

        return (int) selectedValue == JOptionPane.OK_OPTION;
    }

    /**
     * Obtient le gabarit sélectionné lors de l'appel de cette méthode
     *
     * @return Le gabarit sélectionné
     */
    public PresetTable getPreset() {
        if (list.getSelectedValue() != null) return getPresetFromName(list.getSelectedValue());
        else return DEFAULT_PRESET;
    }

    /**
     * Cherche PresetTable pour la valeur qui correspond au nom passé en paramètre
     *
     * @param name Le nom du preset à trouver
     * @return Le preset en format PresetTable trouvé
     */
    public PresetTable getPresetFromName(String name) {
        for (PresetTable preset : PresetTable.values()) {
            if (preset.getName().equals(name)) return preset;
        }

        return null;
    }
}
