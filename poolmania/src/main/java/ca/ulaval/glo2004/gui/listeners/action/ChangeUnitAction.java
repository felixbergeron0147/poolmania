package ca.ulaval.glo2004.gui.listeners.action;

import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.frames.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeUnitAction implements ActionListener {
    private final MainWindow mainWindow;
    private final Unit wantedUnit;

    public ChangeUnitAction(MainWindow mainWindow, Unit wantedUnit) {
        this.mainWindow = mainWindow;
        this.wantedUnit = wantedUnit;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        mainWindow.getController().setDisplayUnit(wantedUnit);
        mainWindow.getMainPanel().getTopMenu().changeDisplayUnit(wantedUnit);
    }
}
