package ca.ulaval.glo2004.geometry.structure;

public class UnitSpeedVector {
    private final Vec2 direction;
    private double speed;

    //Un constructeur assignant x,y,v
    public UnitSpeedVector(double xDir, double yDir, double speed) {
        //Ne pas créer de vecteur null svp
        direction = new Vec2(xDir, yDir);
        direction.normalize();
        this.speed = speed;
    }

    public UnitSpeedVector(UnitSpeedVector unitSpeedVector) {
        this.direction = new Vec2(unitSpeedVector.direction);
        this.speed = unitSpeedVector.speed;
    }

    private double getLength() {
        return direction.getLength();
    }

    public double getXDirection() {
        return direction.getX();
    }

    public void setXDirection(double x) {
        direction.setX(direction.getX() / direction.getLength());
    }

    public void setDirection(double x, double y) {
        direction.setX(x);
        direction.setY(y);
        direction.normalize();
    }

    public void setDirection(Vec2 v) {
        direction.setX(v.getX());
        direction.setY(v.getY());
        direction.normalize();
    }

    public double getYDirection() {
        return direction.getY();
    }

    public void setYDirection(double y) {
        direction.setY(direction.getY() / direction.getLength());
    }

    public double getSpeed() {return speed;}

    public void setSpeed(double speed) {
        if (speed < 0) {
            speed = 0;
        }
        this.speed = speed;
    }

    public Vec2 getDirection() {
        return direction;
    }

    public Vec2 getMovement() {
        return new Vec2(direction.getX() * speed, direction.getY() * speed);
    }

    public void setMovement(Vec2 v) {
        direction.setX(v.getX());
        direction.setY(v.getY());
        if (direction.isNull()) {
            speed = 0;
        } else {
            speed = direction.getLength();
        }
        direction.normalize();
    }

    public double getXMovement() {
        return direction.getX() * speed;
    }

    public double getYMovement() {
        return direction.getY() * speed;
    }

    public void flipX() {
        direction.setX(direction.getX() * -1);
    }

    public void flipY() {
        direction.setY(direction.getY() * -1);
    }
}
