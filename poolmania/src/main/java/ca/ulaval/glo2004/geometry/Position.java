package ca.ulaval.glo2004.geometry;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.awt.geom.Point2D;

/**
 * Pour sauvegarder la position en valeur impériale
 */
public class Position {
    public final Quantity<Imperial> x;
    public final Quantity<Imperial> y;

    public Position() {
        x = new Quantity<>(0, Imperial.get());
        y = new Quantity<>(0, Imperial.get());
    }

    public <U extends Unit, Y extends Unit> Position(Quantity<U> x, Quantity<Y> y) {
        this();
        this.x.set(x);
        this.y.set(y);
    }

    public Position(double xPixel, double yPixel) {
        this();
        x.set(new Quantity<>(xPixel, Pixels.get()));
        y.set(new Quantity<>(yPixel, Pixels.get()));
    }

    public Position(Position pos) {
        x = new Quantity<>(pos.getXValue(), Imperial.get());
        y = new Quantity<>(pos.getYValue(), Imperial.get());
    }

    public <U extends Unit, Y extends Unit> void setLocation(Quantity<U> x, Quantity<Y> y) {
        this.x.set(x);
        this.y.set(y);
    }

    public void translate(Position translation) {
        x.add(translation.x);
        y.add(translation.y);
    }

    public void translate(Vec2 translation) {
        x.add(new Quantity<>(translation.getX(), Pixels.get()));
        y.add(new Quantity<>(translation.getY(), Pixels.get()));
    }

    public void translate(double xPixel, double yPixel) {
        x.add(new Quantity<>(xPixel, Pixels.get()));
        y.add(new Quantity<>(yPixel, Pixels.get()));
    }

    public double getXPixel() {
        return x.toPixels();
    }

    public double getYPixel() {
        return y.toPixels();
    }

    public double getXValue() {
        return x.value();
    }

    public double getYValue() {
        return y.value();
    }

    public Point2D.Double toPoint2DDouble() {
        return new Point2D.Double(x.toPixels(), y.toPixels());
    }

    public Vec2 toVec2(){
        return new Vec2(x.toPixels(), y.toPixels());
    }
}
