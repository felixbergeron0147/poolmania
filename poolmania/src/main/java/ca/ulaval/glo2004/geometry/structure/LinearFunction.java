package ca.ulaval.glo2004.geometry.structure;

import java.awt.geom.Point2D;

public class LinearFunction {
    private double a;
    private double b;

    public LinearFunction(Point2D.Double c1, Point2D.Double c2) {
        if (c1.x != c2.x)
            a = ((c2.y - c1.y) / (c2.x - c1.x));
        b = -((a * c1.x) - c1.y);
    }

    public LinearFunction getOrthogonalFunction() {
        LinearFunction orthogonalFunction = this;
        orthogonalFunction.setA(-1 / orthogonalFunction.getA());
        return orthogonalFunction;
    }

    public double getA() {return a;}

    public void setA(double a) {this.a = a;}

    public double getB() {return b;}

    public double executeFunction(double x) {
        return (a * x) + b;
    }
}
