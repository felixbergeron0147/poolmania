package ca.ulaval.glo2004.geometry;

import ca.ulaval.glo2004.domain.elements.WallNode;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.geometry.structure.LinearFunction;
import ca.ulaval.glo2004.geometry.structure.UnitSpeedVector;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.awt.geom.Point2D;
import java.util.Vector;

public final class MathUtils {
    public static Point2D.Double getPointIntersection(LinearFunction f1, LinearFunction f2) {
        double x = (f1.getB() - f2.getB()) / (f2.getA() - f1.getA());
        double y = f1.executeFunction(0);
        return new Point2D.Double(x, y);
    }

    public static double scalarProduct(UnitSpeedVector vec1, UnitSpeedVector vec2) {
        return (vec1.getXDirection() * vec2.getXDirection() + vec1.getYDirection() * vec2.getYDirection());
    }

    public static UnitSpeedVector getVectorFromLinearFunction(LinearFunction line) {
        double defaultXValue = 1;
        return new UnitSpeedVector(defaultXValue, line.executeFunction(defaultXValue) - line.getB(), 1);
    }

    public static UnitSpeedVector getReflectedVectorFromLinearFunction(UnitSpeedVector spdVec, LinearFunction line) {
        UnitSpeedVector wallNormalVector = getVectorFromLinearFunction(line.getOrthogonalFunction());
        double vX = spdVec.getXDirection();
        double vY = spdVec.getYDirection();
        double nX = wallNormalVector.getXDirection();
        double nY = wallNormalVector.getYDirection();
        double refX = vX - (2 * ((vX * nX) + (vY * nY)) * nX);
        double refY = vY - (2 * ((vX * nX) + (vY * nY)) * nY);
        return new UnitSpeedVector(refX, refY, spdVec.getSpeed());
    }

    public static Point2D.Float getMiddWayPoint(Point2D.Float p1, Point2D.Float p2) {
        return new Point2D.Float((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    public static Vec2 getIntersectionPosition(Vec2 line1Pos1, Vec2 line1Pos2, Vec2 line2Pos1, Vec2 line2Pos2) {
        double line1X = line1Pos1.getX() - line1Pos2.getX();
        double line2X = line2Pos1.getX() - line2Pos2.getX();
        double line1Y = line1Pos1.getY() - line1Pos2.getY();
        double line2Y = line2Pos1.getY() - line2Pos2.getY();
        double c = line1X * line2Y - line1Y * line2X;
        double a = line1Pos1.getX() * line1Pos2.getY() - line1Pos1.getY() * line1Pos2.getX();
        double b = line2Pos1.getX() * line2Pos2.getY() - line2Pos1.getY() * line2Pos2.getX();
        if (c != 0) {
            double x = (a * line2X - b * line1X) / c;
            double y = (a * line2Y - b * line1Y) / c;
            return new Vec2(x, y);
        } else {
            // Parrallel
            return null;
        }
    }

    public static boolean areClose(double d1, double d2, double threshold) {
        return Math.abs(d1 - d2) < threshold;
    }

    public static Vec2 getPointOnLineClosestToAnotherPoint(Vec2 linePoint, Vec2 lineVec, Vec2 distantPoint)
    {
        lineVec = lineVec.normalize();
        return Vec2.add(linePoint, Vec2.multi(lineVec, Vec2.substract(distantPoint, linePoint).dot(lineVec)));
    }

    public static Polygon2D createPolygonFromWallNodes(Vector<WallNode> nodes){
        Polygon2D polygon2D = new Polygon2D();
        for (WallNode wallNode : nodes) {
            polygon2D.addPoint((float) wallNode.getPos().getXPixel(), (float) wallNode.getPos().getYPixel());
        }
        return polygon2D;
    }
}
