package ca.ulaval.glo2004.geometry.structure;

import java.awt.geom.Point2D;

public class Vec2 {
    private double x;
    private double y;

    /**
     * Constructeur de la classe Vec2
     * @param x Le X du Vec2
     * @param y Le y du Vec2
     */
    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructeur de copie de la classe Vec2
     * @param v Le Vec2 a copier
     */
    public Vec2(Vec2 v) {
        this.x = v.x;
        this.y = v.y;
    }

    public static Vec2 substract(Vec2 v1, Vec2 v2) {
        return new Vec2(v1.x - v2.x, v1.y - v2.y);
    }

    public static Vec2 add(Vec2 vec1,Vec2 vec2) { return new Vec2(vec1.x + vec2.x,vec1.y + vec2.y);
    }
    public static Vec2 multi(Vec2 v1, double v) {
        return new Vec2(v1.x * v, v1.y * v);
    }

    public static Vec2 divide(Vec2 vec, double v) {
        if (v != 0) {
            vec.x /= v;
            vec.y /= v;
        }
        return vec;
    }

    public static Vec2 getOrthogonalVec(Vec2 v1) {
        return new Vec2(v1.x, v1.y * -1);
    }

    public double getLength() {
        if (!isNull()) {
            return (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
        } else {
            return x + y;
        }
    }

    public Vec2 normalize() {
        if (!isNull()) {
            double lg = getLength();
            x /= lg;
            y /= lg;
        }
        return this;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean isNull() {return (x == 0 && y == 0);}

    public Vec2 multi(double value) {
        x *= value;
        y *= value;
        return this;
    }

    public Vec2 add(Vec2 vec) {
        x += vec.getX();
        y += vec.getY();
        return this;
    }

    public double distance(Vec2 vec) {
        return Math.sqrt(Math.pow(vec.x - x, 2) + Math.pow(vec.y - y, 2));
    }

    public double dot(Vec2 vec) {
        return x * vec.x + y * vec.y;
    }

    public double det(Vec2 vec) {
        return x * vec.x - y * vec.y;
    }

    public double angle(Vec2 vec) {
        return Math.atan2(vec.dot(new Vec2(-y, x)), vec.dot(new Vec2(x, y)));
    }

    public Vec2 inverse() {
        return new Vec2(-x, -y);
    }

    public Point2D.Double toPoint2DDouble(){
        return new Point2D.Double(x,y);
    }
}
