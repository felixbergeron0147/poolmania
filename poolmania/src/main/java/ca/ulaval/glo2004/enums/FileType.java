package ca.ulaval.glo2004.enums;

public enum FileType {
    JPG(".jpg", "Image"),
    SVG(".svg", "Image vectorielle"),
    PMT(".pmt", "PoolMania Table");

    public final String fileExtension;
    private final String description;

    FileType(String fileExtension, String description) {
        this.fileExtension = fileExtension;
        this.description = description;
    }

    public String getDescription() {
        return String.format("(%s) " + description, fileExtension);
    }
}
