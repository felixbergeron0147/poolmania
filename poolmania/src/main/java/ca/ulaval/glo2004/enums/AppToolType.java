package ca.ulaval.glo2004.enums;

public enum AppToolType {
    SELECTION(Mode.EDITION),
    BREAK_WALL(Mode.EDITION),
    TABLE_DIMENSION(Mode.EDITION),
    SIMULATION(Mode.SIMULATION),
    PLACE_WHITE_BALL(Mode.SIMULATION);

    private final Mode associatedMode;

    AppToolType(Mode associatedMode) {
        this.associatedMode = associatedMode;
    }

    public Mode getAssociatedMode() {
        return associatedMode;
    }
}
