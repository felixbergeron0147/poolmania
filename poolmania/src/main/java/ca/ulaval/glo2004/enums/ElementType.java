package ca.ulaval.glo2004.enums;

/**
 * First element is processed/drawn first
 */
public enum ElementType {
    WALL_NODE,
    WALL,
    POCKET,
    OBSTACLE,
    BALL,
    WHITE_BALL
}
