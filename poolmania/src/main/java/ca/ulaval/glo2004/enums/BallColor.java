package ca.ulaval.glo2004.enums;

import java.awt.*;
import java.util.Random;

public enum BallColor {
    YELLOW(Color.yellow),
    RED(Color.red),
    BLUE(Color.blue),
    PURPLE(Color.magenta),
    ORANGE(Color.orange),
    GREEN(new Color(2, 138, 2)),
    BURGUNDY(new Color(70, 6, 6)),
    BLACK(new Color(50, 50, 50)),
    WHITE(Color.white); // Dois être à la fin pour que le générateur de couleur aléatoire fonctionne

    public final Color color;

    BallColor(Color color) {
        this.color = color;
    }

    public static BallColor getRandomColor() {
        Random rnd = new Random();
        return BallColor.values()[rnd.nextInt(BallColor.values().length - 1)];
    }
}
