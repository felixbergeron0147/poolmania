package ca.ulaval.glo2004.enums;

public enum AppSound {
    BALL_COLLISION("/sounds/BallCollision.wav"), // https://youtu.be/cVj_Gy5HE7A
    CUE_HIT("/sounds/CueHit.wav"), // https://www.soundfishing.eu/sound/billiards
    BALL_POCKET("/sounds/BallPocket.wav"); //https://freesound.org/people/Angie81Dee/sounds/365345/

    private final String filePath;

    AppSound(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }
}
