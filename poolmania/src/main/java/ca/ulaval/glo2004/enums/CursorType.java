package ca.ulaval.glo2004.enums;

public enum CursorType {
    DEFAULT,
    BREAK_WALL,
    MOVE_VIEW,
    RESIZE_V,
    RESIZE_H,
    RESIZE_CORNER_NW,
    RESIZE_CORNER_NE,
    TARGET,
    WHITE_BALL_GOOD,
    WHITE_BALL_BAD,
}
