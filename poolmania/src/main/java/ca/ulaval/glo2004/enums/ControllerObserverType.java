package ca.ulaval.glo2004.enums;

public enum ControllerObserverType {
    DRAWING,
    RIGHT_PANEL,
    DISPLAY_UNIT // TODO not needed to be outside of numberinput anymore
}
