package ca.ulaval.glo2004.enums;

/**
 * Énumération constituée de tous les gabarits de tables billard créés
 */
public enum PresetTable {
    ENGLISH6FT("Anglais 6 pieds", "/presets/English6ft.pmt"),
    ENGLISH7FT("Anglais 7 pieds", "/presets/English7ft.pmt"),
    AMERICAN7FT("Américain 7 pieds", "/presets/American7ft.pmt"),
    AMERICAN8FT("Américain 8 pieds", "/presets/American8ft.pmt"),
    AMERICAN9FT("Américain 9 pieds", "/presets/American9ft.pmt"),
    OCTAGON_OBSTACLE("Octogone Obstacle", "/presets/OctagonObstacle.pmt");

    private final String filePath;
    private final String name;

    PresetTable(String name, String filePath) {
        this.name = name;
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getName() {
        return name;
    }
}
