package ca.ulaval.glo2004.enums;

public enum Mode {
    EDITION,
    SIMULATION
}
