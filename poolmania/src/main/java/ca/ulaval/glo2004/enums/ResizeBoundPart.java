package ca.ulaval.glo2004.enums;

/**
 * Doit être dans le même ordre que l'ordre de traitement des partis du bound box
 */
public enum ResizeBoundPart {
    TOP_LEFT_CORNER,
    BOTTOM_RIGHT_CORNER,
    BOTTOM_LEFT_CORNER,
    TOP_RIGHT_CORNER,
    TOP_SIDE,
    BOTTOM_SIDE,
    LEFT_SIDE,
    RIGHT_SIDE
}
