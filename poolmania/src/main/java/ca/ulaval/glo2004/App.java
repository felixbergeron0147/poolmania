package ca.ulaval.glo2004;

import ca.ulaval.glo2004.gui.frames.MainWindow;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
        mainWindow.setExtendedState(mainWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        mainWindow.setVisible(true);
    }
}