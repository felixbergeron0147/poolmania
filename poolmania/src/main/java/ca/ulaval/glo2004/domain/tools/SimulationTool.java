package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.CueDrawer;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.CursorType;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class SimulationTool extends AppTool {
    public SimulationTool(Controller controller) {
        super(controller, AppToolType.SIMULATION);
    }

    @Override
    public void onEnable() {}

    @Override
    public void onDisable() {}

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && !controller.getSimulationHandler().isSimulating()) {
            updateShotOptions(e);
            controller.getCursorHandler().setCursor(CursorType.TARGET);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            controller.getCursorHandler().setCursor(CursorType.DEFAULT);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && !controller.getSimulationHandler().isSimulating()) {
            updateShotOptions(e);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    private void updateShotOptions(MouseEvent e) {
        Ball whiteBall = controller.getBilliardTable().getWhiteBall();
        if (whiteBall == null) {
            return;
        }

        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(e.getX(), e.getY());
        Point2D.Double whiteBallPos = whiteBall.getPos().toPoint2DDouble();
        double newAngle = Math.atan2(mousePos.getY() - whiteBallPos.getY(), mousePos.getX() - whiteBallPos.getX());

        double cueWidth = CueDrawer.getInstance().getCueWidth();

        double newShotForce = Point2D.distance(mousePos.getX(), mousePos.getY(), whiteBallPos.getX(), whiteBallPos.getY()) / cueWidth;
        if (newShotForce > 1) {
            newShotForce = 1;
        }

        SimulationHandler simulationHandler = controller.getSimulationHandler();
        simulationHandler.setShotAngle(Math.toDegrees(newAngle));
        simulationHandler.setShotForce((int) (newShotForce * 100));

        controller.notifyObservers(ControllerObserverType.DRAWING);
        controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }
}
