package ca.ulaval.glo2004.domain.drawing;

import java.awt.*;

public interface MainDrawer {
    void draw(Graphics2D g);
}
