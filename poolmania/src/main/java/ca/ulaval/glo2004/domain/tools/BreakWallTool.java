package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.elements.Wall;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.CursorType;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class BreakWallTool extends AppTool {
    private Wall hoveredWall;
    private Point2D.Double breakPos;

    public BreakWallTool(Controller controller) {
        super(controller, AppToolType.BREAK_WALL);
    }

    @Override
    public void onEnable() {
        controller.getCursorHandler().setCursor(CursorType.BREAK_WALL);

        controller.getSelectionHandler().removeSelectedElement();

        Point mousePos = controller.getDrawingPanel().getMousePosition();
        if (mousePos != null) {
            updateHoveredWall(mousePos.getX(), mousePos.getY());
        }
    }

    @Override
    public void onDisable() {
        controller.getCursorHandler().setCursor(CursorType.DEFAULT);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e) && hoveredWall != null) {
            controller.getUndoRedoHandler().pushEditionChange();
            controller.getBilliardTable().breakWall(hoveredWall, breakPos);
            controller.notifyObservers(ControllerObserverType.DRAWING);
            updateHoveredWall(e.getX(), e.getY());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        updateHoveredWall(e.getX(), e.getY());
    }

    public Wall getHoveredWall() {
        return hoveredWall;
    }

    public Point2D.Double getBreakPos() {
        return breakPos;
    }

    private void updateHoveredWall(double xPixel, double yPixel) {
        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(xPixel, yPixel);
        Element hoveredElement = controller.getBilliardTable().getElementAtPosition(mousePos.getX(), mousePos.getY(), ElementType.WALL);
        if (hoveredElement != null) {
            hoveredWall = (Wall) hoveredElement;
            Vec2 nearestBreakPos = hoveredWall.getNearestBreakPos(mousePos);
            if (nearestBreakPos != null) {
                breakPos = new Point2D.Double(nearestBreakPos.getX(), nearestBreakPos.getY());
            } else {
                breakPos = null;
            }
        } else {
            hoveredWall = null;
            breakPos = null;
        }
        controller.notifyObservers(ControllerObserverType.DRAWING);
    }
}
