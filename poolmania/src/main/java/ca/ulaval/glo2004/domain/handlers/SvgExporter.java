package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.TableDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.BallDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.ObstacleDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.PocketDrawer;
import ca.ulaval.glo2004.domain.elements.*;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Vector;

@SuppressWarnings("SameParameterValue")
public class SvgExporter {
    private final Controller controller;

    public SvgExporter(Controller controller) {
        this.controller = controller;
    }

    public String getData() {
        // SVG information
        StringBuilder data = new StringBuilder();

        String FILE_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
        addData(data, FILE_HEADER);

        int size = (int) DrawingPanel.DRAWING_PANEL_SIZE.toPixels();
        addData(data, start(size, size));

        // Defs
        String pocketFade = "pocketFade";
        addData(data, addRadialGradient(pocketFade, PocketDrawer.GRADIENT_VALUES[0], PocketDrawer.COLOR_VALUES[0], PocketDrawer.GRADIENT_VALUES[1], PocketDrawer.COLOR_VALUES[1]));

        // Background
        addData(data, rect(Color.white, Color.black, 0, 0, size, size));

        BilliardTable table = controller.getBilliardTable();

        // Walls
        Vector<Wall> walls = table.getExteriorWalls();
        for (int i = 0; i < walls.size(); i++) {
            Wall wall = walls.elementAt(i);
            Wall nextWall = walls.elementAt((i + 1) % walls.size());
            int previousIndex = i - 1;
            if (previousIndex < 0) {
                previousIndex = walls.size() - 1;
            }
            Wall previousWall = walls.elementAt(previousIndex);
            Vector<Polygon2D> wallPolygons = TableDrawer.getInstance().getWallPolygons(wall, previousWall, nextWall);
            for (Polygon2D polygon : wallPolygons) {
                addData(data, polygon(TableDrawer.WALL_COLOR, TableDrawer.BORDER_COLOR, TableDrawer.WALL_BORDER_WIDTH, toPoints(polygon)));
            }

            // Guide dots
            Vector<Polygon2D> guideDots = TableDrawer.getInstance().getWallGuidePoints(wall);
            for (Polygon2D polygon : guideDots) {
                addData(data, polygon(TableDrawer.GUIDE_DOT_COLOR, null, 0, toPoints(polygon)));
            }
        }

        // Table background
        Vector<Point2D.Double> points = new Vector<>();
        for (WallNode wallNode : table.getWallNodes()) {
            points.add(new Point2D.Double(wallNode.getPos().getXPixel(), wallNode.getPos().getYPixel()));
        }
        addData(data, polygon(TableDrawer.TABLE_BG_COLOR, TableDrawer.TABLE_BORDER_COLOR, TableDrawer.TABLE_BORDER_WIDTH, points));

        // Elements
        Vector<Element> elements = table.getElements();
        for (Element element : elements) {
            switch (element.getElementType()) {
                case POCKET: {
                    Pocket pocket = (Pocket) element;
                    addData(data, circle(pocket.getPos().getXPixel(), pocket.getPos().getYPixel(), pocket.getDiameter().toPixels() / 2, pocketFade));
                    break;
                }
                case OBSTACLE: {
                    Obstacle obstacle = (Obstacle) element;
                    addData(data, polygon(ObstacleDrawer.OBSTACLE_COLOR, ObstacleDrawer.OBSTACLE_BORDER_COLOR, ObstacleDrawer.OBSTACLE_BORDER_WIDTH, toPoints(obstacle.getSelectionBox())));
                    break;
                }
                case WHITE_BALL:
                case BALL: {
                    Ball ball = (Ball) element;
                    Vector<Polygon2D> ballPolygons = BallDrawer.getBallPolygons(ball);
                    if (ball.isStriped()) {
                        // Blanc
                        addData(data, polygon(Color.white, null, 0, toPoints(ballPolygons.elementAt(0))));
                        // Color
                        addData(data, polygon(ball.getColor().color, null, 0, toPoints(ballPolygons.elementAt(1))));
                        // Border
                        addData(data, polygon(null, BallDrawer.BALL_BORDER_COLOR, BallDrawer.BALL_BORDER_WIDTH, toPoints(ballPolygons.elementAt(0))));
                    } else {
                        addData(data, polygon(ball.getColor().color, BallDrawer.BALL_BORDER_COLOR, BallDrawer.BALL_BORDER_WIDTH, toPoints(ballPolygons.elementAt(0))));
                    }
                    break;
                }
            }
        }

        // Pas de watermark car le projet est privé

        addData(data, "</svg>");

        return data.toString();
    }

    private void addData(StringBuilder stringBuilder, String data) {
        stringBuilder.append(data);
        stringBuilder.append(System.lineSeparator());
    }

    private String start(int width, int height) {
        return String.format("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"4000\" height=\"4000\" viewBox=\"0 0 %d %d\">", width, height);
    }

    private String rect(Color fill, Color stroke, double x, double y, double width, double height) {
        return String.format("<rect fill=\"%s\" stroke=\"%s\" x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\"/>", toHex(fill), toHex(stroke), x, y, width, height);
    }

    private String polygon(Color fill, Color stroke, int strokeWidth, Vector<Point2D.Double> points) {
        StringBuilder pointsData = new StringBuilder();
        for (Point2D.Double point : points) {
            pointsData.append(String.format("%f,%f ", point.getX(), point.getY()));
        }
        pointsData.deleteCharAt(pointsData.length() - 1);

        return String.format("<polygon fill=\"%s\" stroke=\"%s\" stroke-width=\"%d\" points=\"%s\" />", toHex(fill), toHex(stroke), strokeWidth, pointsData);
    }

    private String toHex(Color color) {
        if (color == null) {
            return "none";
        }
        return String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
    }

    private String addRadialGradient(String id, float offset1, Color color1, float offset2, Color color2) {
        return String.format("<defs> <radialGradient id=\"%s\"> <stop offset=\"%d%%\" stop-color=\"%s\" /> <stop offset=\"%d%%\" stop-color=\"%s\" /> </radialGradient> </defs>", id, (int) offset1 * 100, toHex(color1), (int) offset2 * 100, toHex(color2));
    }

    private String circle(double x, double y, double radius, String gradient) {
        return String.format("<circle cx=\"%f\" cy=\"%f\" r=\"%f\" fill=\"url('#%s')\" />", x, y, radius, gradient);
    }

    private Vector<Point2D.Double> toPoints(Polygon2D polygon) {
        Vector<Point2D.Double> points = new Vector<>();
        for (int j = 0; j < polygon.npoints; j++) {
            points.add(new Point2D.Double(polygon.xpoints[j], polygon.ypoints[j]));
        }
        return points;
    }
}
