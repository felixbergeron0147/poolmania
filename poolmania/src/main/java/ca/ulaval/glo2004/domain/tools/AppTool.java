package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.AppToolType;

import java.awt.event.MouseEvent;

public abstract class AppTool {
    protected final Controller controller;
    private final AppToolType associatedType;

    public AppTool(Controller controller, AppToolType associatedType) {
        this.controller = controller;
        this.associatedType = associatedType;
    }

    public abstract void onEnable();

    public abstract void onDisable();

    public abstract void mousePressed(MouseEvent e);

    public abstract void mouseReleased(MouseEvent e);

    public abstract void mouseDragged(MouseEvent e);

    public abstract void mouseMoved(MouseEvent e);

    public AppToolType getToolType() {
        return associatedType;
    }
}
