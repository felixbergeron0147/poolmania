package ca.ulaval.glo2004.domain.factories;

import ca.ulaval.glo2004.domain.drawing.tools.AppToolDrawer;
import ca.ulaval.glo2004.domain.drawing.tools.BreakWallToolDrawer;
import ca.ulaval.glo2004.domain.drawing.tools.DimensionTableToolDrawer;
import ca.ulaval.glo2004.domain.drawing.tools.SelectionToolDrawer;
import ca.ulaval.glo2004.domain.tools.AppTool;
import ca.ulaval.glo2004.enums.AppToolType;

import java.util.HashMap;
import java.util.Map;

public class ToolDrawerFactory {
    private static ToolDrawerFactory instance = null;

    private final Map<AppToolType, AppToolDrawer> appToolDrawers;

    private ToolDrawerFactory() {
        appToolDrawers = new HashMap<>();

        appToolDrawers.put(AppToolType.TABLE_DIMENSION, new DimensionTableToolDrawer());
        appToolDrawers.put(AppToolType.BREAK_WALL, new BreakWallToolDrawer());
        appToolDrawers.put(AppToolType.SELECTION, new SelectionToolDrawer());
    }

    public static ToolDrawerFactory getInstance() {
        if (instance == null) {
            instance = new ToolDrawerFactory();
        }

        return instance;
    }

    public AppToolDrawer getToolDrawer(AppTool tool) {
        return appToolDrawers.get(tool.getToolType());
    }
}
