package ca.ulaval.glo2004.domain.drawing.ui;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.gui.widgets.NumberInput;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.Map;

/**
 * Classe qui représente les éléments pouvant être affichés dans le panneau à drotie de l'interface utilisateur.
 */
public abstract class UIDrawer extends JPanel {
    protected final Controller controller;

    private final DocumentListener documentValueChangeListener;
    private final ActionListener actionValueChangeListener;
    private final ChangeListener valueChangeListener;

    protected final Box content;

    /**
     * Doit être mis a true avant de changer les valeurs du UI quand cela provient d'un appel backend pour ne pas créer de boucle infinie et doit être mis a valse après la modification des valeurs du UI
     */
    protected boolean isInternalUpdate = false;

    /**
     * Constructeur
     *
     * @param controller Le controlleur
     * @param topLabel   Le texte à afficher en haut du panneau, si null ne va pas ajouter du texte
     */
    public UIDrawer(Controller controller, String topLabel) {
        this.controller = controller;

        content = Box.createVerticalBox();

        documentValueChangeListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                internalUpdateReferencedObjectValues();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                internalUpdateReferencedObjectValues();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                internalUpdateReferencedObjectValues();
            }
        };

        actionValueChangeListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                internalUpdateReferencedObjectValues();
            }
        };

        valueChangeListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                internalUpdateReferencedObjectValues();
            }
        };

        internalBuildUp(topLabel);
    }

    /**
     * Initialisation et mise en place des pièces requises pour la construction du UIDrawer.
     *
     * @param topLabel Le texte à afficher en haut du panneau, null si aucun texte.
     */
    private void internalBuildUp(String topLabel) {
        setLayout(new BorderLayout());

        setBorder(new LineBorder(new Color(0, 0, 0), 2));
        setBackground(new Color(128, 183, 135));

        if (topLabel != null) {
            JLabel nameLabel = new JLabel(topLabel, SwingConstants.CENTER);
            nameLabel.setFont(new Font("Monaco", Font.BOLD, 18));
            Font font = nameLabel.getFont();
            Map attributes = font.getAttributes();
            attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            nameLabel.setFont(font.deriveFont(attributes));
            nameLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            add(nameLabel, BorderLayout.NORTH);
        }

        add(content, BorderLayout.CENTER);
    }

    /**
     * Lie les éléments des classes enfants à ceux de cette classe
     *
     * @param component L'élément à relier.
     */
    protected void registerChildComponent(JComponent component) {
        if (component instanceof NumberInput) {
            JTextComponent textComponent = ((NumberInput) component).getInput();
            PlainDocument document = (PlainDocument) textComponent.getDocument();
            document.addDocumentListener(documentValueChangeListener);
        } else if (component instanceof JComboBox) {
            JComboBox jComboBox = (JComboBox) component;
            jComboBox.addActionListener(actionValueChangeListener);
        } else if (component instanceof JCheckBox) {
            JCheckBox checkBox = (JCheckBox) component;
            checkBox.addActionListener(actionValueChangeListener);
        } else if (component instanceof JSlider) {
            JSlider slider = (JSlider) component;
            slider.addChangeListener(valueChangeListener);
        }
    }

    private void internalUpdateReferencedObjectValues() {
        if (!isInternalUpdate) {
            updateReferencedObjectValues();
        }
    }

    public void isInternalUpdate(boolean isInternalUpdate) {
        this.isInternalUpdate = isInternalUpdate;
    }

    /**
     * Indique à l'application que les valeurs affichées dans le panneau de gauche doivent être afficher de nouveau
     */
    protected void notifyForUpdatedValues() {
        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    /**
     * Appeler quand une valeur du UI a été changée par l'utilisateur
     */
    protected abstract void updateReferencedObjectValues();

    /**
     * Appeler quand le panneau de droite a changé et doit être mis à jour
     * Doit mettre la variable isInternalUpdate à true avant d'appeler la méthode et false âpres quand c'est un appel du backend
     */
    public abstract void updateUIValues();
}
