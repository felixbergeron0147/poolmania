package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;

public class WallNodeUIDrawer extends ElementUIDrawer {
    public WallNodeUIDrawer(Controller controller) {
        super(controller, "Coin de Mur", true, true);
    }

    public WallNodeUIDrawer(Controller controller, String name) {
        super(controller, name, true, false);
    }

    @Override
    public void setElementReference(Element element) {
        super.setElementReference(element);
        setDeleteBtnEnabled(controller.getBilliardTable().getNBWallNodes() > BilliardTable.MIN_NB_TABLE_WALL_NODES);
    }

    @Override
    protected void updateReferencedObjectValues() {
        super.updateReferencedObjectValues();

        notifyForUpdatedValues();
    }

    @Override
    public void updateUIValues() {
        super.updateUIValues();

        setDeleteBtnEnabled(controller.getBilliardTable().getNBWallNodes() > BilliardTable.MIN_NB_TABLE_WALL_NODES);
    }
}
