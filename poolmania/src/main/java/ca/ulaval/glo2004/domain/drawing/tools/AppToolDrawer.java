package ca.ulaval.glo2004.domain.drawing.tools;

import ca.ulaval.glo2004.domain.controller.Controller;

import java.awt.*;

public interface AppToolDrawer {
    void draw(Graphics2D g, Controller controller);
}
