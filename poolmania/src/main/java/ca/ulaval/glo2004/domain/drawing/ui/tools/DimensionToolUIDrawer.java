package ca.ulaval.glo2004.domain.drawing.ui.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.ui.UIDrawer;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.tools.TableDimensionTool;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

public class DimensionToolUIDrawer extends UIDrawer {
    private final TableDimensionTool tableDimensionTool;
    private final NumberInput widthInput;
    private final NumberInput heightInput;

    public DimensionToolUIDrawer(TableDimensionTool tableDimensionTool, Controller controller) {
        super(controller, "Dimension de la table");
        this.tableDimensionTool = tableDimensionTool;

        widthInput = new NumberInput(controller, 8, 0.0, false, null, true);
        heightInput = new NumberInput(controller, 8, 0.0, false, null, true);

        buildUp();
    }

    private void buildUp() {
        registerChildComponent(widthInput);
        content.add(WidgetBuilder.buildLabelInputPanel("Largeur", widthInput));

        registerChildComponent(heightInput);
        content.add(WidgetBuilder.buildLabelInputPanel("Hauteur", heightInput));
    }

    @Override
    protected void updateReferencedObjectValues() {
        Unit displayUnit = Controller.getDisplayUnit();

        try {
            tableDimensionTool.setTableWidth(new Quantity<>(displayUnit.toDoubleFromUnitString(widthInput.getValue()), displayUnit).toPixels());
            tableDimensionTool.setTableHeight(new Quantity<>(displayUnit.toDoubleFromUnitString(heightInput.getValue()), displayUnit).toPixels());
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
    }

    @Override
    public void updateUIValues() {
        Unit displayUnit = Controller.getDisplayUnit();
        widthInput.setValue(displayUnit.toStringValue(tableDimensionTool.getTableWidth().asUnit(displayUnit)));
        heightInput.setValue(displayUnit.toStringValue(tableDimensionTool.getTableHeight().asUnit(displayUnit)));
    }
}
