package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.*;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.AppSound;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.MathUtils;
import ca.ulaval.glo2004.geometry.structure.LinearFunction;
import ca.ulaval.glo2004.geometry.structure.UnitSpeedVector;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.util.Vector;

public class PhysicsHandler {
    private final Controller controller;

    public PhysicsHandler(Controller controller) {
        this.controller = controller;
    }

    /**
     * Mets à jour tous les éléments de la table
     *
     * @param table La table à mettre à jour
     * @return True quand aucun élément à une vélocité
     */
    public boolean update(BilliardTable table, double delta_time) {

        Vector<Element> balls = table.getElementsOfType(ElementType.BALL);
        // Collision detection

        boolean[][] collision = new boolean[balls.size()][balls.size()];
        int index1 = 0;
        SoundHandler soundHandler = controller.getSoundHandler();
        for (Element b1 : balls) {
            int index2 = 0;
            for (Element b2 : balls) {
                Ball ball1 = (Ball) b1;
                Ball ball2 = (Ball) b2;

                if (ball1 == ball2) {
                    index2++;
                    continue;
                }

                if (!collision[index1][index2] && !collision[index2][index1] && areBallsColliding(ball1, ball2)) {
                    soundHandler.playSound(AppSound.BALL_COLLISION, (float) (Math.max(ball1.getSpdVector().getSpeed(), ball2.getSpdVector().getSpeed()) / SimulationHandler.FORCE / 0.5)); // 0.5 = un coup à 50% va avoir un volume de 100%
                    separateOverlapingBalls(ball1, ball2);
                    ballBounceOnBall(ball1, ball2);
                    collision[index1][index2] = true;
                    collision[index2][index1] = true;
                }
                index2++;
            }
            index1++;
        }

        double friction = controller.getSimulationHandler().getFrictionPercent();

        // Check if still moving and move ball
        boolean finished = true;
        for (Element element : balls) {
            Ball ball = (Ball) element;

            // Check wall collisions
            for (Wall wall : table.getExteriorWalls()) {
                if (isBallCollidingWithWall(ball, wall)) {
                    //Si la balle est dans la table, on la replace, sinon on verra pour l'instant elle est perdue . . .
                    if (isInsidePolygon(table.getWallNodes(), ball.getPos().toVec2())) {
                        //Les décoller
                        separateOverlappingBallAndWall(wall, ball, Ball.getDiameter().toPixels() / 2);
                    }
                    ballBounceOnWall(ball, wall);
                }
            }

            // Check obstacle wall collisions
            for (Element obsE : table.getElementsOfType(ElementType.OBSTACLE)) {
                Obstacle obs = (Obstacle) obsE;
                for (Wall wall : obs.getCollisionWalls()) {
                    if (isBallCollidingWithWall(ball, wall)) {
                        //Si la balle est à l'extérieur de l'obstacle, on la replace, sinon on verra pour l'instant elle est perdue . . .
                        if (!isInsidePolygon(table.getWallNodes(), ball.getPos().toVec2())) {
                            //Les décoller
                            separateOverlappingBallAndWall(wall, ball, Ball.getDiameter().toPixels() / 2);
                        }
                        ballBounceOnWall(ball, wall);
                    }
                }
            }

            // Check if ball is in pocket
            for (Element pocket : table.getElementsOfType(ElementType.POCKET)) {
                if (isBallInPocket((Pocket) pocket, ball)) {
                    soundHandler.playSound(AppSound.BALL_POCKET, 0.8f);
                    table.deleteElement(ball);
                    break;
                }
            }

            // Check if all balls stopped moving
            if (ball.getSpdVector().getSpeed() > 0) {
                finished = false;
            }

            // Update ball pos;
            ball.updatePositionAndSpeed(delta_time, friction);
        }

        return finished;
    }

    public boolean areBallsColliding(Ball b1, Ball b2) {
        return b1.getPos().toPoint2DDouble().distance(b2.getPos().toPoint2DDouble()) <= Ball.getDiameter().toPixels();
    }

    private boolean isBallInPocket(Pocket pocket, Ball ball) {
        return pocket.getPos().toPoint2DDouble().distance(ball.getPos().toPoint2DDouble()) <= pocket.getDiameter().toPixels() / 2;
    }

    public boolean isBallCollidingWithWall(Ball b1, Wall w) {
        Vec2 d = Vec2.substract(w.getNode1().getPos().toVec2(), w.getNode2().getPos().toVec2());
        Vec2 f = Vec2.substract(w.getNode2().getPos().toVec2(), b1.getPos().toVec2());

        double r = Ball.getDiameter().toPixels() / 2;

        double a = d.dot(d);
        double b = f.dot(d) * 2;
        double c = f.dot(f) - Math.pow(r, 2);

        //quadratic solving (-b +- root(b^2 - 4ac)/2a)
        double dis = Math.pow(b, 2) - (4 * a * c);
        if (dis < 0)
            return false;
        else {
            //Possiblement toucher le segment, on est sur la ligne
            dis = Math.sqrt(dis);

            double t1 = (-b - dis) / (2 * a);
            double t2 = (-b + dis) / (2 * a);
            // 3x HIT cases:
            //          -o->             --|-->  |            |  --|->
            // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit),

            // 3x MISS cases:
            //       ->  o                     o ->              | -> |
            // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

            if (t1 >= 0 && t1 <= 1) {
                // t1 is the intersection, and it's closer than t2
                // (since t1 uses -b - discriminant)
                // Impale, Poke
                return true;
            }

            // here t1 didn't intersect, so we are either started
            // inside the sphere or completely past it
            // ExitWound
            return t2 >= 0 && t2 <= 1;

            // no intn: FallShort, Past, CompletelyInside
        }
    }

    private void ballBounceOnWall(Ball b1, Wall w) {
        UnitSpeedVector spdVector = b1.getSpdVector();
        if (w.isVertical()) {
            spdVector.flipX();
        } else if (w.isHorizontal()) {
            spdVector.flipY();
        } else {
            b1.setUnitSpeedVector(MathUtils.getReflectedVectorFromLinearFunction(spdVector, new LinearFunction(w.getNode1().getPos().toPoint2DDouble(), w.getNode2().getPos().toPoint2DDouble())));
        }
        spdVector.setSpeed(spdVector.getSpeed() * controller.getSimulationHandler().getReboundLossPercent());
    }

    private void ballBounceOnBall(Ball b1, Ball b2) {
        // Distance between balls
        double fDistance = Vec2.substract(b1.getPos().toVec2(), b2.getPos().toVec2()).getLength();
        // Normal
        double nx = (b2.getPos().getXPixel() - b1.getPos().getXPixel()) / fDistance;
        double ny = (b2.getPos().getYPixel() - b1.getPos().getYPixel()) / fDistance;

        // Tangent
        double tx = -ny;

        //PERSO
        double b1vx = b1.getSpdVector().getXMovement();
        double b1vy = b1.getSpdVector().getYMovement();
        double b2vx = b2.getSpdVector().getXMovement();
        double b2vy = b2.getSpdVector().getYMovement();

        // Dot Product Tangent
        double dpTan1 = b1vx * tx + b1vy * nx;
        double dpTan2 = b2vx * tx + b2vy * nx;

        // Dot Product Normal
        double dpNorm1 = b1vx * nx + b1vy * ny;

        // Conservation of momentum in 1D
        double m1 = b2vx * nx + b2vy * ny;

        //PERSO
        b1vx = tx * dpTan1 + nx * m1;
        b1vy = nx * dpTan1 + ny * m1;

        b2vx = tx * dpTan2 + nx * dpNorm1;
        b2vy = nx * dpTan2 + ny * dpNorm1;

        // Update ball velocities
        double reboundLoss = controller.getSimulationHandler().getReboundLossPercent();
        b1.getSpdVector().setMovement(new Vec2(b1vx * reboundLoss, b1vy * reboundLoss));
        b2.getSpdVector().setMovement(new Vec2(b2vx * reboundLoss, b2vy * reboundLoss));
    }

    private void separateOverlapingBalls(Ball b1, Ball b2) {
        //Crash si les balles n'overlap pas mais je vous fait confiance
        double fDistance = Vec2.substract(b1.getPos().toVec2(), b2.getPos().toVec2()).getLength();
        double fOverlap = (fDistance - (Ball.getDiameter().toPixels())) / 2;

        double px1 = b1.getPos().getXPixel();
        double py1 = b1.getPos().getYPixel();
        double px2 = b2.getPos().getXPixel();
        double py2 = b2.getPos().getYPixel();

        px1 -= fOverlap * (px1 - px2) / fDistance;
        py1 -= fOverlap * (py1 - py2) / fDistance;

        b1.setXPos(px1);
        b1.setYPos(py1);

        px2 += fOverlap * (px1 - px2) / fDistance;
        py2 += fOverlap * (py1 - py2) / fDistance;

        b2.setXPos(px2);
        b2.setYPos(py2);
    }

    private boolean isInsidePolygon(Vector<WallNode> nodes, Vec2 p) {
        Polygon2D polygon2D = MathUtils.createPolygonFromWallNodes(nodes);
        return polygon2D.contains(p.getX(), p.getY());
    }

    private void separateOverlappingBallAndWall(Wall wall, Ball ball, double circleRadius) {
        Vec2 wallP1 = wall.getNode1().getPos().toVec2();
        Vec2 wallP2 = wall.getNode2().getPos().toVec2();
        Vec2 circlePos = ball.getPos().toVec2();
        Vec2 closestPoint = MathUtils.getPointOnLineClosestToAnotherPoint(wallP1, Vec2.substract(wallP1, wallP2), circlePos);
        Vec2 pWallVec = Vec2.substract(circlePos, closestPoint);
        double transVecLen = circleRadius - pWallVec.getLength();
        pWallVec.normalize();
        Vec2 transVec = Vec2.multi(pWallVec, transVecLen);
        ball.getPos().translate(transVec);
    }
}
