package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.ControllerObserverType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class SelectionTool extends AppTool {
    public SelectionTool(Controller controller) {
        super(controller, AppToolType.SELECTION);
    }

    @Override
    public void onEnable() {
        Point mousePosition = controller.getDrawingPanel().getMousePosition();
        if (mousePosition != null) {
            controller.getSelectionHandler().updateMouseHoveredPosition(mousePosition.x, mousePosition.y);
        }
    }

    @Override
    public void onDisable() {}

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            controller.getSelectionHandler().switchSelection(e.getX(), e.getY());
            if (controller.getSelectionHandler().getSelectedElement() != null) {
                controller.getUndoRedoHandler().pushEditionChange();
            }
        }

        if (SwingUtilities.isRightMouseButton(e)) {
            controller.getSelectionHandler().removeSelectedElement();
            controller.getRightPanel().setUiDrawer(null);
            controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
            controller.notifyObservers(ControllerObserverType.DRAWING);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            controller.getSelectionHandler().updateSelectedPositionFromMouse(e.getPoint().x, e.getPoint().y);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        controller.getSelectionHandler().updateMouseHoveredPosition(e.getPoint().x, e.getPoint().y);
    }
}
