package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.elements.Wall;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

public class WallUIDrawer extends ElementUIDrawer {
    private final WallNodeUIDrawer node1UIDrawer;
    private final WallNodeUIDrawer node2UIDrawer;

    private final NumberInput wallThickness;

    public WallUIDrawer(Controller controller) {
        super(controller, "Mur", false, false);

        node1UIDrawer = new WallNodeUIDrawer(controller, "Coin 1");
        node2UIDrawer = new WallNodeUIDrawer(controller, "Coin 2");

        wallThickness = new NumberInput(controller, 8, 0.5, false);

        buildUp();
    }

    private void buildUp() {
        node1UIDrawer.setBorder(null);
        node2UIDrawer.setBorder(null);

        content.add(node1UIDrawer);
        content.add(node2UIDrawer);

        registerChildComponent(wallThickness);
        content.add(WidgetBuilder.buildLabelInputPanel("Épaisseur des murs", wallThickness));
    }

    @Override
    public void setElementReference(Element element) {
        Wall wall = (Wall) element;
        node1UIDrawer.setElementReference(wall.getNode1());
        node2UIDrawer.setElementReference(wall.getNode2());

        super.setElementReference(element);
    }

    @Override
    public void isInternalUpdate(boolean isInternalUpdate) {
        super.isInternalUpdate(isInternalUpdate);

        node1UIDrawer.isInternalUpdate(isInternalUpdate);
        node2UIDrawer.isInternalUpdate(isInternalUpdate);
    }

    @Override
    protected void updateReferencedObjectValues() {
        super.updateReferencedObjectValues();

        Unit displayUnit = Controller.getDisplayUnit();
        try {
            Wall.setWallThickness(new Quantity<>(displayUnit.toDoubleFromUnitString(wallThickness.getValue()), displayUnit));
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
    }

    @Override
    public void updateUIValues() {
        super.updateUIValues();

        node1UIDrawer.updateUIValues();
        node2UIDrawer.updateUIValues();

        Unit displayUnit = Controller.getDisplayUnit();
        wallThickness.setValue(displayUnit.toStringValue(Wall.getWallThickness().value()));
    }
}
