package ca.ulaval.glo2004.domain.factories;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.*;
import ca.ulaval.glo2004.domain.handlers.DrawingPanelViewHandler;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;

import java.awt.geom.Point2D;

public class ElementFactory {
    private static ElementFactory instance = null;

    private ElementFactory() {}

    public static ElementFactory getInstance() {
        if (instance == null) {
            instance = new ElementFactory();
        }

        return instance;
    }

    /**
     * Méthode qui crée l'objet de type Element qu centre de l'écran
     *
     * @param type       Le type de l'objet Element qui sera créé
     * @param controller Le contrôleur
     * @return Le nouvel Element
     */
    public Element createElementAtCenter(ElementType type, Controller controller) {
        DrawingPanel drawingPanel = controller.getDrawingPanel();
        DrawingPanelViewHandler drawingPanelViewHandler = controller.getDrawingPanelViewHandler();
        double zoom = drawingPanelViewHandler.getZoom();
        Point2D.Double centerPos = new Point2D.Double((drawingPanel.getWidth() / 2.0), (drawingPanel.getHeight() / 2.0));
        centerPos.x = centerPos.x / zoom - drawingPanelViewHandler.getTopLeftXPos();
        centerPos.y = centerPos.y / zoom - drawingPanelViewHandler.getTopLeftYPos();

        Element element = createElement(type);
        if (element != null) {
            element.setPosition(centerPos);
        }
        return element;
    }

    public Element duplicateElement(Element element) {
        Element newElement = createElement(element.getElementType());

        switch (newElement.getElementType()) {
            case WALL_NODE: {
                newElement.setPosition(element.getPos());
                break;
            }
            case POCKET: {
                Pocket p = (Pocket) newElement;
                Pocket oldP = (Pocket) element;
                newElement.setPosition(element.getPos());
                p.setDiameter(oldP.getDiameter());
                break;
            }
            case OBSTACLE: {
                Obstacle o = (Obstacle) newElement;
                Obstacle oldO = (Obstacle) element;
                newElement.setPosition(element.getPos());
                o.setWidth(oldO.getWidth());
                o.setHeight(oldO.getHeight());
                o.setRotation(oldO.getRotation());
                break;
            }
            case WHITE_BALL:
            case BALL: {
                Ball b = (Ball) newElement;
                Ball oldB = (Ball) element;
                b.setPosition(oldB.getPos());
                b.setBallColor(oldB.getColor());
                b.setStriped(oldB.isStriped());
                b.setIsWhite(oldB.isWhiteBall());
                break;
            }
        }

        return newElement;
    }

    private Element createElement(ElementType type) {
        Element retval = null;

        switch (type) {
            case WALL_NODE: {
                retval = new WallNode();
                break;
            }
            case BALL: {
                retval = new Ball(false);
                break;
            }
            case WHITE_BALL: {
                retval = new Ball(true);
                break;
            }
            case POCKET: {
                retval = new Pocket();
                break;
            }
            case OBSTACLE: {
                retval = new Obstacle();
                break;
            }
        }

        return retval;
    }
}
