package ca.ulaval.glo2004.domain.drawing;

import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Wall;
import ca.ulaval.glo2004.domain.elements.WallNode;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.geometry.MathUtils;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Vector;

public class TableDrawer {
    public static final Color WALL_COLOR = new Color(72, 37, 10);
    public static final Color BORDER_COLOR = new Color(178, 136, 109);
    public static final int WALL_BORDER_WIDTH = 2;

    public static final Color TABLE_BG_COLOR = new Color(13, 109, 17);
    public static final Color TABLE_BORDER_COLOR = new Color(25, 174, 49);
    public static final int TABLE_BORDER_WIDTH = 4;

    public static final Color GUIDE_DOT_COLOR = new Color(222, 222, 222);
    private static final Quantity<Imperial> GUIDE_DOT_DIAMETER = new Quantity<>(0.5, Imperial.get());

    private static TableDrawer instance = null;

    private TableDrawer() {}

    public static TableDrawer getInstance() {
        if (instance == null) {
            instance = new TableDrawer();
        }

        return instance;
    }

    public void draw(Graphics2D g, BilliardTable billiardTable) {
        // Murs de la table
        Vector<Wall> walls = billiardTable.getExteriorWalls();
        for (int i = 0; i < walls.size(); i++) {
            Wall nextWall = walls.elementAt((i + 1) % walls.size());
            int previousIndex = i - 1;
            if (previousIndex < 0) {
                previousIndex = walls.size() - 1;
            }
            Wall previousWall = walls.elementAt(previousIndex);

            Vector<Polygon2D> polygons = getWallPolygons(walls.elementAt(i), previousWall, nextWall);

            for (Polygon2D polygon : polygons) {
                g.setColor(WALL_COLOR);
                g.fill(polygon);

                g.setStroke(new BasicStroke(WALL_BORDER_WIDTH, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
                g.setColor(BORDER_COLOR);
                g.draw(polygon);
            }

            // Points de référence
            g.setColor(GUIDE_DOT_COLOR);
            for (Polygon2D polygon : getWallGuidePoints(walls.elementAt(i))) {
                g.fill(polygon);
            }
        }

        // Fond de table
        Polygon2D tableBackground = MathUtils.createPolygonFromWallNodes(billiardTable.getWallNodes());
        g.setColor(TABLE_BG_COLOR);
        g.fill(tableBackground);
        g.setStroke(new BasicStroke(TABLE_BORDER_WIDTH));
        g.setColor(TABLE_BORDER_COLOR);
        g.draw(tableBackground);
    }

    /**
     * Retourne les polygones nécessaire pour dessiner un mur soir le mur et au maximum 2 coins
     *
     * @param wall         Le mur voulu
     * @param previousWall Le mur prévendant le mur voulu
     * @param nextWall     Le prochain mur du mur voulu
     * @return Un vecteur de polygone du mur et de ses coins
     */
    public Vector<Polygon2D> getWallPolygons(Wall wall, Wall previousWall, Wall nextWall) {
        WallNode node1 = wall.getNode1();
        WallNode node2 = wall.getNode2();

        int wallThicknessPixels = (int) Wall.getWallThicknessPixels();

        Vec2 wallVector = wall.getNormalizedWallVector().multi(wallThicknessPixels);

        Vec2 perpendicularVector = wall.getNormalizedPerpendicularVector().multi(wallThicknessPixels);

        // Points intérieurs
        Vec2 p1 = new Vec2(node1.getPos().getXPixel(), node1.getPos().getYPixel());
        Vec2 p3 = new Vec2(node2.getPos().getXPixel(), node2.getPos().getYPixel());

        // Points extérieurs
        Vec2 p2 = new Vec2(node1.getPos().getXPixel() + (perpendicularVector.getX() + wallVector.getX()), node1.getPos().getYPixel() + (perpendicularVector.getY() + wallVector.getY()));
        Vec2 p4 = new Vec2(node2.getPos().getXPixel() + (perpendicularVector.getX() - wallVector.getX()), node2.getPos().getYPixel() + (perpendicularVector.getY() - wallVector.getY()));

        Vec2 nextWallVector = nextWall.getNormalizedWallVector().multi(wallThicknessPixels);
        Vec2 nextWallPerpendicular = nextWall.getNormalizedPerpendicularVector().multi(wallThicknessPixels);

        Vec2 previousWallVector = previousWall.getNormalizedWallVector().multi(wallThicknessPixels);
        Vec2 previousWallPerpendicular = previousWall.getNormalizedPerpendicularVector().multi(wallThicknessPixels);

        Vec2 originOtherP2 = new Vec2(nextWall.getNode1().getPos().getXPixel() + nextWallPerpendicular.getX(), nextWall.getNode1().getPos().getYPixel() + nextWallPerpendicular.getY());
        Vec2 otherP2 = new Vec2(nextWall.getNode1().getPos().getXPixel() + (nextWallPerpendicular.getX() + nextWallVector.getX()), nextWall.getNode1().getPos().getYPixel() + (nextWallPerpendicular.getY() + nextWallVector.getY()));

        Vec2 originP4 = new Vec2(node2.getPos().getXPixel() + perpendicularVector.getX(), node2.getPos().getYPixel() + perpendicularVector.getY());

        Vec2 p4IntersectPos = MathUtils.getIntersectionPosition(originP4, p4, originOtherP2, otherP2);


        Vec2 originOtherP4 = new Vec2(previousWall.getNode2().getPos().getXPixel() + previousWallPerpendicular.getX(), previousWall.getNode2().getPos().getYPixel() + previousWallPerpendicular.getY());
        Vec2 otherP4 = new Vec2(previousWall.getNode2().getPos().getXPixel() + (previousWallPerpendicular.getX() - previousWallVector.getX()), previousWall.getNode2().getPos().getYPixel() + (previousWallPerpendicular.getY() - previousWallVector.getY()));

        Vec2 originP2 = new Vec2(node1.getPos().getXPixel() + perpendicularVector.getX(), node1.getPos().getYPixel() + perpendicularVector.getY());

        Vec2 p2IntersectPos = MathUtils.getIntersectionPosition(originP2, p2, originOtherP4, otherP4);

        double previousWallAngle = Math.toDegrees(previousWallVector.angle(wallVector.inverse()));
        double nextWallAngle = Math.toDegrees(wallVector.angle(nextWallVector.inverse()));

        Vector<Point2D.Double> wallPoints = new Vector<>();
        wallPoints.add(new Point2D.Double(p1.getX(), p1.getY()));
        wallPoints.add(new Point2D.Double(p3.getX(), p3.getY()));
        Vector<Vector<Point2D.Double>> wallEdgePointsP3 = getWallEdgePoints(p4IntersectPos, nextWallAngle, p3, p4, otherP2, perpendicularVector);
        wallPoints.addAll(wallEdgePointsP3.elementAt(0));
        Vector<Vector<Point2D.Double>> wallEdgePointsP1 = getWallEdgePoints(p2IntersectPos, previousWallAngle, p1, p2, otherP4, perpendicularVector);
        wallPoints.addAll(wallEdgePointsP1.elementAt(0));

        Vector<Polygon2D> polygons = new Vector<>();

        if (!wallEdgePointsP3.elementAt(1).isEmpty()) {
            Polygon2D corner = new Polygon2D();
            for (Point2D.Double cornerPoint : wallEdgePointsP3.elementAt(1)) {
                corner.addPoint(cornerPoint);
            }
            polygons.add(corner);
        }

        if (!wallEdgePointsP1.elementAt(1).isEmpty()) {
            Polygon2D corner = new Polygon2D();
            for (Point2D.Double cornerPoint : wallEdgePointsP1.elementAt(1)) {
                corner.addPoint(cornerPoint);
            }
            polygons.add(corner);
        }

        Polygon2D wallPolygon = new Polygon2D();
        for (Point2D.Double wallPoint : wallPoints) {
            wallPolygon.addPoint(wallPoint);
        }

        polygons.add(wallPolygon);
        return polygons;
    }

    private Vector<Vector<Point2D.Double>> getWallEdgePoints(Vec2 intersectingPos, double wallAngle, Vec2 innerPos, Vec2 edgeOuterExtendedPos, Vec2 otherWallEdgeOuterPos, Vec2 wallPerpendicularVector) {
        Vector<Point2D.Double> wall = new Vector<>();
        Vector<Point2D.Double> corner = new Vector<>();
        if (intersectingPos != null && !MathUtils.areClose(wallAngle, 180, 0.1) && !MathUtils.areClose(wallAngle, -180, 0.1)) {
            double distanceIntersection = innerPos.distance(intersectingPos);
            double distanceOuterPos = innerPos.distance(edgeOuterExtendedPos);
            if (distanceIntersection < distanceOuterPos && wallAngle > 0) {
                wall.add(new Point2D.Double(intersectingPos.getX(), intersectingPos.getY()));
            } else {
                if (wallAngle > 90 || (wallAngle > -90 && wallAngle < 0) || wallAngle < -90) {
                    wall.add(new Point2D.Double(intersectingPos.getX(), intersectingPos.getY()));
                } else {
                    wall.add(new Point2D.Double(edgeOuterExtendedPos.getX(), edgeOuterExtendedPos.getY()));

                    corner.add(new Point2D.Double(edgeOuterExtendedPos.getX(), edgeOuterExtendedPos.getY()));
                    corner.add(new Point2D.Double(otherWallEdgeOuterPos.getX(), otherWallEdgeOuterPos.getY()));
                    corner.add(new Point2D.Double(innerPos.getX(), innerPos.getY()));
                }
            }
        } else {
            wall.add(new Point2D.Double(innerPos.getX() + wallPerpendicularVector.getX(), innerPos.getY() + wallPerpendicularVector.getY()));
        }

        Vector<Vector<Point2D.Double>> points = new Vector<>();
        points.add(wall);
        points.add(corner);
        return points;
    }

    public Vector<Polygon2D> getWallGuidePoints(Wall wall) {
        WallNode node1 = wall.getNode1();
        WallNode node2 = wall.getNode2();
        Vec2 wallVector = wall.getNormalizedWallVector();
        Vec2 perpendicularVector = wall.getNormalizedPerpendicularVector();
        int wallThicknessPixels = (int) Wall.getWallThicknessPixels();

        final Quantity<Imperial> minDistance = new Quantity<>(12, Imperial.get());
        final double minDistancePixels = minDistance.toPixels();
        Vector<Polygon2D> guideDots = new Vector<>();

        double distance = node1.getPos().toPoint2DDouble().distance(node2.getPos().toPoint2DDouble());
        if (distance > minDistancePixels) {
            double nbDots = distance / minDistancePixels;
            double newMinDistance = distance / nbDots;

            for (int i = 0; i < nbDots / 2; i++) {
                int sign = 1;
                for (int j = 0; j < 2; j++) {
                    Point2D.Double midWallPos = wall.getPos().toPoint2DDouble();
                    double x = midWallPos.x + sign * wallVector.getX() * newMinDistance * i + (perpendicularVector.getX() * wallThicknessPixels / 2);
                    double y = midWallPos.y + sign * wallVector.getY() * newMinDistance * i + (perpendicularVector.getY() * wallThicknessPixels / 2);
                    sign *= -1;

                    Polygon2D guideDot = new Polygon2D();
                    int nbPoints = 30;
                    double radiusPixel = GUIDE_DOT_DIAMETER.toPixels() / 2;
                    double segmentWidth = Math.PI * 2 / nbPoints;
                    double angle = 0;
                    for (int k = 0; k < nbPoints; ++k) {
                        guideDot.addPoint((float) ((Math.cos(angle) * radiusPixel) + x), (float) ((Math.sin(angle) * radiusPixel) + y));
                        angle -= segmentWidth;
                    }

                    guideDots.add(guideDot);
                }
            }
        }

        return guideDots;
    }
}
