package ca.ulaval.glo2004.domain.drawing.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Wall;
import ca.ulaval.glo2004.domain.tools.BreakWallTool;

import java.awt.*;
import java.awt.geom.Point2D;

public class BreakWallToolDrawer implements AppToolDrawer {
    @Override
    public void draw(Graphics2D g, Controller controller) {
        BreakWallTool tool = (BreakWallTool) controller.getCurrentTool();
        Wall hoveredWall = tool.getHoveredWall();
        if (hoveredWall != null) {
            g.setColor(new Color(155, 0, 0));
            g.setStroke(new BasicStroke(2));
            g.draw(hoveredWall.getSelectionBox());
        }

        Point2D.Double breakPos = tool.getBreakPos();
        if (breakPos != null) {
            g.setColor(Color.red);
            final int breakWallPosIndicatorRadius = 8;
            g.fillOval((int) breakPos.getX() - breakWallPosIndicatorRadius, (int) breakPos.getY() - breakWallPosIndicatorRadius, breakWallPosIndicatorRadius * 2, breakWallPosIndicatorRadius * 2);
        }
    }
}
