package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.tools.*;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.gui.listeners.action.ChangeToolAction;
import ca.ulaval.glo2004.gui.listeners.action.DeleteSelectedElementAction;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

public class AppToolsHandler {
    private final Controller controller;

    private final Map<AppToolType, AppTool> appTools;

    private AppTool currentTool;

    public AppToolsHandler(Controller controller) {
        this.controller = controller;
        appTools = new HashMap<>();

        appTools.put(AppToolType.SELECTION, new SelectionTool(controller));
        appTools.put(AppToolType.BREAK_WALL, new BreakWallTool(controller));
        appTools.put(AppToolType.TABLE_DIMENSION, new TableDimensionTool(controller));
        appTools.put(AppToolType.SIMULATION, new SimulationTool(controller));
        appTools.put(AppToolType.PLACE_WHITE_BALL, new PlaceWhiteBallTool(controller));

        currentTool = appTools.get(AppToolType.SELECTION);
    }

    public void setup() {
        KeyBindHandler keyBindHandler = controller.getKeyBindHandler();

        keyBindHandler.addKeyBind("use_break_wall", KeyStroke.getKeyStroke(KeyEvent.VK_B, 0), new ChangeToolAction(controller, AppToolType.BREAK_WALL));
        keyBindHandler.addKeyBind("table_dimension_enable", KeyStroke.getKeyStroke(KeyEvent.VK_D, 0), new ChangeToolAction(controller, AppToolType.TABLE_DIMENSION));
        keyBindHandler.addKeyBind("delete_selected", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), new DeleteSelectedElementAction(controller));
    }

    public AppTool getCurrentTool() {
        return currentTool;
    }

    public void setTool(AppToolType type) {
        if (currentTool.getToolType() == type || type.getAssociatedMode() != controller.getCurrentMode()) {
            return;
        }

        AppTool tool = appTools.get(type);
        if (tool != null) {
            currentTool.onDisable();
            currentTool = tool;
            currentTool.onEnable();

            controller.notifyObservers(ControllerObserverType.DRAWING);
            controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
        }
    }
}
