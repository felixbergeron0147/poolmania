package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.gui.listeners.action.RedoAction;
import ca.ulaval.glo2004.gui.listeners.action.UndoAction;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Stack;
import java.util.Vector;

public class UndoRedoHandler {
    private final Controller controller;

    ///region Undo/Redo pour le mode Édition
    private final Stack<BilliardTable> editionTableStates;
    private final Stack<BilliardTable> redoEditionTableStates;

    public UndoRedoHandler(Controller controller) {
        this.controller = controller;

        editionTableStates = new Stack<>();
        redoEditionTableStates = new Stack<>();

        simTableStates = new Stack<>();
        simRedoTableStates = new Stack<>();
    }

    public void setUp() {
        controller.getKeyBindHandler().addKeyBind("undo", KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK), new UndoAction(controller));
        controller.getKeyBindHandler().addKeyBind("redo", KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.SHIFT_DOWN_MASK | InputEvent.CTRL_DOWN_MASK), new RedoAction(controller));
    }

    /**
     * Ajoute une action dans le undo stack
     * IMPORTANT: Appeler avant de faire un changement
     */
    public void pushEditionChange() {
        if (controller.getCurrentMode() == Mode.EDITION) {
            editionTableStates.push(new BilliardTable(controller.getBilliardTable()));
            redoEditionTableStates.clear();
        }
    }

    public void redoEditionChange() {
        if (controller.getCurrentMode() == Mode.EDITION && !redoEditionTableStates.isEmpty()) {
            BilliardTable pop = redoEditionTableStates.pop();
            controller.setBilliardTable(pop);
            editionTableStates.push(new BilliardTable(pop));
        }
    }

    public void undoEditionChange() {
        if (controller.getCurrentMode() == Mode.EDITION && !editionTableStates.empty()) {
            redoEditionTableStates.push(new BilliardTable(controller.getBilliardTable()));
            BilliardTable pop = editionTableStates.pop();
            controller.setBilliardTable(pop);
        }
    }

    public void resetEditionStates() {
        editionTableStates.clear();
        redoEditionTableStates.clear();
        pushEditionChange();
    }

    ///endregion

    ///region Undo/Redo pour le mode Simulation

    private final Stack<Vector<Element>> simTableStates;
    private final Stack<Vector<Element>> simRedoTableStates;

    public void pushSimState() {
        Vector<Element> v = new Vector<>();
        for (Element e : controller.getBilliardTable().getElementsOfType(ElementType.BALL)) {
            v.add(e.copy());
        }
        simTableStates.push(v);
    }

    public void pushSimRedoState() {
        Vector<Element> v = new Vector<>();
        for (Element e : controller.getBilliardTable().getElementsOfType(ElementType.BALL)) {
            v.add(e.copy());
        }
        simRedoTableStates.push(v);
    }

    public void pushSimState(Vector<Element> tableState) {
        Vector<Element> v = new Vector<>();
        for (Element e : tableState) {
            v.add(e.copy());
        }
        simTableStates.push(v);
    }

    public void pushSimRedoState(Vector<Element> tableState) {
        Vector<Element> v = new Vector<>();
        for (Element e : tableState) {
            v.add(e.copy());
        }
        simRedoTableStates.push(v);
    }

    public Vector<Element> popSimState() {
        return simTableStates.pop();
    }

    public Vector<Element> popSimRedoState() {
        return simRedoTableStates.pop();
    }

    public void clearSimStates() {
        simTableStates.clear();
    }

    public void clearSimRedoStates() {
        simRedoTableStates.clear();
    }

    public Stack<Vector<Element>> getSimTableStates() {
        return simTableStates;
    }

    public Stack<Vector<Element>> getSimRedoTableStates() {
        return simRedoTableStates;
    }

    ///endregion
}
