package ca.ulaval.glo2004.domain.drawing.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.tools.TableDimensionTool;

import java.awt.*;
import java.awt.geom.Point2D;

public class DimensionTableToolDrawer implements AppToolDrawer {
    public DimensionTableToolDrawer() {}

    @Override
    public void draw(Graphics2D g, Controller controller) {
        TableDimensionTool tool = (TableDimensionTool) controller.getCurrentTool();
        BilliardTable table = controller.getBilliardTable();
        Point2D.Double centerPos = table.getCenterPos();
        Rectangle boundsRect = table.getBoundsRect();

        g.setColor(Color.red);
        g.setStroke(new BasicStroke(2));
        g.drawRect((int) (centerPos.x + boundsRect.getX()), (int) (centerPos.y + boundsRect.getY()), boundsRect.width, boundsRect.height);

        for (Rectangle rect : tool.getBoundRectCorners()) {
            g.fill(rect);
        }

        Unit displayUnit = Controller.getDisplayUnit();
        String widthText = displayUnit.toStringValue(tool.getTableWidth().asUnit(displayUnit));
        String heightText = displayUnit.toStringValue(tool.getTableHeight().asUnit(displayUnit));

        g.setColor(new Color(163, 51, 255));
        g.setFont(new Font("Monaco", Font.BOLD, 20));
        FontMetrics metrics = g.getFontMetrics();

        g.drawString(widthText, (int) (boundsRect.getX() + (boundsRect.getWidth() - metrics.stringWidth(widthText)) / 2 + centerPos.getX()), (int) (centerPos.getY() + boundsRect.getY() - 10));
        g.drawString(heightText, (int) (boundsRect.getX() + boundsRect.getWidth() + centerPos.getX() + 10), (int) (centerPos.getY() + boundsRect.getY() + ((boundsRect.getHeight() - metrics.getHeight()) / 2) + metrics.getAscent()));
    }
}
