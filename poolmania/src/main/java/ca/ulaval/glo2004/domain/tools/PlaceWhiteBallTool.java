package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.elements.Wall;
import ca.ulaval.glo2004.domain.handlers.PhysicsHandler;
import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.CursorType;
import ca.ulaval.glo2004.enums.ElementType;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class PlaceWhiteBallTool extends AppTool {
    public PlaceWhiteBallTool(Controller controller) {
        super(controller, AppToolType.PLACE_WHITE_BALL);
    }

    @Override
    public void onEnable() {
        controller.getCursorHandler().setCursor(CursorType.WHITE_BALL_BAD);
    }

    @Override
    public void onDisable() {
        controller.getCursorHandler().setCursor(CursorType.DEFAULT);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            Ball wouldBeWhiteBall = canPlaceBall(e);
            if (wouldBeWhiteBall != null) {
                controller.getBilliardTable().addElement(wouldBeWhiteBall);
                controller.getSimulationHandler().updatePlaceBallUI();
                controller.resetToDefaultTool();
                controller.getCursorHandler().setCursor(CursorType.DEFAULT);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        controller.getCursorHandler().setCursor(canPlaceBall(e) != null ? CursorType.WHITE_BALL_GOOD : CursorType.WHITE_BALL_BAD);
    }

    private Ball canPlaceBall(MouseEvent e) {
        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(e.getX(), e.getY());
        BilliardTable billiardTable = controller.getBilliardTable();
        if (billiardTable.isPointInTable(mousePos.getX(), mousePos.getY())) {
            SimulationHandler simulationHandler = controller.getSimulationHandler();
            PhysicsHandler physicsHandler = simulationHandler.getPhysicsHandler();
            Ball wouldBeWhitBall = new Ball(true);
            wouldBeWhitBall.setPosition(mousePos);

            boolean valid = true;
            for (Wall wall : billiardTable.getExteriorWalls()) {
                if (physicsHandler.isBallCollidingWithWall(wouldBeWhitBall, wall)) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                for (Element ball : billiardTable.getElementsOfType(ElementType.BALL)) {
                    if (physicsHandler.areBallsColliding(wouldBeWhitBall, (Ball) ball)) {
                        valid = false;
                        break;
                    }
                }
            }

            // Todo check if over obstacle

            return valid ? wouldBeWhitBall : null;
        }
        return null;
    }
}
