package ca.ulaval.glo2004.domain.elements;


import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.MathUtils;
import ca.ulaval.glo2004.geometry.Position;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.awt.geom.Point2D;

/**
 * Classe utilisée pour le traitement de deux WallNodes
 */
public class Wall extends Element {
    private static final Quantity<Imperial> wallThickness = new Quantity<>(4, Imperial.get());
    private final WallNode node1;
    private final WallNode node2;

    /**
     * Constructeur de la classe Wall
     *
     * @param p1 La premiere node du Wall
     * @param p2 La deuxième node du Wall
     */
    public Wall(WallNode p1, WallNode p2) {
        super(ElementType.WALL);
        node1 = p1;
        node2 = p2;
    }

    /**
     * Constructeur de copie de la classe Wall
     *
     * @param wall Le Wall a copier
     */
    public Wall(Wall wall) {
        super(ElementType.WALL);
        node1 = new WallNode(wall.node1);
        node2 = new WallNode(wall.node2);
    }

    public static Quantity<Imperial> getWallThickness() {
        return wallThickness;
    }

    public static <U extends Unit> void setWallThickness(Quantity<U> wallThickness) {
        Wall.wallThickness.set(wallThickness);
    }

    public static double getWallThicknessPixels() {
        return wallThickness.toPixels();
    }

    public WallNode getNode1() {
        return node1;
    }

    public WallNode getNode2() {
        return node2;
    }

    @Override
    public void setPosition(Point2D.Double position) {
        Position newPos = new Position(position.getX(), position.getY());
        Position pos = getPos();
        double newX = newPos.getXPixel() - pos.getXPixel();
        double newY = newPos.getYPixel() - pos.getYPixel();
        Position translationTodo = new Position(newX, newY);

        node1.position.translate(translationTodo);
        node2.position.translate(translationTodo);
    }

    @Override
    public Polygon2D getSelectionBox() {
        Vec2 perpendicularVector = getNormalizedPerpendicularVector();

        perpendicularVector = Vec2.multi(perpendicularVector, wallThickness.toPixels());
        Point2D.Double p1 = new Point2D.Double(node1.getPos().getXPixel(), node1.getPos().getYPixel());
        Point2D.Double p2 = new Point2D.Double(node1.getPos().getXPixel() + perpendicularVector.getX(), node1.getPos().getYPixel() + perpendicularVector.getY());
        Point2D.Double p3 = new Point2D.Double(node2.getPos().getXPixel(), node2.getPos().getYPixel());
        Point2D.Double p4 = new Point2D.Double(node2.getPos().getXPixel() + perpendicularVector.getX(), node2.getPos().getYPixel() + perpendicularVector.getY());

        Polygon2D box = new Polygon2D();
        box.addPoint((float) p1.x, (float) p1.y);
        box.addPoint((float) p3.x, (float) p3.y);
        box.addPoint((float) p4.x, (float) p4.y);
        box.addPoint((float) p2.x, (float) p2.y);
        return box;
    }

    public Vec2 getNormalizedPerpendicularVector() {
        Vec2 wallVector = getNormalizedWallVector();
        return new Vec2(wallVector.getY(), -wallVector.getX()).normalize();
    }

    public Vec2 getNormalizedWallVector() {
        return new Vec2(node1.getPos().getXPixel() - node2.getPos().getXPixel(), node1.getPos().getYPixel() - node2.getPos().getYPixel()).normalize();
    }

    public boolean intersectsWithWall(Wall w) {
        double x1 = node1.getPos().getXValue();
        double x2 = node2.getPos().getXValue();
        double x3 = w.getNode1().getPos().getXValue();
        double x4 = w.getNode2().getPos().getXValue();
        double y1 = node1.getPos().getYPixel();
        double y2 = node2.getPos().getYPixel();
        double y3 = w.getNode1().getPos().getYPixel();
        double y4 = w.getNode2().getPos().getYPixel();
        double denominator = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        double t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denominator;
        double u = ((x1 - x3) * (y1 - y2) - (y1 - y3) * (x1 - x2)) / denominator;
        return 0 <= t && t <= 1.0 && 0 <= u && u <= 1.0;
    }

    /**
     * Retourne la position au milieu des deux sommets du mur
     *
     * @return La position au milieu des deux sommets du mur
     */
    @Override
    public Position getPos() {
        Point2D.Double node1Pos = node1.getPos().toPoint2DDouble();
        Point2D.Double node2Pos = node2.getPos().toPoint2DDouble();
        return new Position((node1Pos.x + node2Pos.x) / 2, (node1Pos.y + node2Pos.y) / 2);
    }

    public Vec2 getNearestBreakPos(Point2D.Double mousePos) {
        return MathUtils.getIntersectionPosition(new Vec2(mousePos.getX(), mousePos.getY()), Vec2.add(getNormalizedPerpendicularVector(), new Vec2(mousePos.getX(), mousePos.getY())), node1.getPos().toVec2(), node2.getPos().toVec2());
    }

    public boolean isVertical() {
        return (node1.getPos().getXValue() == node2.getPos().getXValue());
    }

    public boolean isHorizontal() {
        return (node1.getPos().getYValue() == node2.getPos().getYValue());
    }

    /**
     * Fait une "deep" copie de Wall
     *
     * @return La copie de Wall
     */
    @Override
    public Element copy() {
        return new Wall(this);
    }

}
