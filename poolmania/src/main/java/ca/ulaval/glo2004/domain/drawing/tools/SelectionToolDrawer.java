package ca.ulaval.glo2004.domain.drawing.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.handlers.SelectionHandler;

import java.awt.*;

public class SelectionToolDrawer implements AppToolDrawer {
    @Override
    public void draw(Graphics2D g, Controller controller) {
        SelectionHandler selectionHandler = controller.getSelectionHandler();

        Element hoveredElement = selectionHandler.getHoveredElement();
        if (hoveredElement != null) {
            g.setColor(new Color(155, 0, 0));
            g.setStroke(new BasicStroke(2));
            g.draw(hoveredElement.getSelectionBox());
        }

        Element selectedElement = selectionHandler.getSelectedElement();
        if (selectedElement != null) {
            g.setColor(Color.red);
            g.setStroke(new BasicStroke(2));
            g.draw(selectedElement.getSelectionBox());
        }
    }
}
