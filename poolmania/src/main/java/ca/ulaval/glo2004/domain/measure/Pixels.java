package ca.ulaval.glo2004.domain.measure;

public class Pixels extends Unit {
    private static Pixels instance = null;

    private Pixels() {
        super(1, "px"); // 1 px = 1 pixel
    }

    public static Pixels get() {
        if (instance == null) {
            instance = new Pixels();
        }
        return instance;
    }

    @Override
    public Double internalToDoubleFromUnitString(String unitString) {
        try {
            // Dois être un int
            int val = Integer.parseInt(unitString);
            return (double) val;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String toStringValue(Double value) {
        return Integer.toString(value.intValue());
    }
}
