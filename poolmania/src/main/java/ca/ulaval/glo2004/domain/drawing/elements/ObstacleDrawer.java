package ca.ulaval.glo2004.domain.drawing.elements;

import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.elements.Obstacle;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;

import java.awt.*;

public class ObstacleDrawer implements ElementDrawer {
    public static final Color OBSTACLE_COLOR = new Color(72, 37, 10);
    public static final Color OBSTACLE_BORDER_COLOR = new Color(178, 136, 109);
    public static final int OBSTACLE_BORDER_WIDTH = 2;

    @Override
    public void draw(Graphics2D g, Element element) {
        Obstacle obstacle = (Obstacle) element;

        Polygon2D obstaclePolygon = obstacle.getSelectionBox();

        g.setColor(OBSTACLE_COLOR);
        g.fill(obstaclePolygon);

        g.setStroke(new BasicStroke(OBSTACLE_BORDER_WIDTH));
        g.setColor(OBSTACLE_BORDER_COLOR);
        g.draw(obstaclePolygon);
    }
}
