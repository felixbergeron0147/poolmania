package ca.ulaval.glo2004.domain.drawing.elements;

import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.elements.Pocket;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;

import java.awt.*;

public class PocketDrawer implements ElementDrawer {
    public static final Color POCKET_COLOR = Color.black;
    public static final Color POCKET_BORDER_COLOR = new Color(51, 51, 51);
    public static final float[] GRADIENT_VALUES = {0.5f, 1.0f};
    public static final Color[] COLOR_VALUES = {POCKET_COLOR, POCKET_BORDER_COLOR};

    @Override
    public void draw(Graphics2D g, Element element) {
        Pocket pocket = (Pocket) element;

        Polygon2D pocketPolygon = pocket.getSelectionBox();

        RadialGradientPaint gradientPaint = new RadialGradientPaint(pocket.getPos().toPoint2DDouble(), (float) pocket.getDiameter().toPixels() / 2, GRADIENT_VALUES, COLOR_VALUES);
        g.setPaint(gradientPaint);
        g.fill(pocketPolygon);
    }
}
