package ca.ulaval.glo2004.domain.drawing;

import ca.ulaval.glo2004.domain.handlers.GridHandler;

import java.awt.*;

import static ca.ulaval.glo2004.gui.panels.main.DrawingPanel.DRAWING_PANEL_SIZE;

public class GridDrawer {
    private final GridHandler gridHandler;

    public GridDrawer(GridHandler gridHandler) {
        this.gridHandler = gridHandler;
    }

    public GridHandler getGridHandler() {
        return gridHandler;
    }

    public void draw(Graphics2D g) {
        g.setColor(new Color(120, 120, 120, 120));
        g.setStroke(new BasicStroke(1));

        double gridSpace = gridHandler.getSpacing().toPixels();

        for (int x = 0; x < DRAWING_PANEL_SIZE.toPixels(); x += gridSpace) {
            g.drawLine(x, 0, x, (int) DRAWING_PANEL_SIZE.toPixels());
        }

        for (int y = 0; y < DRAWING_PANEL_SIZE.toPixels(); y += gridSpace) {
            g.drawLine(0, y, (int) DRAWING_PANEL_SIZE.toPixels(), y);
        }
    }
}
