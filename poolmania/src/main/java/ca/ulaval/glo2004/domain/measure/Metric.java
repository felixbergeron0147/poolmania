package ca.ulaval.glo2004.domain.measure;

public class Metric extends Unit {
    private static Metric instance = null;

    private Metric() {
        super(Imperial.get().getPixelRatio() / 2.54, "cm"); // 1 in = 2.54 cm => 16/2.54 = 6.299...
    }

    public static Metric get() {
        if (instance == null) {
            instance = new Metric();
        }
        return instance;
    }

    @Override
    public Double internalToDoubleFromUnitString(String unitString) {
        try {
            return Double.parseDouble(unitString);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public String toStringValue(Double value) {
        return String.format("%.2f", value);
    }
}
