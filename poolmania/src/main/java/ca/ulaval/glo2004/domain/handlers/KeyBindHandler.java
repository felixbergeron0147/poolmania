package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.gui.frames.MainWindow;

import javax.swing.*;

public class KeyBindHandler {
    private final MainWindow mainWindow;

    public KeyBindHandler(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void addKeyBind(String actionName, KeyStroke keyStroke, AbstractAction action) {
        ActionMap actionMap = mainWindow.getRootPane().getActionMap();
        InputMap inputMap = mainWindow.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

        inputMap.put(keyStroke, actionName);
        actionMap.put(actionName, action);
    }
}
