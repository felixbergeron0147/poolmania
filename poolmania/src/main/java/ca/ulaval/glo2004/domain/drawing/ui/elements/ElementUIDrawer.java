package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.ui.UIDrawer;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.listeners.action.DeleteSelectedElementAction;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

import javax.swing.*;
import java.awt.*;

public abstract class ElementUIDrawer extends UIDrawer {
    protected final NumberInput xPositionField;
    protected final NumberInput yPositionField;
    private final JButton deleteBtn;
    protected Element elementReference;

    /**
     * Constructeur de la classe ElementUIDrawer
     *
     * @param controller        Contrôleur
     * @param name              Texte qui sera affiché en haut du panneau
     * @param addPositionPanels true pour que les attributs position X et Y soient initialisés sur le panneau, false sinon
     * @param addDeleteButton   true pour ajouter un bouton permettant de supprimer l'élément associé, false sinon
     */
    public ElementUIDrawer(Controller controller, String name, boolean addPositionPanels, boolean addDeleteButton) {
        super(controller, name);

        if (addPositionPanels) {
            xPositionField = new NumberInput(controller, 8, null, false);
            yPositionField = new NumberInput(controller, 8, null, false);
        } else {
            xPositionField = null;
            yPositionField = null;
        }

        if (addDeleteButton) {
            deleteBtn = new JButton("Supprimer");
        } else {
            deleteBtn = null;
        }

        buildUp();
    }

    /**
     * Initialise et mets en place les éléments requis pour la construction du ElementUIDrawer.
     */
    private void buildUp() {
        if (xPositionField != null && yPositionField != null) {
            registerChildComponent(xPositionField);
            content.add(WidgetBuilder.buildLabelInputPanel("Position x", xPositionField));

            registerChildComponent(yPositionField);
            content.add(WidgetBuilder.buildLabelInputPanel("Position y", yPositionField));
        }

        if (deleteBtn != null) {
            JPanel deletePanel = new JPanel();
            deleteBtn.addActionListener(new DeleteSelectedElementAction(controller));
            deletePanel.add(deleteBtn);
            deletePanel.add(Box.createGlue());
            deletePanel.setOpaque(false);
            WidgetBuilder.buildUpButton(deleteBtn, 12, new Color(211, 25, 25), Color.black, new Color(255, 178, 178), new Color(250, 109, 109), Color.black);
            add(deletePanel, BorderLayout.SOUTH);
        }
    }

    /**
     * Modifie l'état du bouton "supprimer" si celui-ci existe.
     *
     * @param enabled Le nouvel état désiré du bouton, true pour activé, false sinon.
     */
    protected void setDeleteBtnEnabled(boolean enabled) {
        if (deleteBtn != null) {
            deleteBtn.setEnabled(enabled);
        }
    }

    /**
     * Appeler quand l'utilisateur a cliqué sur un élément
     *
     * @param element L'élément cliquer
     */
    public void setElementReference(Element element) {
        elementReference = element;

        isInternalUpdate = true;
        updateUIValues();
        isInternalUpdate = false;
    }

    @Override
    protected void updateReferencedObjectValues() {
        if (elementReference == null || xPositionField == null || yPositionField == null) {
            return;
        }

        controller.getUndoRedoHandler().pushEditionChange();

        Unit displayUnit = Controller.getDisplayUnit();
        try {
            elementReference.setXPos(new Quantity<>(displayUnit.toDoubleFromUnitString(xPositionField.getValue()), displayUnit));
            elementReference.setYPos(new Quantity<>(displayUnit.toDoubleFromUnitString(yPositionField.getValue()), displayUnit));
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }
    }

    @Override
    public void updateUIValues() {
        if (xPositionField == null || yPositionField == null) {
            return;
        }

        Unit unitToDisplay = Controller.getDisplayUnit();
        xPositionField.setValue(unitToDisplay.toStringValue(elementReference.getPos().x.asUnit(unitToDisplay)));
        yPositionField.setValue(unitToDisplay.toStringValue(elementReference.getPos().y.asUnit(unitToDisplay)));
    }
}
