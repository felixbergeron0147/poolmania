package ca.ulaval.glo2004.domain.drawing.ui;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.handlers.GridHandler;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

public class GridUIDrawer extends UIDrawer {
    private final GridHandler gridHandler;

    private final NumberInput spaceField;

    public GridUIDrawer(Controller controller) {
        super(controller, "Grille");
        this.gridHandler = controller.getGridHandler();

        spaceField = new NumberInput(controller, 8, 0.1, false);

        buildUp();
    }

    private void buildUp() {
        registerChildComponent(spaceField);
        content.add(WidgetBuilder.buildLabelInputPanel("Espacement", spaceField));
    }

    @Override
    protected void updateReferencedObjectValues() {
        if (this.gridHandler == null) {
            return;
        }

        Unit displayUnit = Controller.getDisplayUnit();

        try {
            this.gridHandler.setSpacing(new Quantity<>(displayUnit.toDoubleFromUnitString(spaceField.getValue()), displayUnit));
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }
    }

    @Override
    public void updateUIValues() {
        Unit displayUnit = Controller.getDisplayUnit();
        spaceField.setValue(displayUnit.toStringValue(gridHandler.getSpacing().asUnit(displayUnit)));
    }
}
