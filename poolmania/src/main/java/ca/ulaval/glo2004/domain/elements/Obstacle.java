package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.ElementType;

import java.awt.geom.Point2D;
import java.util.Vector;

public class Obstacle extends Element {
    private final Quantity<Imperial> width;
    private final Quantity<Imperial> height;

    private double rotation = 0;

    /**
     * Constructeur de l'objet Obstacle
     */
    public Obstacle() {
        super(ElementType.OBSTACLE);
        width = new Quantity<>(12, Imperial.get());
        height = new Quantity<>(12, Imperial.get());
    }

    public Vector<Wall> getCollisionWalls() {
        return BilliardTable.getWallVecFromWallNodeVec(getCorners());
    }

    public Vector<WallNode> getCorners() {
        Vector<WallNode> corners = new Vector<>();
        corners.add(new WallNode(rotatePoint(position.getXPixel() - width.toPixels() / 2, position.getYPixel() - height.toPixels() / 2)));
        corners.add(new WallNode(rotatePoint(position.getXPixel() + width.toPixels() / 2, position.getYPixel() - height.toPixels() / 2)));
        corners.add(new WallNode(rotatePoint(position.getXPixel() + width.toPixels() / 2, position.getYPixel() + height.toPixels() / 2)));
        corners.add(new WallNode(rotatePoint(position.getXPixel() - width.toPixels() / 2, position.getYPixel() + height.toPixels() / 2)));
        return corners;
    }

    private Point2D.Double rotatePoint(double xPixel, double yPixel) {
        double xRot = Math.sin(Math.toRadians(rotation));
        double yRot = Math.cos(Math.toRadians(rotation));

        xPixel -= position.getXPixel();
        yPixel -= position.getYPixel();

        double xnew = xPixel * yRot - yPixel * xRot;
        double ynew = xPixel * xRot + yPixel * yRot;

        xPixel = xnew + position.getXPixel();
        yPixel = ynew + position.getYPixel();
        return new Point2D.Double(xPixel, yPixel);
    }

    @Override
    public Polygon2D getSelectionBox() {
        Polygon2D obstaclePolygon = new Polygon2D();
        for (WallNode wallNode : getCorners()) {
            obstaclePolygon.addPoint((float) wallNode.getPos().getXPixel(), (float) wallNode.getPos().getYPixel());
        }
        return obstaclePolygon;
    }

    public Quantity<Imperial> getWidth() {
        return width;
    }

    public <U extends Unit> void setWidth(Quantity<U> newWidth) {
        width.set(newWidth);
    }

    public Quantity<Imperial> getHeight() {
        return height;
    }

    public <U extends Unit> void setHeight(Quantity<U> newHeight) {
        height.set(newHeight);
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double newRotation) {
        rotation = newRotation;
    }

    /**
     * Constructeur de copie de la classe Obstacle
     *
     * @param obstacle L'Obstacle a copier
     */
    protected Obstacle(Obstacle obstacle) {
        super(obstacle);
        this.width = obstacle.width;
        this.height = obstacle.height;
        this.rotation = obstacle.rotation;
    }

    /**
     * Fait une "deep" copie de Obstacle
     *
     * @return La copie de Obstacle
     */
    @Override
    public Element copy() {
        return new Obstacle(this);
    }
}
