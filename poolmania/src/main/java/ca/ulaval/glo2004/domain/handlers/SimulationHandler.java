package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.ui.SimUIDrawer;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.enums.*;
import ca.ulaval.glo2004.geometry.structure.UnitSpeedVector;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.listeners.action.UpdateTableLoop;

import javax.swing.*;
import java.util.Vector;

public class SimulationHandler {
    public static final double FORCE = 5000;
    private int friction = 70;

    private int reboundLoss = 10;

    private final Controller controller;
    private final MainWindow mainWindow;
    private final PhysicsHandler physicsHandler;

    private final SimUIDrawer simUIDrawer;

    /**
     * Table à ne pas modifier, car elle est une copie de la table
     */
    private BilliardTable originalTable;

    private UpdateTableLoop updateTableLoop;
    private Thread thread;

    private boolean isSimulating = false;

    private double shotAngle = 0;
    private int shotForce = 50; // 1-100

    public SimulationHandler(MainWindow mainWindow, Controller controller) {
        this.controller = controller;
        this.mainWindow = mainWindow;

        simUIDrawer = new SimUIDrawer(controller, this);
        physicsHandler = new PhysicsHandler(controller);
    }

    public boolean canEnterSimulationMode() {
        BilliardTable billiardTable = controller.getBilliardTable();

        Ball whiteBall = billiardTable.getWhiteBall();
        Vector<Element> balls = billiardTable.getElementsOfType(ElementType.BALL);
        if (whiteBall == null) {
            int count = 0;
            for (Element element : balls) {
                if (element.getElementType() == ElementType.WHITE_BALL) {
                    count++;
                }
            }
            JOptionPane.showMessageDialog(mainWindow, "Table à une quantité invalide de balle blanche [" + count + "] \n Il doit avoir une balle blanche en tout temps");
            return false;
        }

        for (Element ball1 : balls) {
            for (Element ball2 : balls) {
                if (ball1 == ball2) {
                    continue;
                }
                if (physicsHandler.areBallsColliding((Ball) ball1, (Ball) ball2)) {
                    JOptionPane.showMessageDialog(mainWindow, "Au moins une balle est superposée sur une autre balle.\n Impossible d'initialiser la simulation");
                    return false;
                }
            }
        }

        // TODO check if over obstacle

        return true;
    }

    public void playShot() {
        BilliardTable billiardTable = controller.getBilliardTable();

        for (Element ball : billiardTable.getElementsOfType(ElementType.BALL)) {
            ((Ball) ball).setUnitSpeedVector(new UnitSpeedVector(0, 0, 0));
        }

        Ball whiteBall = billiardTable.getWhiteBall();
        double shotAngleRad = Math.toRadians(shotAngle);
        UnitSpeedVector whiteBallSpdVector = new UnitSpeedVector(Math.cos(shotAngleRad), Math.sin(shotAngleRad), FORCE * ((double) shotForce / 100));
        whiteBall.setUnitSpeedVector(whiteBallSpdVector);
        controller.getSoundHandler().playSound(AppSound.CUE_HIT);

        updateTableLoop = new UpdateTableLoop(controller, this, billiardTable);

        thread = new Thread(updateTableLoop);
        thread.start();

        isSimulating = true;
        setToIsSimulating(true);
    }

    public void stopSimulation() {
        if (updateTableLoop != null && thread != null) {
            updateTableLoop.stop();
        }
    }

    public double getShotAngle() {
        return shotAngle;
    }

    public void setShotAngle(double shotAngle) {
        this.shotAngle = shotAngle;
    }

    public int getShotForce() {
        return shotForce;
    }

    public void setShotForce(int shotForce) {
        if (shotForce < 1) {
            shotForce = 1;
        } else if (shotForce > 100) {
            shotForce = 100;
        }
        this.shotForce = shotForce;
    }

    public void changeMode(Mode currentMode) {
        switch (currentMode) {
            case EDITION: {
                stopSimulation();
                resetSimulation();
                originalTable = null;
                controller.resetToDefaultTool();
                break;
            }
            case SIMULATION: {
                controller.setCurrentTool(AppToolType.SIMULATION);
                updatePlaceBallUI();
                controller.getRightPanel().setUiDrawer(simUIDrawer);
                originalTable = new BilliardTable(controller.getBilliardTable());
                break;
            }
        }
    }

    public void setToIsSimulating(boolean enabled) {
        updatePlaceBallUI();

        isSimulating = enabled;
        simUIDrawer.enableInputs(!enabled); // If disabled it means the shot is being simulated
    }

    public void updatePlaceBallUI() {
        Ball whiteBall = controller.getBilliardTable().getWhiteBall();
        boolean hasWhiteBall = whiteBall != null;
        simUIDrawer.showPlaceBall(!hasWhiteBall);
        simUIDrawer.enableInputs(hasWhiteBall);
    }

    public PhysicsHandler getPhysicsHandler() {
        return physicsHandler;
    }

    public void resetSimulation() {
        isSimulating = false;
        stopSimulation();
        updateTableLoop = null;
        thread = null;
        controller.setBilliardTable(new BilliardTable(originalTable));
        setToIsSimulating(false);
        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public boolean isSimulating() {
        return isSimulating;
    }

    public int getFriction() {
        return friction;
    }

    public void setFriction(int friction) {
        this.friction = friction;
    }

    public double getFrictionPercent() {
        return (double) friction / 100;
    }

    public int getReboundLoss() {
        return reboundLoss;
    }

    public void setReboundLoss(int reboundLoss) {
        this.reboundLoss = reboundLoss;
    }

    public double getReboundLossPercent() {
        return 1 - (double) reboundLoss / 100;
    }
}
