package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.Position;

import java.awt.geom.Point2D;

public class WallNode extends Element {
    private static final Quantity<Imperial> SELECTION_RADIUS = new Quantity<>(1.5, Imperial.get());

    /**
     * Constructeur de la classe WallNode
     *
     * @param x Position en X du nouveau Wallnode
     * @param y Position en Y du nouveau Wallnode
     */
    public WallNode(double x, double y) {
        super(ElementType.WALL_NODE);
        position.setLocation(new Quantity<>(x, Pixels.get()), new Quantity<>(y, Pixels.get()));
    }

    /**
     * Constructeur de la classe Wallnode
     *
     * @param pos Position du nouveau Wallnode
     */
    public WallNode(Position pos) {
        this();
        position = pos;
    }

    /**
     * Constructeur de copie de la classe Wallnode
     *
     * @param node Wallnode à copier
     */
    public WallNode(WallNode node) {
        this();
        position = new Position(node.position);
    }

    public WallNode(Point2D.Double pos) {
        this();
        position = new Position(pos.getX(), pos.y);
    }

    public WallNode() {
        super(ElementType.WALL_NODE);
    }

    @Override
    public Polygon2D getSelectionBox() {
        return getSquareSelectionPolygon(position.getXPixel(), position.getYPixel(), SELECTION_RADIUS.toPixels());
    }

    @Override
    public Element copy() {
        return new WallNode(this);
    }
}