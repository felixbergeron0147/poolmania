package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.TableDrawer;
import ca.ulaval.glo2004.domain.elements.*;
import ca.ulaval.glo2004.domain.factories.ElementDrawerFactory;
import ca.ulaval.glo2004.domain.factories.ElementSaveLoadFactory;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.enums.FileType;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.listeners.action.SaveProjectAction;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;

import static ca.ulaval.glo2004.enums.FileType.PMT;

public class FileHandler {
    public static final char START_OBJ = '[';
    public static final char END_OBJ = ']';
    public static final String DATA_SEPARATOR = ",";

    private static final String FILE_HEADER = "PoolMania Project";
    private static final String WALL_THICKNESS = "Wall Thickness:";
    private static final String WALL_NODES = "WallNodes:";
    private static final String BALL_DIAMETER = "Ball diameter:";
    private static final String ELEMENTS = "Elements:";
    private static final String DATA_FORMAT = "\t%s" + System.lineSeparator();

    private final MainWindow mainWindow;
    private final Controller controller;

    private final SvgExporter svgExporter;

    private String currentProjectFilePath;

    public FileHandler(MainWindow mainWindow, Controller controller) {
        this.mainWindow = mainWindow;
        this.controller = controller;

        svgExporter = new SvgExporter(controller);
    }

    public void setUp() {
        controller.getKeyBindHandler().addKeyBind("save_project", KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK), new SaveProjectAction(controller, false));
    }

    public void clearCurrentFilePath() {
        currentProjectFilePath = null;
    }

    public BilliardTable loadTableFile() {
        String filePath = chooseLoadFile();
        if (!isFilePathValid(filePath)) {
            return null;
        }

        try {
            BilliardTable table = readReader(new BufferedReader(new FileReader(filePath)));
            if (table != null) {
                currentProjectFilePath = filePath;
                return table;
            }
            return null;
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(mainWindow, "Fichier non trouvable");
            return null;
        }
    }

    public BilliardTable loadTablePreset(String resourceFilePath) {
        try (InputStream resourceAsStream = getClass().getResourceAsStream(resourceFilePath)) {
            if (resourceAsStream != null) {
                return readReader(new BufferedReader(new InputStreamReader(resourceAsStream)));
            }
            throw new RuntimeException("No file found: " + resourceFilePath);
        } catch (IOException | RuntimeException e) {
            JOptionPane.showMessageDialog(mainWindow, "Erreur en ouvrant le fichier prédéfini");
            return null;
        }
    }

    private BilliardTable readReader(BufferedReader reader) {
        try {
            BilliardTable table = new BilliardTable();
            ElementSaveLoadFactory elementSaveLoadFactory = ElementSaveLoadFactory.getInstance();

            String line = reader.readLine();
            if (!line.equals(FILE_HEADER)) {
                throw new RuntimeException("No project file header found");
            }

            line = reader.readLine();
            if (!line.equals(WALL_THICKNESS)) {
                throw new RuntimeException("No wall thickness header found");
            }
            line = reader.readLine();
            line = line.trim();
            Wall.setWallThickness(new Quantity<>(Double.parseDouble(line), Imperial.get()));

            line = reader.readLine();
            if (!line.equals(BALL_DIAMETER)) {
                throw new RuntimeException("No ball diameter header found");
            }
            line = reader.readLine();
            line = line.trim();
            Ball.setDiameter(new Quantity<>(Double.parseDouble(line), Imperial.get()));

            line = reader.readLine();
            if (!line.equals(WALL_NODES)) {
                throw new RuntimeException("No wall nodes header found");
            }
            line = reader.readLine();
            while (!line.equals(ELEMENTS)) {
                table.addWallNode((WallNode) elementSaveLoadFactory.getElementFromDataString(ElementType.WALL_NODE, getLineData(line)));
                line = reader.readLine();
            }

            if (table.getNBWallNodes() < BilliardTable.MIN_NB_TABLE_WALL_NODES) {
                throw new RuntimeException("Invalid number of WallNodes");
            }

            line = reader.readLine();
            while (line != null) {
                String typeString = line.trim();
                typeString = typeString.substring(0, typeString.indexOf(START_OBJ));
                ElementType type = ElementType.values()[Integer.parseInt(typeString)];

                String[] data = getLineData(line);
                table.addElement(elementSaveLoadFactory.getElementFromDataString(type, data));
                line = reader.readLine();
            }

            reader.close();

            return table;
        } catch (IOException | RuntimeException e) {
            JOptionPane.showMessageDialog(mainWindow, "Fichier corrompu ou invalide");
            return null;
        }
    }

    public boolean saveTable(boolean forceAskFilePath) {
        if (!isFilePathValid(currentProjectFilePath) || forceAskFilePath) {
            currentProjectFilePath = chooseSaveFilePath();
            if (!isFilePathValid(currentProjectFilePath)) {
                return false;
            }
        }

        try {
            String folderPath = currentProjectFilePath.substring(0, currentProjectFilePath.lastIndexOf(File.separator));
            File folder = new File(folderPath);
            if (!folder.exists()) {
                throw new RuntimeException("Invalid save location");
            }

            writeTableToFile(currentProjectFilePath);
            return true;
        } catch (RuntimeException e) {
            JOptionPane.showMessageDialog(mainWindow, "Erreur en sauvegardant le projet");
            return false;
        }
    }

    private void writeTableToFile(String filePath) throws RuntimeException {
        try {
            BilliardTable table = controller.getBilliardTable();
            if (table == null) {
                return;
            }

            ElementSaveLoadFactory elementSaveLoadFactory = ElementSaveLoadFactory.getInstance();

            FileWriter fileWriter = new FileWriter(filePath);
            fileWriter.write(FILE_HEADER + System.lineSeparator());

            fileWriter.write(WALL_THICKNESS + System.lineSeparator());
            fileWriter.write(String.format(DATA_FORMAT, Wall.getWallThickness().value()));

            fileWriter.write(BALL_DIAMETER + System.lineSeparator());
            fileWriter.write(String.format(DATA_FORMAT, Ball.getDiameter().value()));

            fileWriter.write(WALL_NODES + System.lineSeparator());
            for (WallNode wallnode : table.getWallNodes()) {
                fileWriter.write(String.format(DATA_FORMAT, elementSaveLoadFactory.getElementStringData(wallnode)));
            }

            fileWriter.write(ELEMENTS + System.lineSeparator());
            for (Element element : table.getElements()) {
                fileWriter.write(String.format(DATA_FORMAT, elementSaveLoadFactory.getElementStringData(element)));
            }

            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException("Unable to save project", e);
        }
    }

    public String chooseLoadFile() {
        JFileChooser fileChooser = createNewFileChooser(PMT);
        fileChooser.setDialogTitle("Ouvrir un projet PoolMania");

        int result = fileChooser.showOpenDialog(mainWindow);

        if (result == JFileChooser.APPROVE_OPTION) {
            File selected = fileChooser.getSelectedFile();
            return selected.getAbsolutePath();
        }

        return null;
    }

    private String chooseSaveFilePath() {
        JFileChooser fileChooser = createNewFileChooser(PMT);
        fileChooser.setDialogTitle("Sauvegarder un projet PoolMania");

        int result = fileChooser.showSaveDialog(mainWindow);

        if (result == JFileChooser.APPROVE_OPTION) {
            File selected = fileChooser.getSelectedFile();

            if (selected.getName().endsWith(PMT.fileExtension)) {
                return selected.getAbsolutePath();
            } else {
                return selected.getAbsolutePath() + PMT.fileExtension;
            }
        }

        return null;
    }

    public String chooseExportFilePath(FileType type) {
        JFileChooser fileChooser = createNewFileChooser(type);
        fileChooser.setDialogTitle("Exporter un projet PoolMania en " + type.fileExtension);

        int result = fileChooser.showSaveDialog(mainWindow);

        if (result == JFileChooser.APPROVE_OPTION) {
            File selected = fileChooser.getSelectedFile();

            if (selected.getName().endsWith(type.fileExtension)) {
                return selected.getAbsolutePath();
            } else {
                return selected.getAbsolutePath() + type.fileExtension;
            }
        }

        return null;
    }

    private boolean isFilePathValid(String filePath) {
        return filePath != null && !filePath.isEmpty();
    }

    private String[] getLineData(String line) {
        line = line.trim();
        line = line.substring(line.indexOf(START_OBJ) + 1);
        line = line.substring(0, line.indexOf(END_OBJ));
        return line.split(DATA_SEPARATOR);
    }

    public void exportAsSvg(String filePath) {
        try {
            FileWriter fileWriter = new FileWriter(filePath);

            fileWriter.write(svgExporter.getData());

            fileWriter.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(mainWindow, "Erreur en exportant le projet en svg");
        }
    }

    public void exportAsJpg(String filePath) {
        int size = (int) DrawingPanel.DRAWING_PANEL_SIZE.toPixels();
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        drawJpg(g2d);
        g2d.dispose();
        try {
            ImageIO.write(image, "jpg", new File(filePath));
        } catch (IOException exp) {
            JOptionPane.showMessageDialog(mainWindow, "Erreur en exportant le projet en jpg");
        }
    }

    private JFileChooser createNewFileChooser(final FileType fileType) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }

                return f.getName().toLowerCase().endsWith(fileType.fileExtension);
            }

            @Override
            public String getDescription() {
                return fileType.getDescription();
            }
        });

        return fileChooser;
    }

    private void drawJpg(Graphics2D g2d) {
        g2d.setColor(Color.white);
        int size = (int) DrawingPanel.DRAWING_PANEL_SIZE.toPixels();
        g2d.fillRect(0, 0, size, size);
        BilliardTable table = controller.getBilliardTable();
        TableDrawer.getInstance().draw(g2d, table);

        for (Element element : table.getElements()) {
            ElementDrawerFactory.getInstance().getElementDrawer(element).draw(g2d, element);
        }

        Image watermark = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/PoolManiaLogo.png"));
        g2d.drawImage(watermark, size / 2 - watermark.getWidth(null) / 2, size - watermark.getHeight(null), null);
    }
}
