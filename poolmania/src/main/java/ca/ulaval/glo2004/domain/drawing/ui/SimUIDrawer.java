package ca.ulaval.glo2004.domain.drawing.ui;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.domain.measure.Metric;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.gui.listeners.action.PlayShotAction;
import ca.ulaval.glo2004.gui.listeners.action.ResetSimulationAction;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimUIDrawer extends UIDrawer {
    private final SimulationHandler simulationHandler;

    private final JLabel forceValueLabel;
    private final JSlider forceSlider;
    private final NumberInput angleField;
    private final JButton playShotBtn;
    private final JButton resetSimulationBtn;
    private final JButton placeWhiteBallBtn;

    private final JSlider frictionSlider;
    private final JLabel frictionValueLabel;
    private final JSlider reboundLossSlider;
    private final JLabel reboundValueLabel;

    public SimUIDrawer(Controller controller, SimulationHandler simulationHandler) {
        super(controller, "Paramètres du coup");

        this.simulationHandler = simulationHandler;

        forceSlider = new JSlider(1, 100);
        angleField = new NumberInput(controller, 8, null, false, Metric.get(), false);
        forceValueLabel = new JLabel();
        playShotBtn = new JButton("Jouer le coup");
        resetSimulationBtn = new JButton("Réinitialiser la simulation");

        placeWhiteBallBtn = new JButton(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/PlaceCueBallIcon.png"))));

        frictionSlider = new JSlider(0, 100);
        frictionValueLabel = new JLabel();
        reboundLossSlider = new JSlider(0, 100);
        reboundValueLabel = new JLabel();

        buildUp();
    }

    public void buildUp() {
        registerChildComponent(forceSlider);
        content.add(WidgetBuilder.buildSliderWithLabel(forceSlider, forceValueLabel, "Force : "));

        content.add(Box.createVerticalStrut(10));

        registerChildComponent(angleField);
        content.add(WidgetBuilder.buildLabelInputPanel("Angle (Degré) :", angleField));

        content.add(Box.createVerticalStrut(20));
        playShotBtn.addActionListener(new PlayShotAction(controller));
        playShotBtn.setFocusPainted(false);
        playShotBtn.setAlignmentX(CENTER_ALIGNMENT);
        WidgetBuilder.buildUpButton(playShotBtn, 14);
        content.add(playShotBtn);

        content.add(Box.createVerticalStrut(30));
        JPanel southPanel = new JPanel();
        southPanel.setOpaque(false);
        resetSimulationBtn.addActionListener(new ResetSimulationAction(controller));
        resetSimulationBtn.setAlignmentX(CENTER_ALIGNMENT);
        resetSimulationBtn.setFocusPainted(false);
        WidgetBuilder.buildUpButton(resetSimulationBtn, 14);
        southPanel.add(resetSimulationBtn);
        content.add(southPanel);

        registerChildComponent(frictionSlider);
        JPanel frictionPanel = WidgetBuilder.buildSliderWithLabel(frictionSlider, frictionValueLabel, "Friction : ");
        frictionPanel.setToolTipText("Garde X% de sa vitesse");
        content.add(frictionPanel);

        registerChildComponent(reboundLossSlider);
        JPanel reboundLossPanel = WidgetBuilder.buildSliderWithLabel(reboundLossSlider, reboundValueLabel, "Perte au rebond");
        reboundLossPanel.setToolTipText("Perte de X% a une collision");
        content.add(reboundLossPanel);

        WidgetBuilder.buildUpButton(placeWhiteBallBtn);
        placeWhiteBallBtn.setToolTipText("Cliquer pour placer la balle blanche");
        placeWhiteBallBtn.setAlignmentX(JButton.CENTER_ALIGNMENT);
        placeWhiteBallBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setCurrentTool(AppToolType.PLACE_WHITE_BALL);
            }
        });
        content.add(Box.createVerticalStrut(30));
        content.add(placeWhiteBallBtn);
        content.add(Box.createVerticalStrut(10));
    }

    @Override
    protected void updateReferencedObjectValues() {
        try {
            simulationHandler.setShotAngle(Metric.get().toDoubleFromUnitString(angleField.getValue()));
            simulationHandler.setShotForce(forceSlider.getValue());
            simulationHandler.setFriction(frictionSlider.getValue());
            simulationHandler.setReboundLoss(reboundLossSlider.getValue());
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
        updateSliderLabels();
    }

    @Override
    public void updateUIValues() {
        angleField.setValue(Metric.get().toStringValue(simulationHandler.getShotAngle()));
        forceSlider.setValue(simulationHandler.getShotForce());
        frictionSlider.setValue(simulationHandler.getFriction());
        reboundLossSlider.setValue(simulationHandler.getReboundLoss());
        updateSliderLabels();
    }

    public void enableInputs(boolean enabled) {
        forceSlider.setEnabled(enabled);
        angleField.getInput().setEnabled(enabled);
        if (enabled && controller.getBilliardTable().getWhiteBall() == null) {
            playShotBtn.setEnabled(false);
        } else {
            playShotBtn.setEnabled(enabled);
        }
    }

    public void showPlaceBall(boolean show) {
        placeWhiteBallBtn.setVisible(show);
    }

    private void updateSliderLabels() {
        String percentFormat = "%d%%";
        forceValueLabel.setText(String.format(percentFormat, simulationHandler.getShotForce()));
        frictionValueLabel.setText(String.format(percentFormat, simulationHandler.getFriction()));
        reboundValueLabel.setText(String.format(percentFormat, simulationHandler.getReboundLoss()));
    }
}
