package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.enums.BallColor;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

import javax.swing.*;

public class BallUIDrawer extends ElementUIDrawer {
    private final NumberInput diameterField;
    private final JComboBox<BallColor> colorComboBox;
    private final JCheckBox whiteBallCheckBox;
    private final JCheckBox strippedCheckBox;

    public BallUIDrawer(Controller controller) {
        super(controller, "Balle", true, true);

        diameterField = new NumberInput(controller, 8, Ball.MIN_BALL_DIAMETER.value(), false);

        BallColor[] values = BallColor.values();
        BallColor[] valuesWithOutWhite = new BallColor[values.length - 1];
        System.arraycopy(values, 0, valuesWithOutWhite, 0, values.length - 1);
        colorComboBox = new JComboBox<>(valuesWithOutWhite);

        whiteBallCheckBox = new JCheckBox();
        strippedCheckBox = new JCheckBox();

        buildUp();
    }

    private void buildUp() {
        registerChildComponent(diameterField);
        content.add(WidgetBuilder.buildLabelInputPanel("Diamètre", diameterField));

        registerChildComponent(colorComboBox);
        content.add(WidgetBuilder.buildLabelInputPanel("Couleur", colorComboBox));

        registerChildComponent(strippedCheckBox);
        strippedCheckBox.setOpaque(false);
        content.add(WidgetBuilder.buildLabelInputPanel("Rayée", strippedCheckBox));

        registerChildComponent(whiteBallCheckBox);
        whiteBallCheckBox.setOpaque(false);
        content.add(WidgetBuilder.buildLabelInputPanel("Boule blanche", whiteBallCheckBox));
    }

    @Override
    protected void updateReferencedObjectValues() {
        super.updateReferencedObjectValues();

        if (elementReference == null) {
            return;
        }

        Ball ball = (Ball) elementReference;
        boolean changedIsWhiteBall = ball.isWhiteBall() != whiteBallCheckBox.isSelected();
        Unit unitToDisplay = Controller.getDisplayUnit();
        try {
            ball.setBallColor((BallColor) colorComboBox.getSelectedItem());
            ball.setStriped(strippedCheckBox.isSelected());
            Ball.setDiameter(new Quantity<>(unitToDisplay.toDoubleFromUnitString(diameterField.getValue()), unitToDisplay));
            ball.setIsWhite(whiteBallCheckBox.isSelected());
            updateWhiteBallFields();
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
        if (changedIsWhiteBall) {
            controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
        }
    }

    @Override
    public void setElementReference(Element element) {
        super.setElementReference(element);

        updateWhiteBallFields();
    }

    @Override
    public void updateUIValues() {
        super.updateUIValues();

        Ball ball = (Ball) elementReference;
        Unit unitToDisplay = Controller.getDisplayUnit();

        diameterField.setValue(unitToDisplay.toStringValue(Ball.getDiameter().asUnit(unitToDisplay)));
        colorComboBox.setSelectedItem(ball.getColor());
        whiteBallCheckBox.setSelected(ball.isWhiteBall());
        strippedCheckBox.setSelected(ball.isStriped());
    }

    private void updateWhiteBallFields() {
        if (((Ball) elementReference).isWhiteBall()) {
            strippedCheckBox.setEnabled(false);
            colorComboBox.setEnabled(false);
        } else {
            strippedCheckBox.setEnabled(true);
            colorComboBox.setEnabled(true);
        }
    }
}
