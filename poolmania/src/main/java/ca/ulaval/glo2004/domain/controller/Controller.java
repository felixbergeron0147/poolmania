package ca.ulaval.glo2004.domain.controller;

import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.handlers.*;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.tools.AppTool;
import ca.ulaval.glo2004.enums.*;
import ca.ulaval.glo2004.gui.frames.MainWindow;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;
import ca.ulaval.glo2004.gui.panels.main.NewProjectPopUp;
import ca.ulaval.glo2004.gui.panels.main.PresetProjectPopUp;
import ca.ulaval.glo2004.gui.panels.main.RightPanel;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Controller {
    private final MainWindow mainWindow;

    private final FileHandler fileHandler;
    private final GridHandler gridHandler;
    private final DrawingPanelViewHandler drawingPanelViewHandler;
    private final SelectionHandler selectionHandler;
    private final SimulationHandler simulationHandler;
    private final CursorHandler cursorHandler;
    private final AppToolsHandler appToolsHandler;
    private final KeyBindHandler keyBindHandler;
    private final SoundHandler soundHandler;
    private final UndoRedoHandler undoRedoHandler;

    private final Map<ControllerObserver, ControllerObserverType> observers;

    private BilliardTable table;
    private Mode currentMode = Mode.EDITION;

    private static Unit displayUnit = Imperial.get();

    public Controller(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        observers = new HashMap<>();
        gridHandler = new GridHandler(this);
        drawingPanelViewHandler = new DrawingPanelViewHandler(this);
        selectionHandler = new SelectionHandler(this);
        simulationHandler = new SimulationHandler(mainWindow, this);
        cursorHandler = new CursorHandler(mainWindow);
        fileHandler = new FileHandler(mainWindow, this);
        appToolsHandler = new AppToolsHandler(this);
        keyBindHandler = new KeyBindHandler(mainWindow);
        soundHandler = new SoundHandler();
        undoRedoHandler = new UndoRedoHandler(this);
    }

    /**
     * Appeler quand tous les éléments de l'application ont ete initialisés pour ne pas avoir de référence nulle
     * Plus utiliser pour faire des KeyBinds
     */
    public void setUp() {
        fileHandler.setUp();
        appToolsHandler.setup();
        drawingPanelViewHandler.setUp();
        undoRedoHandler.setUp();
    }

    public void switchToWelcomePanel() {
        if (!canChangeProject()) {
            return;
        }

        simulationHandler.stopSimulation();

        mainWindow.switchToPanel(ApplicationPanel.WELCOME);
        table = null;
        fileHandler.clearCurrentFilePath();
    }

    public void changeMode(Mode wantedMode) {
        if (currentMode != null && currentMode == wantedMode) {
            return;
        }

        if (wantedMode == Mode.SIMULATION && !simulationHandler.canEnterSimulationMode()) {
            return;
        }

        currentMode = wantedMode;

        selectionHandler.removeSelectedElement();
        simulationHandler.changeMode(currentMode);
        mainWindow.getMainPanel().changeMode(currentMode);
        undoRedoHandler.clearSimStates();

        notifyObservers(ControllerObserverType.DRAWING);
        notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    public void registerObserver(ControllerObserver observer, ControllerObserverType type) {
        observers.put(observer, type);
    }

    public void unregisterObserver(ControllerObserver observer) {
        observers.remove(observer);
    }

    public void notifyObservers(ControllerObserverType typeToNotify) {
        for (Map.Entry<ControllerObserver, ControllerObserverType> entry : observers.entrySet()) {
            if (entry.getValue() == typeToNotify) {
                entry.getKey().notifyListeners();
            }
        }
    }

    public void deleteSelectedElement() {
        Element selectedElement = selectionHandler.getSelectedElement();
        if (selectedElement != null && currentMode == Mode.EDITION) {
            undoRedoHandler.pushEditionChange();
            if (table.deleteElement(selectedElement)) {
                selectionHandler.removeSelectedElement();
                notifyObservers(ControllerObserverType.DRAWING);
                notifyObservers(ControllerObserverType.RIGHT_PANEL);
            }
        }
    }

    public AppTool getCurrentTool() {
        return appToolsHandler.getCurrentTool();
    }

    public void setCurrentTool(AppToolType type) {
        appToolsHandler.setTool(type);

        notifyObservers(ControllerObserverType.DRAWING);
        notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    public static Unit getDisplayUnit() {
        return displayUnit;
    }

    public void saveCurrentProject(boolean forceAskFilePath) {
        fileHandler.saveTable(forceAskFilePath);
    }

    public boolean canChangeProject() {
        if (table != null) {
            int result = JOptionPane.showConfirmDialog(mainWindow, "Voulez vous sauvegarder le projet?", "Projet en cours", JOptionPane.YES_NO_OPTION);
            switch (result) {
                case JOptionPane.YES_OPTION: {
                    return fileHandler.saveTable(false);
                }
                case JOptionPane.NO_OPTION: {
                    return true;
                }
                case JOptionPane.CLOSED_OPTION: {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Démarre un nouveau projet à partir d'un gabarit
     */
    public void createNewPresetTable() {
        if (table == null || canChangeProject()) {
            PresetProjectPopUp presetProjectPopUp = new PresetProjectPopUp(mainWindow);
            boolean wantPresetProject = presetProjectPopUp.ChoosePreset();
            if (!wantPresetProject)
                return;

            loadPreset(presetProjectPopUp.getPreset().getFilePath());
        }
    }

    /**
     * Démarre un nouveau projet avec une table par défaut
     */
    public void createNewProject() {
        if (table == null || canChangeProject()) {
            NewProjectPopUp newProjectPopUp = new NewProjectPopUp(mainWindow);
            boolean wantNewProject = newProjectPopUp.showAndGetAnswer();
            if (!wantNewProject) {
                return;
            }
            table = BilliardTable.createNewTable(newProjectPopUp.getWidth().toPixels(), newProjectPopUp.getHeight());

            fileHandler.clearCurrentFilePath();
            prepareForNewTable(table);
        }
    }

    public void chooseAndLoadProject() {
        if (!canChangeProject()) {
            return;
        }

        BilliardTable loaded = fileHandler.loadTableFile();
        if (loaded == null) {
            return;
        }

        prepareForNewTable(loaded);
    }

    public void loadPreset(String resourceFilePath) {
        if (!canChangeProject()) {
            return;
        }

        BilliardTable loaded = fileHandler.loadTablePreset(resourceFilePath);
        if (loaded == null) {
            return;
        }

        prepareForNewTable(loaded);
    }

    private void prepareForNewTable(BilliardTable loaded) {
        Runnable center = new Runnable() {
            @Override
            public void run() {
                drawingPanelViewHandler.resetZoom();
                drawingPanelViewHandler.centerTable();
            }
        };
        SwingUtilities.invokeLater(center);

        selectionHandler.removeSelectedElement();
        mainWindow.switchToPanel(ApplicationPanel.MAIN);
        changeMode(Mode.EDITION);
        table = loaded;
        undoRedoHandler.resetEditionStates();
        undoRedoHandler.clearSimRedoStates();
        notifyObservers(ControllerObserverType.DRAWING);
        notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    public void setDisplayUnit(Unit unit) {
        displayUnit = unit;
        notifyObservers(ControllerObserverType.DISPLAY_UNIT);
        notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    public void exportProjectAs(FileType fileType) {
        String filePath = fileHandler.chooseExportFilePath(fileType);
        if (filePath == null) {
            return;
        }

        switch (fileType) {
            case JPG: {
                fileHandler.exportAsJpg(filePath);
                break;
            }
            case SVG: {
                fileHandler.exportAsSvg(filePath);
                break;
            }
        }
    }

    public void setBilliardTable(BilliardTable table) {
        this.table = table;
    }

    public void resetToDefaultTool() {
        switch (currentMode) {
            case EDITION: {
                appToolsHandler.setTool(AppToolType.SELECTION);
                break;
            }
            case SIMULATION: {
                appToolsHandler.setTool(AppToolType.SIMULATION);
                break;
            }
        }
    }

    /**
     * Méthode appelée par le PlayShotAction listener
     */
    public void playShot() {
        undoRedoHandler.pushSimState();
        undoRedoHandler.clearSimRedoStates();
        simulationHandler.playShot();
    }

    public void undo() {
        if (currentMode == Mode.SIMULATION && !simulationHandler.isSimulating() && !undoRedoHandler.getSimTableStates().isEmpty()) {
            Vector<Element> vector = undoRedoHandler.popSimState();
            if (vector != null) {
                undoRedoHandler.pushSimRedoState();
                table.removeAllBalls();
                for (Element e : vector) {
                    table.addElement(e);
                }
            }
            notifyObservers(ControllerObserverType.DRAWING);
            simulationHandler.updatePlaceBallUI();
        }

        if (currentMode == Mode.EDITION) {
            undoRedoHandler.undoEditionChange();
            selectionHandler.removeSelectedElement();
            resetToDefaultTool();
            notifyObservers(ControllerObserverType.DRAWING);
            notifyObservers(ControllerObserverType.RIGHT_PANEL);
        }
    }

    public void redo() {
        if (currentMode == Mode.SIMULATION && !simulationHandler.isSimulating() && !undoRedoHandler.getSimRedoTableStates().isEmpty()) {
            Vector<Element> vector = undoRedoHandler.popSimRedoState();
            if (vector != null) {
                undoRedoHandler.pushSimState();
                table.removeAllBalls();
                for (Element e : vector) {
                    table.addElement(e);
                }
            }
            notifyObservers(ControllerObserverType.DRAWING);
            simulationHandler.updatePlaceBallUI();
        }

        if (currentMode == Mode.EDITION) {
            undoRedoHandler.redoEditionChange();
            notifyObservers(ControllerObserverType.DRAWING);
            notifyObservers(ControllerObserverType.RIGHT_PANEL);
        }
    }

    public Mode getCurrentMode() {
        return currentMode;
    }

    public SimulationHandler getSimulationHandler() {
        return simulationHandler;
    }

    public GridHandler getGridHandler() {
        return gridHandler;
    }

    public SelectionHandler getSelectionHandler() {
        return selectionHandler;
    }

    public DrawingPanelViewHandler getDrawingPanelViewHandler() {
        return drawingPanelViewHandler;
    }

    public DrawingPanel getDrawingPanel() {
        return mainWindow.getMainPanel().getDrawingPanel();
    }

    public BilliardTable getBilliardTable() {
        return table;
    }

    public CursorHandler getCursorHandler() {
        return cursorHandler;
    }

    public KeyBindHandler getKeyBindHandler() {
        return keyBindHandler;
    }

    public RightPanel getRightPanel() {
        return mainWindow.getMainPanel().getRightPanel();
    }

    public SoundHandler getSoundHandler() {
        return soundHandler;
    }

    public UndoRedoHandler getUndoRedoHandler() {
        return undoRedoHandler;
    }
}
