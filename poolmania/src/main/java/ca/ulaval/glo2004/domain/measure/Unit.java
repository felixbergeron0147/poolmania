package ca.ulaval.glo2004.domain.measure;

public abstract class Unit {

    protected final double pixelRatio;
    protected final String symbol;

    protected Unit(double pixelRatio, String symbol) {
        this.pixelRatio = pixelRatio;
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getPixelRatio() {
        return pixelRatio;
    }

    /**
     * Convertis une unité en string à l'unité en double
     * Si n'est pas capable de convertir en l'unité essaye de convertir en décimal pour permettre d’écrire en un ou l'autre
     *
     * @param unitString Le string à convertir
     * @return Retourne null si le string envoyé est invalide sinon la valeur en double
     */
    public Double toDoubleFromUnitString(String unitString) {
        Double val = internalToDoubleFromUnitString(unitString);
        if (val == null) {
            try {
                val = Double.parseDouble(unitString);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        return val;
    }

    protected abstract Double internalToDoubleFromUnitString(String unitString);

    public abstract String toStringValue(Double value);
}
