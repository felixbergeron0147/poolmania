package ca.ulaval.glo2004.domain.drawing.elements;

import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;

import java.awt.*;
import java.util.Vector;

public class BallDrawer implements ElementDrawer {
    public static final int BALL_BORDER_WIDTH = 2;
    public static final Color BALL_BORDER_COLOR = Color.black;

    /**
     * Retourne les polygones nécessaires pour une balle
     *
     * @param ball La balle
     * @return Un polygone en cercle ou un polygone en cercle et un polygone pour la ligne
     */
    public static Vector<Polygon2D> getBallPolygons(Ball ball) {
        Vector<Polygon2D> polygons = new Vector<>();

        Polygon2D circle = ball.getSelectionBox();
        int diameter = (int) Ball.getDiameter().toPixels();

        polygons.add(circle);

        if (ball.isStriped()) {
            Polygon2D strip = new Polygon2D();

            double threshold = diameter / 4.0;

            for (int i = 0; i < circle.npoints; i++) {
                float y = circle.ypoints[i] - (float) ball.getPos().toPoint2DDouble().getY();
                if (y < threshold && y > -threshold) {
                    strip.addPoint(circle.xpoints[i], circle.ypoints[i]);
                }
            }
            polygons.add(strip);
        }

        return polygons;
    }

    @Override
    public void draw(Graphics2D g, Element element) {
        Ball ball = (Ball) element;

        Vector<Polygon2D> ballPolygons = getBallPolygons(ball);
        Polygon2D circle = ballPolygons.elementAt(0);

        if (ball.isStriped()) {
            g.setColor(Color.white);
            g.fill(circle);

            g.setColor(ball.getColor().color);
            g.fill(ballPolygons.elementAt(1));
        } else {
            g.setColor(ball.getColor().color);
            g.fill(circle);
        }

        g.setStroke(new BasicStroke(BALL_BORDER_WIDTH));
        g.setColor(BALL_BORDER_COLOR);
        g.draw(circle);
    }
}
