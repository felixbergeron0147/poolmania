package ca.ulaval.glo2004.domain.drawing.elements;

import ca.ulaval.glo2004.domain.elements.Element;

import java.awt.*;

public interface ElementDrawer {
    void draw(Graphics2D g, Element element);
}
