package ca.ulaval.glo2004.domain.measure;

public class Quantity<T extends Unit> {
    private final Unit converter;
    private double quantity;

    public Quantity(double quantity, T unit) {
        this.quantity = quantity;
        converter = unit;
    }

    /**
     * Retourne la quantité dans l'unité de mesure spécifiée à la déclaration
     *
     * @return La valeur de cette classe
     */
    public double value() {
        return quantity;
    }

    /**
     * Équivalent à faire un =
     *
     * @param q   La nouvelle quantité dans l'unité de mesure voulue
     * @param <U> L'unité de mesure de q
     */
    public <U extends Unit> void set(Quantity<U> q) {
        quantity = q.toPixels() / converter.getPixelRatio();
    }

    /**
     * Équivalent à faire un +=
     *
     * @param q   La quantité à ajouté dans l'unité de mesure voulue
     * @param <U> L'unité de mesure de q
     */
    public <U extends Unit> void add(Quantity<U> q) {
        quantity += q.toPixels() / converter.getPixelRatio();
    }

    /**
     * Équivalent à faire un -=
     *
     * @param q   La quantité à soustraire dans l'unité de mesure voulue
     * @param <U> L'unité de mesure de q
     */
    public <U extends Unit> void subtract(Quantity<U> q) {
        quantity -= q.toPixels() / converter.getPixelRatio();
    }

    /**
     * Retourne la valeur de cette variable en unité de mesure de pixel
     *
     * @return La valeur de cette variable en unité de mesure de pixel
     */
    public double toPixels() {
        return quantity * converter.getPixelRatio();
    }

    /**
     * Retourne la valeur de la quantité dans l'unité de mesure voulue
     *
     * @param unitWanted L'unité de mesure voulue
     * @param <U>        Le type d'unité de mesure voulue
     * @return La valeur de la quantité dans l'unité de mesure voulue
     */
    public <U extends Unit> double asUnit(U unitWanted) {
        return toPixels() / unitWanted.getPixelRatio();
    }

    /**
     * Équivalent à faire un *=
     *
     * @param C La quantité ou constante à multiplier dans l'unité de mesure voulue
     */
    public <U extends Unit> void multiply(double C) {
        quantity *= C;
    }

    public <U extends Unit> void divide(double C) {
        quantity /= C;
    }

}
