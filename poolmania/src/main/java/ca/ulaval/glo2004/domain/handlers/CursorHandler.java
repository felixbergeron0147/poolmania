package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.enums.CursorType;
import ca.ulaval.glo2004.gui.frames.MainWindow;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CursorHandler {
    private final MainWindow mainWindow;

    private final Map<CursorType, Cursor> cursors;

    private CursorType currentCursor = CursorType.DEFAULT;

    public CursorHandler(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        cursors = new HashMap<>();

        cursors.put(CursorType.DEFAULT, Cursor.getDefaultCursor());

        createCustomCursor("/images/BreakWallCursor.png", "BreakWallCursor", CursorType.BREAK_WALL);

        createCustomCursor("/images/MoveViewCursor.png", "MoveViewCursor", CursorType.MOVE_VIEW);

        createCustomCursor("/images/TargetCursor.png", "TargetCursor", CursorType.TARGET);

        createCustomCursor("/images/PlaceWhiteBallGood.png", "WhiteBallGoodCursor", CursorType.WHITE_BALL_GOOD);
        createCustomCursor("/images/PlaceWhiteBallBad.png", "WhiteBallBadCursor", CursorType.WHITE_BALL_BAD);

        cursors.put(CursorType.RESIZE_H, Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
        cursors.put(CursorType.RESIZE_V, Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
        cursors.put(CursorType.RESIZE_CORNER_NW, Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
        cursors.put(CursorType.RESIZE_CORNER_NE, Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
    }

    public void setCursor(CursorType type) {
        if (currentCursor == type) {
            return;
        }

        Cursor cursorToSet = cursors.get(type);

        if (cursorToSet != null) {
            mainWindow.setCursor(cursorToSet);
            currentCursor = type;
        }
    }

    private void createCustomCursor(String filePath, String name, CursorType associatedType) {
        try {
            BufferedImage cursorImage = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream(filePath)));
            Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(cursorImage.getWidth() / 2, cursorImage.getHeight() / 2), name);
            cursors.put(associatedType, cursor);
        } catch (IOException | NullPointerException e) {
            throw new RuntimeException("Unable to load " + name + " image", e);
        }
    }
}
