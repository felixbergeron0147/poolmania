package ca.ulaval.glo2004.domain.drawing;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.factories.ElementDrawerFactory;

import java.awt.*;

public class SimulationMainDrawer implements MainDrawer {
    private final Controller controller;

    public SimulationMainDrawer(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void draw(Graphics2D g) {
        BilliardTable table = new BilliardTable(controller.getBilliardTable()); // Copy to be thread safe
        TableDrawer.getInstance().draw(g, table);

        for (Element element : table.getElements()) {
            ElementDrawerFactory.getInstance().getElementDrawer(element).draw(g, element);
        }

        Ball whiteBall = table.getWhiteBall();
        if (!controller.getSimulationHandler().isSimulating() && whiteBall != null) {
            CueDrawer.getInstance().draw(g, controller, whiteBall);
        }
    }
}
