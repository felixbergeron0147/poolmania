package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.enums.AppSound;

import javax.sound.sampled.*;
import java.io.IOException;
import java.util.Objects;

public class SoundHandler {
    public void playSound(AppSound sound) {
        playSound(sound, 1);
    }

    public void playSound(AppSound sound, float volume) {
        if (volume > 1) {
            volume = 1;
        } else if (volume < 0) {
            volume = 0;
        }
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(Objects.requireNonNull(getClass().getResource(sound.getFilePath())));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue(20f * (float) Math.log10(volume));
            clip.start();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            System.out.println("Cannot play sound");
        }
    }
}
