package ca.ulaval.glo2004.domain.drawing;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.tools.AppToolDrawer;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.factories.ElementDrawerFactory;
import ca.ulaval.glo2004.domain.factories.ToolDrawerFactory;

import java.awt.*;

public class EditionMainDrawer implements MainDrawer {
    private final Controller controller;
    private final GridDrawer gridDrawer;

    public EditionMainDrawer(Controller controller) {
        this.controller = controller;

        gridDrawer = new GridDrawer(controller.getGridHandler());
    }

    public void draw(Graphics2D g) {
        TableDrawer.getInstance().draw(g, controller.getBilliardTable());

        for (Element element : controller.getBilliardTable().getElements()) {
            ElementDrawerFactory.getInstance().getElementDrawer(element).draw(g, element);
        }

        AppToolDrawer toolDrawer = ToolDrawerFactory.getInstance().getToolDrawer(controller.getCurrentTool());
        if (toolDrawer != null) {
            toolDrawer.draw(g, controller);
        }

        if (gridDrawer.getGridHandler().snapToGrid()) {
            gridDrawer.draw(g);
        }
    }
}
