package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.factories.ElementFactory;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.MathUtils;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class BilliardTable {
    public static final int MIN_NB_TABLE_WALL_NODES = 3;
    private final Vector<WallNode> wallNodes;
    private final Vector<Element> elements;

    public BilliardTable() {
        wallNodes = new Vector<>();
        elements = new Vector<>();
    }

    public BilliardTable(BilliardTable table) {
        this();

        for (WallNode wallNode : table.wallNodes) {
            wallNodes.add((WallNode) ElementFactory.getInstance().duplicateElement(wallNode));
        }

        for (Element element : table.elements) {
            Element newElement = ElementFactory.getInstance().duplicateElement(element);
            addElement(newElement);
        }
    }

    public static BilliardTable createNewTable(double wallWidthPixels, double nbWalls) {
        final BilliardTable table = new BilliardTable();

        double segmentWidth = Math.PI * 2 / nbWalls;
        double wallRadius = wallWidthPixels / (2 * Math.sin(Math.toRadians(180 / nbWalls)));
        double defaultPocketDiameter = Pocket.DEFAULT_POCKET_DIAMETER_INCHES.toPixels();
        double pocketRadius = wallRadius - defaultPocketDiameter / 2;
        double angle = Math.toRadians(45);

        for (int i = 0; i < nbWalls; ++i) {
            double xPos = Math.cos(angle) * wallRadius + DrawingPanel.DRAWING_PANEL_SIZE.toPixels() / 2;
            double yPos = Math.sin(angle) * wallRadius + DrawingPanel.DRAWING_PANEL_SIZE.toPixels() / 2;
            double pocketXPos = Math.cos(angle) * pocketRadius + DrawingPanel.DRAWING_PANEL_SIZE.toPixels() / 2;
            double pocketYPos = Math.sin(angle) * pocketRadius + DrawingPanel.DRAWING_PANEL_SIZE.toPixels() / 2;

            table.addWallNode(new WallNode(xPos, yPos));
            table.addElement(new Pocket(pocketXPos, pocketYPos));
            angle -= segmentWidth;
        }

        return table;
    }

    /**
     * Supprime un élément si l'élément peut être supprimé
     *
     * @param element L'élément a supprimer
     * @return True si l'élément à été supprimé sinon false
     */
    public boolean deleteElement(Element element) {
        if (!elements.remove(element)) {
            if (element instanceof WallNode && wallNodes.size() > MIN_NB_TABLE_WALL_NODES) {
                wallNodes.remove((WallNode) element);
            } else {
                return false;
            }
        }

        return true;
    }

    public Point2D.Double getCenterPos() {
        Point2D.Double center = new Point2D.Double();

        for (WallNode wallNode : wallNodes) {
            center.x += wallNode.getPos().getXPixel();
            center.y += wallNode.getPos().getYPixel();
        }

        center.x /= wallNodes.size();
        center.y /= wallNodes.size();

        return center;
    }

    public void addElement(Element element) {
        elements.addElement(element);

        // Tri les éléments pour avoir un ordre d'affichage
        Collections.sort(elements, new Comparator<Element>() {
            @Override
            public int compare(Element o1, Element o2) {
                return o1.getElementType().ordinal() - o2.getElementType().ordinal();
            }
        });
    }

    public void addWallNode(WallNode node) {
        wallNodes.add(node);
    }

    public void breakWall(Wall wall, Point2D.Double breakPos) {
        WallNode newNode = new WallNode(wall.getPos());
        wallNodes.insertElementAt(newNode, wallNodes.indexOf(wall.getNode2()));
        if (breakPos != null) {
            newNode.setPosition(breakPos);
        }
    }

    public Vector<Element> getElements() {
        return elements;
    }

    public int getNBWallNodes() {
        return wallNodes.size();
    }

    public Vector<Wall> getExteriorWalls() {
        return getWallVecFromWallNodeVec(wallNodes);
    }

    public static Vector<Wall> getWallVecFromWallNodeVec(Vector<WallNode> v) {
        Vector<Wall> walls = new Vector<>();
        for (int i = 0; i < v.size(); i++) {
            walls.add(new Wall(v.elementAt(i), v.elementAt((i + 1) % v.size())));
        }
        return walls;
    }

    public Rectangle getBoundsRect() {
        Point2D.Double topLeft = new Point2D.Double();
        Point2D.Double bottomRight = new Point2D.Double();

        Point2D.Double centerPos = getCenterPos();

        for (WallNode wallNode : wallNodes) {
            Point2D.Double pos = wallNode.getPos().toPoint2DDouble();
            pos.x -= centerPos.x;
            pos.y -= centerPos.y;

            if (pos.x < topLeft.x) {
                topLeft.x = pos.x;
            }
            if (pos.y < topLeft.y) {
                topLeft.y = pos.y;
            }
            if (pos.x > bottomRight.x) {
                bottomRight.x = pos.x;
            }
            if (pos.y > bottomRight.y) {
                bottomRight.y = pos.y;
            }
        }

        return new Rectangle((int) topLeft.x, (int) topLeft.y, (int) Point2D.distance(topLeft.x, 0, bottomRight.x, 0), (int) Point2D.distance(0, topLeft.y, 0, bottomRight.y));
    }

    public Vector<WallNode> getWallNodes() {
        return wallNodes;
    }

    public Element getElementAtPosition(double x, double y) {
        return getElementAtPosition(x, y, null);
    }

    /**
     * Retourne l'élément à la position donnée
     *
     * @param x      Position x
     * @param y      Position y
     * @param wanted Le type d'élément voulu.  Si null considère tous les éléments.
     * @return L'élément à la position donnée du type voulu
     */
    public Element getElementAtPosition(double x, double y, ElementType wanted) {
        // Reverse to get the last drawn element first
        Collections.reverse(elements);
        for (Element element : elements) {
            if (element.getSelectionBox().contains(x, y) && (wanted == null || element.getElementType() == wanted)) {
                Collections.reverse(elements);
                return element;
            }
        }
        // Reverse again to bring back to original order
        Collections.reverse(elements);

        for (WallNode wallNode : wallNodes) {
            if (wallNode.getSelectionBox().contains(x, y) && (wanted == null || wallNode.getElementType() == wanted)) {
                return wallNode;
            }
        }

        for (Wall wall : getExteriorWalls()) {
            if (wall.getSelectionBox().contains(x, y) && (wanted == null || wall.getElementType() == wanted)) {
                return wall;
            }
        }

        return null;
    }

    public Ball getWhiteBall() {
        Ball whiteBall = null;
        for (Element element : elements) {
            if (element.getElementType() == ElementType.WHITE_BALL) {
                if (whiteBall == null) {
                    whiteBall = (Ball) element;
                } else {
                    // Plus d'une balle blanche
                    return null;
                }
            }
        }

        return whiteBall;
    }

    public void setElementPosition(Point2D.Double newPos, Element e) {
        Point2D.Double initialPos = e.getPos().toPoint2DDouble();

        boolean isAValidPosition = false;

        switch (e.getElementType()) {
            case WALL: {
                if (canMoveWall((Wall) e, newPos)) {
                    isAValidPosition = true;
                }
                break;
            }
            case WALL_NODE: {
                if (canMoveCorner((WallNode) e, newPos)) {
                    isAValidPosition = true;
                }
                break;
            }
            default: {
                isAValidPosition = true;
                break;
            }
        }

        if (isAValidPosition) {
            e.setPosition(newPos);
        } else {
            e.setPosition(initialPos);
        }
    }

    private boolean isSimplePolygon(Vector<Wall> walls)//(WallNode corner,Point2D.Double newPos)
    {
        if (walls.size() < 3)
            return true;

        int size = walls.size();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - 3; j++) {
                //La position du mur actuel + le mur de comparaison actuel + 2 (parceque on regarde seulement les mur non adjacent) % size pour ne pas sortir des dimensions
                int offJ = (i + j + 2) % (size);
                if (walls.elementAt(i).intersectsWithWall(walls.elementAt(offJ))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean canMoveCorner(WallNode corner, Point2D.Double newPos) {
        Vector<WallNode> nodes = getWallNodes();
        nodes.elementAt(nodes.indexOf(corner)).setPosition(newPos);
        Vector<Wall> walls = getWallVecFromWallNodeVec(nodes);

        return isSimplePolygon(walls);
    }

    private boolean canMoveWall(Wall wall, Point2D.Double newPos) {
        Vector<WallNode> nodes = getWallNodes();
        Wall tempWall = new Wall(wall);
        tempWall.setPosition(newPos);
        nodes.elementAt(nodes.indexOf(wall.getNode1())).setPosition(tempWall.getNode1().getPos().toPoint2DDouble());
        nodes.elementAt(nodes.indexOf(wall.getNode2())).setPosition(tempWall.getNode2().getPos().toPoint2DDouble());
        Vector<Wall> walls = getWallVecFromWallNodeVec(nodes);

        return isSimplePolygon(walls);
    }

    public Vector<Element> getElementsOfType(ElementType type) {
        Vector<Element> wantedElements = new Vector<>();
        for (Element element : elements) {
            if (element.getElementType() == type || (type == ElementType.BALL && element.getElementType() == ElementType.WHITE_BALL)) {
                wantedElements.add(element);
            }
        }

        return wantedElements;
    }

    public boolean isPointInTable(double xPixel, double yPixel) {
        return MathUtils.createPolygonFromWallNodes(wallNodes).contains(xPixel, yPixel);
    }

    /**
     * Retire toutes les balles de la table de billard.
     */
    public void removeAllBalls() {
        for (Element e : getElementsOfType(ElementType.BALL)) {
            deleteElement(e);
        }
    }
}
