package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.BallColor;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.structure.UnitSpeedVector;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import java.util.Random;

public class Ball extends Element {
    public static final double BALL_CLAMP_SPEED_PERCENT = 0.005;
    private static final Quantity<Imperial> diameter = new Quantity<>(2.375, Imperial.get());
    public static final Quantity<Imperial> MIN_BALL_DIAMETER = new Quantity<>(0.25, Imperial.get());
    private BallColor color;
    private boolean isWhiteBall;
    private boolean isStriped;

    private UnitSpeedVector spdVector;

    /**
     * Constructeur de la classe Ball
     *
     * @param isWhiteBall True si la Ball est blanche, False sinon
     */
    public Ball(boolean isWhiteBall) {
        super(ElementType.BALL);
        if (isWhiteBall) {
            color = BallColor.WHITE;
        } else {
            color = BallColor.getRandomColor();
            isStriped = new Random().nextBoolean();
        }

        this.isWhiteBall = isWhiteBall;
    }

    /**
     * Constructeur par défaut de la classe Ball
     */
    public Ball() {
        super(ElementType.BALL);
        isWhiteBall = false;
        isStriped = false;
        color = BallColor.getRandomColor();
    }

    public static Quantity<Imperial> getDiameter() {
        return diameter;
    }

    public static <U extends Unit> void setDiameter(Quantity<U> d) {
        if (d.value() > MIN_BALL_DIAMETER.value()) {
            diameter.set(d);
        }
    }

    public void setIsWhite(boolean isWhite) {
        if (isWhite == isWhiteBall) {
            return;
        }

        if (isWhite) {
            color = BallColor.WHITE;
            isStriped = false;
        } else {
            color = BallColor.getRandomColor();
            isStriped = new Random().nextBoolean();
        }

        isWhiteBall = isWhite;
    }

    public boolean isWhiteBall() {
        return isWhiteBall;
    }

    public boolean isStriped() {
        return isStriped;
    }

    public void setStriped(boolean striped) {
        isStriped = striped;
    }

    public void setBallColor(BallColor color) {
        if (!isWhiteBall) {
            this.color = color;
        }
    }

    public BallColor getColor() {
        return color;
    }

    public UnitSpeedVector getSpdVector() {
        return spdVector;
    }

    public void setUnitSpeedVector(UnitSpeedVector speedVector) {
        spdVector = speedVector;
    }

    public void updatePositionAndSpeed(double delta_time, double friction) {
        position.translate(Vec2.multi(spdVector.getMovement(), delta_time));

        spdVector.setSpeed(spdVector.getSpeed() - (spdVector.getSpeed() * friction) * delta_time);

        if (spdVector.getSpeed() < SimulationHandler.FORCE * BALL_CLAMP_SPEED_PERCENT) {
            spdVector.setSpeed(0);
        }
    }

    @Override
    public Polygon2D getSelectionBox() {
        return getCircleSelectionPolygon(position.getXPixel(), position.getYPixel(), diameter.toPixels() / 2, 100);
    }

    @Override
    public ElementType getElementType() {
        return isWhiteBall ? ElementType.WHITE_BALL : ElementType.BALL;
    }

    /**
     * Constructeur de copie de la classe Ball
     *
     * @param ball La Ball copier
     */
    protected Ball(Ball ball) {
        super(ball);
        this.color = ball.color;
        this.isStriped = ball.isStriped;
        this.isWhiteBall = ball.isWhiteBall;
        if (ball.spdVector != null)
            this.spdVector = new UnitSpeedVector(ball.spdVector);
    }

    /**
     * Fait une "deep" copie de Ball
     *
     * @return La copie de Ball
     */
    @Override
    public Element copy() {
        return new Ball(this);
    }
}
