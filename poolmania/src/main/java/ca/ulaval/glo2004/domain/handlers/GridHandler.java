package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.enums.ControllerObserverType;

public class GridHandler {
    private static final double GRID_SNAP_THRESHOLD = 0.2;
    private final Controller controller;
    private boolean snapToGrid = false;
    private final Quantity<Imperial> spacing;

    public GridHandler(Controller controller) {
        this.controller = controller;
        spacing = new Quantity<>(4, Imperial.get());
    }

    public void setSnapToGrid(boolean snapToGrid) {
        this.snapToGrid = snapToGrid;
        controller.notifyObservers(ControllerObserverType.DRAWING);
        controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    public boolean snapToGrid() {
        return snapToGrid;
    }

    public Quantity<Imperial> getSpacing() {
        return spacing;
    }

    public <U extends Unit> void setSpacing(Quantity<U> newSpacing) {
        this.spacing.set(newSpacing);
        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public Double getNearestGridPos(double xOrY) {
        double spacingInPixels = spacing.toPixels();

        double nbGridSpaces = xOrY / spacingInPixels;
        double posXInGridSquare = nbGridSpaces - (int) nbGridSpaces;
        if (posXInGridSquare < GRID_SNAP_THRESHOLD) {
            return (int) nbGridSpaces * spacingInPixels;
        } else if (posXInGridSquare + GRID_SNAP_THRESHOLD > 1) {
            return (int) (nbGridSpaces + 1) * spacingInPixels;
        }

        return null;
    }
}
