package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.gui.panels.main.DrawingPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;

public class DrawingPanelViewHandler {

    private final Controller controller;

    public static final double ZOOM_STEP = 1.1;
    public static final double ZOOM_DEFAULT = 1;

    private double zoom;

    private double topLeftXPos;
    private double topLeftYPos;

    private Point2D.Double mouseMovePos;

    public DrawingPanelViewHandler(Controller controller) {
        this.controller = controller;
        zoom = ZOOM_DEFAULT;
    }

    public void setUp() {
        controller.getKeyBindHandler().addKeyBind("center_table", KeyStroke.getKeyStroke(KeyEvent.VK_C, 0), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                centerTable();
            }
        });
    }

    public double getZoom() {
        return zoom;
    }

    public double getTopLeftXPos() {
        return topLeftXPos;
    }

    public double getTopLeftYPos() {
        return topLeftYPos;
    }

    public void setZoom(int wheelRotation, double mouseXPoint, double mouseYPoint) {
        double zoomedXPos = mouseXPoint / zoom - topLeftXPos;
        double zoomedYPos = mouseYPoint / zoom - topLeftYPos;

        double xDiff = zoomedXPos + topLeftXPos;
        double yDiff = zoomedYPos + topLeftYPos;

        double oldZoom = zoom;

        if (wheelRotation < 0) {
            zoom *= ZOOM_STEP;
        } else if (wheelRotation > 0) {
            zoom /= ZOOM_STEP;
        }

        double zoomRatio = oldZoom / zoom;

        topLeftXPos = xDiff * zoomRatio - zoomedXPos;
        topLeftYPos = yDiff * zoomRatio - zoomedYPos;

        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public void resetZoom() {
        DrawingPanel drawingPanel = controller.getDrawingPanel();
        double drawingPanelHalfWidth = (double) drawingPanel.getWidth() / 2;
        double drawingPanelHalfHeight = (double) drawingPanel.getHeight() / 2;
        topLeftXPos = -(drawingPanelHalfWidth / zoom - topLeftXPos) + drawingPanelHalfWidth;
        topLeftYPos = -(drawingPanelHalfHeight / zoom - topLeftYPos) + drawingPanelHalfHeight;

        this.zoom = ZOOM_DEFAULT;

        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public void centerTable() {
        Point2D.Double centerPos = controller.getBilliardTable().getCenterPos();

        DrawingPanel drawingPanel = controller.getDrawingPanel();

        topLeftXPos = -(centerPos.x - drawingPanel.getWidth() / 2.0 / zoom);
        topLeftYPos = -(centerPos.y - drawingPanel.getHeight() / 2.0 / zoom);

        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public void moveView(double xPixel, double yPixel) {
        double newX = (xPixel - mouseMovePos.x) / zoom;
        double newY = (yPixel - mouseMovePos.y) / zoom;

        topLeftXPos += newX;
        topLeftYPos += newY;

        mouseMovePos.x = xPixel;
        mouseMovePos.y = yPixel;

        controller.notifyObservers(ControllerObserverType.DRAWING);
    }

    public void setMouseMovePos(double xPixel, double yPixel) {
        mouseMovePos = new Point2D.Double(xPixel, yPixel);
    }
}
