package ca.ulaval.glo2004.domain.factories;

import ca.ulaval.glo2004.domain.drawing.elements.BallDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.ElementDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.ObstacleDrawer;
import ca.ulaval.glo2004.domain.drawing.elements.PocketDrawer;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.enums.ElementType;

import java.util.HashMap;
import java.util.Map;

public class ElementDrawerFactory {
    private static ElementDrawerFactory instance = null;

    private final Map<ElementType, ElementDrawer> elementDrawers;

    private ElementDrawerFactory() {
        elementDrawers = new HashMap<>();

        elementDrawers.put(ElementType.POCKET, new PocketDrawer());
        elementDrawers.put(ElementType.OBSTACLE, new ObstacleDrawer());
        BallDrawer ballDrawer = new BallDrawer();
        elementDrawers.put(ElementType.BALL, ballDrawer);
        elementDrawers.put(ElementType.WHITE_BALL, ballDrawer);
    }

    public static ElementDrawerFactory getInstance() {
        if (instance == null) {
            instance = new ElementDrawerFactory();
        }

        return instance;
    }

    public ElementDrawer getElementDrawer(Element element) {
        return elementDrawers.get(element.getElementType());
    }
}
