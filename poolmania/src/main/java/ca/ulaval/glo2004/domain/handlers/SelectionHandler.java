package ca.ulaval.glo2004.domain.handlers;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.Mode;
import ca.ulaval.glo2004.geometry.Position;
import ca.ulaval.glo2004.gui.panels.main.RightPanel;

import java.awt.geom.Point2D;

public class SelectionHandler {
    private final Controller controller;

    private Element selectedElement = null;
    private Element hoveredElement = null;

    private Position mouseSelectedOffset = null;

    public SelectionHandler(Controller controller) {
        this.controller = controller;
    }

    public void updateMouseHoveredPosition(double xPixel, double yPixel) {
        if (controller.getCurrentMode() == Mode.EDITION) {
            Point2D.Double mousePos = processMousePosition(xPixel, yPixel);
            hoveredElement = controller.getBilliardTable().getElementAtPosition(mousePos.getX(), mousePos.getY());
            controller.notifyObservers(ControllerObserverType.DRAWING);
        }
    }

    public void switchSelection(double xPixel, double yPixel) {
        if (controller.getCurrentMode() == Mode.EDITION) {
            Point2D.Double mousePos = processMousePosition(xPixel, yPixel);
            selectedElement = controller.getBilliardTable().getElementAtPosition(mousePos.getX(), mousePos.getY());
            RightPanel rightPanel = controller.getRightPanel();
            if (selectedElement != null) {
                mouseSelectedOffset = new Position(new Quantity<>(mousePos.getX() - selectedElement.getPos().getXPixel(), Pixels.get()), new Quantity<>(mousePos.getY() - selectedElement.getPos().getYPixel(), Pixels.get()));
                rightPanel.setUIDrawerForElement(selectedElement);
            } else {
                rightPanel.setUiDrawer(null);
            }
            controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
            controller.notifyObservers(ControllerObserverType.DRAWING);
        }
    }

    public void updateSelectedPositionFromMouse(double xPixel, double yPixel) {
        if (selectedElement != null && controller.getCurrentMode() == Mode.EDITION) {
            Point2D.Double mousePos = processMousePosition(xPixel, yPixel);
            Point2D.Double newPos = new Point2D.Double(mousePos.getX(), mousePos.getY());
            newPos.x -= mouseSelectedOffset.getXPixel();
            newPos.y -= mouseSelectedOffset.getYPixel();

            GridHandler gridHandler = controller.getGridHandler();
            if (gridHandler.snapToGrid()) {
                Double x = gridHandler.getNearestGridPos(newPos.x);
                if (x != null) {
                    newPos.x = x;
                }
                Double y = gridHandler.getNearestGridPos(newPos.y);
                if (y != null) {
                    newPos.y = y;
                }
            }

            controller.getBilliardTable().setElementPosition(newPos, selectedElement);

            controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
            controller.notifyObservers(ControllerObserverType.DRAWING);
        }
    }

    public void removeSelectedElement() {
        selectedElement = null;
        controller.getRightPanel().setUiDrawer(null);
    }

    public Element getHoveredElement() {
        return hoveredElement;
    }

    public Element getSelectedElement() {
        return selectedElement;
    }

    /**
     * Retourne la position de la souris absolue dans le DrawingPanel
     *
     * @param xPixel La position x en pixel de la souris relatif au DrawingPanel
     * @param yPixel La position y en pixel de la souris relatif au DrawingPanel
     * @return La position de la souris absolue dans le DrawingPanel pour interagir avec les éléments
     */
    public Point2D.Double processMousePosition(double xPixel, double yPixel) {
        DrawingPanelViewHandler drawingPanelViewHandler = controller.getDrawingPanelViewHandler();
        double zoom = drawingPanelViewHandler.getZoom();
        xPixel = xPixel / zoom - drawingPanelViewHandler.getTopLeftXPos();
        yPixel = yPixel / zoom - drawingPanelViewHandler.getTopLeftYPos();
        return new Point2D.Double(xPixel, yPixel);
    }
}
