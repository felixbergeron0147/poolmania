package ca.ulaval.glo2004.domain.factories;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.ui.elements.*;
import ca.ulaval.glo2004.domain.elements.Element;
import ca.ulaval.glo2004.enums.ElementType;

import java.util.HashMap;
import java.util.Map;

public class ElementUIFactory {
    private final Map<ElementType, ElementUIDrawer> elementUIDrawers;

    public ElementUIFactory(Controller controller) {
        elementUIDrawers = new HashMap<>();

        BallUIDrawer ballUIDrawer = new BallUIDrawer(controller);
        elementUIDrawers.put(ElementType.BALL, ballUIDrawer);
        elementUIDrawers.put(ElementType.WHITE_BALL, ballUIDrawer);
        elementUIDrawers.put(ElementType.POCKET, new PocketUIDrawer(controller));
        elementUIDrawers.put(ElementType.OBSTACLE, new ObstacleUIDrawer(controller));
        elementUIDrawers.put(ElementType.WALL_NODE, new WallNodeUIDrawer(controller));
        elementUIDrawers.put(ElementType.WALL, new WallUIDrawer(controller));
    }

    public ElementUIDrawer getElementUIDrawer(Element element) {
        return elementUIDrawers.get(element.getElementType());
    }
}
