package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Pocket;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

public class PocketUIDrawer extends ElementUIDrawer {
    private final NumberInput diameterField;

    public PocketUIDrawer(Controller controller) {
        super(controller, "Poche", true, true);

        diameterField = new NumberInput(controller, 8, Pocket.MINIMUM_POCKET_DIAMETER.value(), false);

        buildUp();
    }

    private void buildUp() {
        registerChildComponent(diameterField);
        content.add(WidgetBuilder.buildLabelInputPanel("Diamètre", diameterField));
    }

    @Override
    protected void updateReferencedObjectValues() {
        super.updateReferencedObjectValues();

        Pocket pocket = (Pocket) elementReference;
        Unit displayUnit = Controller.getDisplayUnit();
        try {
            pocket.setDiameter(new Quantity<>(displayUnit.toDoubleFromUnitString(diameterField.getValue()), displayUnit));
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
    }

    @Override
    public void updateUIValues() {
        super.updateUIValues();

        Pocket pocket = (Pocket) elementReference;

        Unit displayUnit = Controller.getDisplayUnit();
        diameterField.setValue(displayUnit.toStringValue(pocket.getDiameter().asUnit(displayUnit)));
    }
}
