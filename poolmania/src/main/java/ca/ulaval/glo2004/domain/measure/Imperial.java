package ca.ulaval.glo2004.domain.measure;

public class Imperial extends Unit {
    private static Imperial instance = null;

    // Dénominateur que nous allons utiliser comme précision maximale pour le calcul de pouces.
    private static final double PRECISION = 128;
    // Valeur représentant 1 / 128 ième pouce.
    private static final double PRECISION_VALUE = 0.0078125;

    private static final String SPACE = " ";
    private static final String DIVISOR = "/";

    private Imperial() {
        super(16, "pouces"); // 16 pixels = 1 in
    }

    public static Imperial get() {
        if (instance == null) {
            instance = new Imperial();
        }
        return instance;
    }

    /**
     * Trouve le plus grand diviseur commun de deux nombres, récursif.
     *
     * @param a Premier nombre
     * @param b Deuxième nombre
     * @return Double représentant le plus grand diviseur commun.
     */
    private static double greatestCommonDivisor(double a, double b) {
        if (a == 0)
            return b;
        else if (b == 0)
            return a;
        if (a < b)
            return greatestCommonDivisor(a, b % a);
        else
            return greatestCommonDivisor(b, a % b);
    }

    @Override
    public Double internalToDoubleFromUnitString(String unitString) {
        unitString = unitString.trim();
        if (unitString.contains(SPACE)) {
            String[] parts = unitString.split(SPACE);
            try {
                double intVal = Double.parseDouble(parts[0]);

                String[] fraction = parts[1].split(DIVISOR);
                Double numerator = Double.parseDouble(fraction[0]);

                Double denominator = null;
                if (fraction.length >= 2) {
                    denominator = Double.parseDouble(fraction[1]);
                }

                if (denominator == null) {
                    return intVal;
                }

                if (intVal >= 0) {
                    return intVal + numerator / denominator;
                } else {
                    return intVal - numerator / denominator;
                }
            } catch (NumberFormatException e) {
                return null;
            }
        } else if (unitString.contains(DIVISOR)) {
            String[] fraction = unitString.split(DIVISOR);
            try {
                Double numerator = Double.parseDouble(fraction[0]);

                Double denominator = null;
                if (fraction.length >= 2) {
                    denominator = Double.parseDouble(fraction[1]);
                }

                if (denominator == null) {
                    return 0.0;
                }

                return numerator / denominator;
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            try {
                return Double.parseDouble(unitString);
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    @Override
    public String toStringValue(Double value) {
        double decimal = (value - value.intValue());

        double num = Math.round(decimal / PRECISION_VALUE);
        double gcdValue = greatestCommonDivisor(Math.abs(num), PRECISION);

        num = Math.abs(num / gcdValue);
        double denominator = PRECISION / gcdValue;

        if (value.intValue() != 0) {
            if (num != 0) {
                return value.intValue() + SPACE + (int) num + DIVISOR + (int) denominator;
            } else {
                return String.valueOf(value.intValue());
            }
        } else {
            return (value > 0 ? "" : "-") + (int) num + DIVISOR + (int) denominator;
        }
    }
}
