package ca.ulaval.glo2004.domain.drawing;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Ball;
import ca.ulaval.glo2004.domain.handlers.SimulationHandler;
import ca.ulaval.glo2004.geometry.Position;
import ca.ulaval.glo2004.geometry.structure.Vec2;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class CueDrawer {
    private static CueDrawer instance = null;
    private final double cueHeight;
    private final double cueWidth;
    private final BufferedImage cueImage;

    private CueDrawer() {
        try {
            cueImage = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/Cue.png")));
            cueHeight = cueImage.getHeight();
            cueWidth = cueImage.getWidth();
        } catch (IOException | NullPointerException e) {
            throw new RuntimeException("Unable to load Cue image", e);
        }
    }

    public static CueDrawer getInstance() {
        if (instance == null) {
            instance = new CueDrawer();
        }

        return instance;
    }

    public void draw(Graphics2D g, Controller controller, Ball whiteBall) {
        SimulationHandler simulationHandler = controller.getSimulationHandler();
        double angleRadians = Math.toRadians(simulationHandler.getShotAngle());

        Position whiteBallPos = whiteBall.getPos();

        Vec2 angleDir = new Vec2(Math.cos(angleRadians), Math.sin(angleRadians));

        int shotForce = simulationHandler.getShotForce();

        double cuePosX = (whiteBallPos.getXPixel()) - angleDir.getX() * cueWidth;
        double cuePosY = (whiteBallPos.getYPixel()) - angleDir.getY() * cueWidth;

        double ballRadius = Ball.getDiameter().toPixels() / 2;

        AffineTransform rotationTransform = AffineTransform.getRotateInstance(angleRadians, cuePosX, cuePosY);

        AffineTransform transform = g.getTransform();
        AffineTransform originalCopy = g.getTransform();
        transform.concatenate(rotationTransform);

        g.setTransform(transform);

        g.drawImage(cueImage, (int) ((cuePosX - cueHeight / 2) - ballRadius - (cueWidth/2) * shotForce / 100), (int) ((cuePosY - cueHeight / 2)), null);

        g.setTransform(originalCopy);
    }

    public double getCueWidth() {
        return cueWidth;
    }
}
