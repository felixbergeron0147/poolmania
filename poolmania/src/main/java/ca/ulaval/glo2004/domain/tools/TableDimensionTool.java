package ca.ulaval.glo2004.domain.tools;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.drawing.ui.tools.DimensionToolUIDrawer;
import ca.ulaval.glo2004.domain.elements.BilliardTable;
import ca.ulaval.glo2004.domain.elements.WallNode;
import ca.ulaval.glo2004.domain.handlers.CursorHandler;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.enums.AppToolType;
import ca.ulaval.glo2004.enums.ControllerObserverType;
import ca.ulaval.glo2004.enums.CursorType;
import ca.ulaval.glo2004.enums.ResizeBoundPart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.Vector;

public class TableDimensionTool extends AppTool {
    private final Quantity<Imperial> selectionSize = new Quantity<>(1, Imperial.get());

    private final DimensionToolUIDrawer dimensionToolUIDrawer;

    private final Quantity<Imperial> tableWidth;
    private final Quantity<Imperial> tableHeight;

    private ResizeBoundPart resizePart;

    public TableDimensionTool(Controller controller) {
        super(controller, AppToolType.TABLE_DIMENSION);

        tableWidth = new Quantity<>(0, Imperial.get());
        tableHeight = new Quantity<>(0, Imperial.get());

        dimensionToolUIDrawer = new DimensionToolUIDrawer(this, controller);
    }

    @Override
    public void onEnable() {
        updateTableWidthHeight();
        Point mousePos = controller.getDrawingPanel().getMousePosition();
        if (mousePos != null) {
            updateCursor(mousePos.getX(), mousePos.getY());
        }

        controller.getSelectionHandler().removeSelectedElement();
        controller.getRightPanel().setUiDrawer(dimensionToolUIDrawer);
    }

    @Override
    public void onDisable() {
        controller.getCursorHandler().setCursor(CursorType.DEFAULT);
        controller.getRightPanel().setUiDrawer(null);
        controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(e.getX(), e.getY());
        int index = 0;
        for (Rectangle corner : getBoundRectCorners()) {
            if (corner.contains(mousePos)) {
                resizePart = ResizeBoundPart.values()[index];
                controller.getUndoRedoHandler().pushEditionChange();
                return;
            }
            index++;
        }

        for (Rectangle side : getBoundRectSides()) {
            if (side.contains(mousePos)) {
                resizePart = ResizeBoundPart.values()[index];
                controller.getUndoRedoHandler().pushEditionChange();
                return;
            }
            index++;
        }

        resizePart = null;
    }

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {
        if (!SwingUtilities.isLeftMouseButton(e) || resizePart == null) {
            return;
        }

        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(e.getX(), e.getY());

        BilliardTable billiardTable = controller.getBilliardTable();
        Vector<WallNode> wallNodes = billiardTable.getWallNodes();
        Rectangle boundsRect = billiardTable.getBoundsRect();
        Point2D.Double centerPos = billiardTable.getCenterPos();
        boundsRect.x += centerPos.x;
        boundsRect.y += centerPos.y;

        boolean doTop = false;
        boolean doBottom = false;
        boolean doLeft = false;
        boolean doRight = false;

        switch (resizePart) {
            case TOP_LEFT_CORNER: {
                doTop = true;
                doLeft = true;
                break;
            }
            case BOTTOM_RIGHT_CORNER: {
                doBottom = true;
                doRight = true;
                break;
            }
            case BOTTOM_LEFT_CORNER: {
                doBottom = true;
                doLeft = true;
                break;
            }
            case TOP_RIGHT_CORNER: {
                doTop = true;
                doRight = true;
                break;
            }
            case TOP_SIDE: {
                doTop = true;
                break;
            }
            case BOTTOM_SIDE: {
                doBottom = true;
                break;
            }
            case LEFT_SIDE: {
                doLeft = true;
                break;
            }
            case RIGHT_SIDE: {
                doRight = true;
                break;
            }
        }

        if (doTop) {
            double newHeight = boundsRect.getY() - mousePos.getY() + boundsRect.getHeight();
            for (WallNode wallNode : wallNodes) {
                double scale = (boundsRect.getY() - wallNode.getPos().getYPixel() + boundsRect.getHeight()) / boundsRect.getHeight();
                if (newHeight > 1) {
                    wallNode.setYPos(boundsRect.getY() + boundsRect.getHeight() - newHeight * scale);
                }
            }
        } else if (doBottom) {
            setTableHeight(mousePos.getY() - boundsRect.getY());
        }

        if (doLeft) {
            double newWidth = boundsRect.getX() - mousePos.getX() + boundsRect.getWidth();
            for (WallNode wallNode : wallNodes) {
                double scale = (boundsRect.getX() - wallNode.getPos().getXPixel() + boundsRect.getWidth()) / boundsRect.getWidth();
                if (newWidth > 1) {
                    wallNode.setXPos(boundsRect.getX() + boundsRect.getWidth() - newWidth * scale);
                }
            }
        } else if (doRight) {
            setTableWidth(mousePos.getX() - boundsRect.getX());
        }

        updateTableWidthHeight();
        controller.notifyObservers(ControllerObserverType.DRAWING);
        controller.notifyObservers(ControllerObserverType.RIGHT_PANEL);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        updateCursor(e.getX(), e.getY());
    }

    public void updateCursor(double xPixel, double yPixel) {
        CursorHandler cursorHandler = controller.getCursorHandler();
        cursorHandler.setCursor(CursorType.DEFAULT);
        Point2D.Double mousePos = controller.getSelectionHandler().processMousePosition(xPixel, yPixel);

        int index = 0;
        for (Rectangle corner : getBoundRectCorners()) {
            if (corner.contains(mousePos)) {
                if (index < 2) { // Car les deux premiers sont le même
                    cursorHandler.setCursor(CursorType.RESIZE_CORNER_NW);
                } else {
                    cursorHandler.setCursor(CursorType.RESIZE_CORNER_NE);
                }
                return;
            }
            index++;
        }

        index = 0;
        for (Rectangle side : getBoundRectSides()) {
            if (side.contains(mousePos)) {
                if (index < 2) { // Car les deux premiers sont en haut et en bas
                    cursorHandler.setCursor(CursorType.RESIZE_V);
                } else {
                    cursorHandler.setCursor(CursorType.RESIZE_H);
                }
                return;
            }
            index++;
        }
    }

    public Vector<Rectangle> getBoundRectCorners() {
        BilliardTable billiardTable = controller.getBilliardTable();
        Rectangle boundsRect = billiardTable.getBoundsRect();
        Point2D.Double centerPos = billiardTable.getCenterPos();

        Vector<Rectangle> corners = new Vector<>();

        int cornerSelectionSize = (int) selectionSize.toPixels();

        // Top left
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() - cornerSelectionSize / 2), (int) (centerPos.y + boundsRect.getY() - cornerSelectionSize / 2), cornerSelectionSize, cornerSelectionSize));
        // Bottom right
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() + boundsRect.width - cornerSelectionSize / 2), (int) (centerPos.y + boundsRect.getY() + boundsRect.height - cornerSelectionSize / 2), cornerSelectionSize, cornerSelectionSize));

        // Bottom left
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() - cornerSelectionSize / 2), (int) (centerPos.y + boundsRect.getY() + boundsRect.height - cornerSelectionSize / 2), cornerSelectionSize, cornerSelectionSize));
        // Top right
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() + boundsRect.width - cornerSelectionSize / 2), (int) (centerPos.y + boundsRect.getY() - cornerSelectionSize / 2), cornerSelectionSize, cornerSelectionSize));

        return corners;
    }

    public Vector<Rectangle> getBoundRectSides() {
        BilliardTable billiardTable = controller.getBilliardTable();
        Rectangle boundsRect = billiardTable.getBoundsRect();
        Point2D.Double centerPos = billiardTable.getCenterPos();

        Vector<Rectangle> corners = new Vector<>();

        int lineSize = (int) selectionSize.toPixels();

        //Top
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX()), (int) (centerPos.y + boundsRect.getY() - lineSize / 2), boundsRect.width, lineSize));
        // Bottom
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX()), (int) (centerPos.y + boundsRect.getY() + boundsRect.height - lineSize / 2), boundsRect.width, lineSize));

        //Left
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() - lineSize / 2), (int) (centerPos.y + boundsRect.getY()), lineSize, boundsRect.height));
        //Right
        corners.add(new Rectangle((int) (centerPos.x + boundsRect.getX() + boundsRect.width - lineSize / 2), (int) (centerPos.y + boundsRect.getY()), lineSize, boundsRect.height));

        return corners;
    }

    private void updateTableWidthHeight() {
        Rectangle boundsRect = controller.getBilliardTable().getBoundsRect();
        tableWidth.set(new Quantity<>(boundsRect.getWidth(), Pixels.get()));
        tableHeight.set(new Quantity<>(boundsRect.getHeight(), Pixels.get()));
    }

    public Quantity<Imperial> getTableWidth() {
        return tableWidth;
    }

    /**
     * Change la largeur de la table pour le nombre de pixels voulu
     * Le côté initial est celui de gauche dont la largeur va vers la droite
     *
     * @param widthPixels La nouvelle largeur en pixels
     */
    public void setTableWidth(double widthPixels) {
        BilliardTable billiardTable = controller.getBilliardTable();
        Rectangle boundsRect = billiardTable.getBoundsRect();
        Point2D.Double centerPos = billiardTable.getCenterPos();
        boundsRect.x += centerPos.x;
        boundsRect.y += centerPos.y;

        for (WallNode wallNode : billiardTable.getWallNodes()) {
            double scale = (wallNode.getPos().getXPixel() - boundsRect.getX()) / boundsRect.getWidth();
            if (widthPixels > 1) {
                wallNode.setXPos(widthPixels * scale + boundsRect.getX());
            }
        }

        updateTableWidthHeight();
    }

    public Quantity<Imperial> getTableHeight() {
        return tableHeight;
    }

    /**
     * Change la hauteur de la table pour le nombre de pixels voulu
     * Le côté initial est celui du haut dont la hauteur va vers le bas
     *
     * @param heightPixels La nouvelle hauteur en pixels
     */
    public void setTableHeight(double heightPixels) {
        BilliardTable billiardTable = controller.getBilliardTable();
        Rectangle boundsRect = billiardTable.getBoundsRect();
        Point2D.Double centerPos = billiardTable.getCenterPos();
        boundsRect.x += centerPos.x;
        boundsRect.y += centerPos.y;

        for (WallNode wallNode : billiardTable.getWallNodes()) {
            double scale = (wallNode.getPos().getYPixel() - boundsRect.getY()) / boundsRect.getHeight();
            if (heightPixels > 1) {
                wallNode.setYPos(heightPixels * scale + boundsRect.getY());
            }
        }

        updateTableWidthHeight();
    }
}
