package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.ElementType;
import ca.ulaval.glo2004.geometry.Position;

import java.awt.geom.Point2D;

public abstract class Element {
    protected Position position;
    private final ElementType type;

    /**
     * Constructeur de la classe abstraite Element
     */
    public Element(ElementType associatedType) {
        position = new Position();
        type = associatedType;
    }

    /**
     * Génère un polygone représentant un carré autour de l'élément
     *
     * @param xPixel      Position en X pixels de l'élément
     * @param yPixel      Position en y pixels de l'élément
     * @param radiusPixel Le rayon désiré du polygone généré
     * @return Le polygone carré généré
     */
    protected static Polygon2D getSquareSelectionPolygon(double xPixel, double yPixel, double radiusPixel) {
        Polygon2D box = new Polygon2D();
        box.addPoint((float) (xPixel + radiusPixel), (float) (yPixel + radiusPixel));
        box.addPoint((float) (xPixel + radiusPixel), (float) (yPixel - radiusPixel));
        box.addPoint((float) (xPixel - radiusPixel), (float) (yPixel - radiusPixel));
        box.addPoint((float) (xPixel - radiusPixel), (float) (yPixel + radiusPixel));
        return box;
    }

    protected static Polygon2D getCircleSelectionPolygon(double xPixel, double yPixel, double radiusPixel, int nbPoints) {
        Polygon2D circle = new Polygon2D();

        double segmentWidth = Math.PI * 2 / nbPoints;
        double angle = 0;
        for (int i = 0; i < nbPoints; ++i) {
            double xPos = (Math.cos(angle) * radiusPixel) + xPixel;
            double yPos = (Math.sin(angle) * radiusPixel) + yPixel;

            circle.addPoint((float) xPos, (float) yPos);
            angle -= segmentWidth;
        }

        return circle;
    }

    /**
     * Retourne la position de l'élément
     *
     * @return La position de l'élément, de type Position
     */
    public Position getPos() {
        return position;
    }

    /**
     * Modifie la position de l'élément
     *
     * @param position la nouvelle position désiré de l'élément, de type Point2D.Double
     */
    public void setPosition(Point2D.Double position) {
        if (position.x > 0 && position.y > 0) {
            this.position.setLocation(new Quantity<>(position.x, Pixels.get()), new Quantity<>(position.y, Pixels.get()));
        }
    }

    public void setPosition(Position position) {
        this.position.setLocation(position.x, position.y);
    }

    public <U extends Unit> void setXPos(Quantity<U> xPos) {
        if (xPos.toPixels() > 0) {
            position.setLocation(xPos, position.y);
        }
    }

    public void setXPos(double xPosPixel) {
        if (xPosPixel > 0) {
            position.setLocation(new Quantity<>(xPosPixel, Pixels.get()), position.y);
        }
    }

    public void setYPos(double yPosPixel) {
        if (yPosPixel > 0) {
            position.setLocation(position.x, new Quantity<>(yPosPixel, Pixels.get()));
        }
    }

    public <U extends Unit> void setYPos(Quantity<U> yPos) {
        if (yPos.toPixels() > 0) {
            position.setLocation(position.x, yPos);
        }
    }

    public abstract Polygon2D getSelectionBox();

    public ElementType getElementType() {
        return type;
    }

    /**
     * Constructeur de copie de Element
     *
     * @param element L'Element a copier
     */
    protected Element(Element element) {
        this.position = new Position(element.position.x, element.position.y);
        this.type = element.type;
    }

    /**
     * Méthode abstraite de copie
     *
     * @return L'Element copié
     */
    public abstract Element copy();
}
