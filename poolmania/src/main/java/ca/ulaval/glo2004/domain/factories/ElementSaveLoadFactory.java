package ca.ulaval.glo2004.domain.factories;

import ca.ulaval.glo2004.domain.elements.*;
import ca.ulaval.glo2004.domain.handlers.FileHandler;
import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.enums.BallColor;
import ca.ulaval.glo2004.enums.ElementType;

import static ca.ulaval.glo2004.domain.handlers.FileHandler.DATA_SEPARATOR;

public class ElementSaveLoadFactory {
    private static ElementSaveLoadFactory instance = null;

    private ElementSaveLoadFactory() {}

    public static ElementSaveLoadFactory getInstance() {
        if (instance == null) {
            instance = new ElementSaveLoadFactory();
        }

        return instance;
    }

    public String getElementStringData(Element element) {
        switch (element.getElementType()) {
            case WALL_NODE: {
                // Pas de nouvelle donnée nécessaire
                return getBaseDataString(element, null);
            }
            case POCKET: {
                // diameter
                String POCKET_DATA_FORMAT = "%f";
                Pocket pocket = (Pocket) element;
                return getBaseDataString(element, String.format(POCKET_DATA_FORMAT, pocket.getDiameter().value()));
            }
            case OBSTACLE: {
                // width,height,rotation
                String OBSTACLE_DATA_FORMAT = "%f" + DATA_SEPARATOR + "%f" + DATA_SEPARATOR + "%f";
                Obstacle obstacle = (Obstacle) element;
                return getBaseDataString(element, String.format(OBSTACLE_DATA_FORMAT, obstacle.getWidth().value(), obstacle.getHeight().value(), obstacle.getRotation()));
            }
            case WHITE_BALL:
            case BALL: {
                // color,isWhiteBall,isStripped
                String BALL_DATA_FORMAT = "%d" + DATA_SEPARATOR + "%b" + DATA_SEPARATOR + "%b";
                Ball ball = (Ball) element;
                return getBaseDataString(element, String.format(BALL_DATA_FORMAT, ball.getColor().ordinal(), ball.isWhiteBall(), ball.isStriped()));
            }
        }

        throw new RuntimeException("Unknown savable element type");
    }

    public Element getElementFromDataString(ElementType type, String[] data) {
        try {
            switch (type) {
                case WALL_NODE:
                    WallNode wallNode = new WallNode();
                    setBaseData(wallNode, data);
                    return wallNode;
                case POCKET:
                    Pocket pocket = new Pocket();
                    setBaseData(pocket, data);
                    pocket.setDiameter(new Quantity<>(Double.parseDouble(data[2]), Imperial.get()));
                    return pocket;
                case OBSTACLE:
                    Obstacle obstacle = new Obstacle();
                    setBaseData(obstacle, data);
                    obstacle.setWidth(new Quantity<>(Double.parseDouble(data[2]), Imperial.get()));
                    obstacle.setHeight(new Quantity<>(Double.parseDouble(data[3]), Imperial.get()));
                    obstacle.setRotation(Double.parseDouble(data[4]));
                    return obstacle;
                case WHITE_BALL:
                case BALL: {
                    Ball ball = new Ball();
                    setBaseData(ball, data);
                    ball.setBallColor(BallColor.values()[Integer.parseInt(data[2])]);
                    ball.setIsWhite(Boolean.parseBoolean(data[3]));
                    ball.setStriped(Boolean.parseBoolean(data[4]));
                    return ball;
                }
            }
            throw new RuntimeException("Unknown loadable element type");
        } catch (NumberFormatException e) {
            throw new RuntimeException("Error loading element", e);
        }
    }

    private String getBaseDataString(Element element, String superClassData) {
        // ElementType[posX,posY,superClassData]
        // ou
        // ElementType[posX,posY]
        String HAS_MORE_DATA_FORMAT = "%d" + FileHandler.START_OBJ + "%f" + DATA_SEPARATOR + "%f" + DATA_SEPARATOR + "%s" + FileHandler.END_OBJ;
        String BASE_DATA_FORMAT = "%d" + FileHandler.START_OBJ + "%f" + DATA_SEPARATOR + "%f" + FileHandler.END_OBJ;

        if (superClassData != null && !superClassData.isEmpty()) {
            return String.format(HAS_MORE_DATA_FORMAT, element.getElementType().ordinal(), element.getPos().getXValue(), element.getPos().getYValue(), superClassData);
        } else {
            return String.format(BASE_DATA_FORMAT, element.getElementType().ordinal(), element.getPos().getXValue(), element.getPos().getYValue());
        }
    }

    private void setBaseData(Element element, String[] data) {
        // ElementType[posX,posY]
        element.getPos().x.set(new Quantity<>(Double.parseDouble(data[0]), Imperial.get()));
        element.getPos().y.set(new Quantity<Unit>(Double.parseDouble(data[1]), Imperial.get()));
    }
}
