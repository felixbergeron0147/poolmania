package ca.ulaval.glo2004.domain.elements;

import ca.ulaval.glo2004.domain.measure.Imperial;
import ca.ulaval.glo2004.domain.measure.Pixels;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.domain.polygon.Polygon2D;
import ca.ulaval.glo2004.enums.ElementType;

public class Pocket extends Element {
    public final static Quantity<Imperial> DEFAULT_POCKET_DIAMETER_INCHES = new Quantity<>(4, Imperial.get());
    public final static Quantity<Imperial> MINIMUM_POCKET_DIAMETER = new Quantity<>(0.25, Imperial.get());

    private final Quantity<Imperial> diameter;

    public Pocket() {
        super(ElementType.POCKET);
        diameter = new Quantity<>(DEFAULT_POCKET_DIAMETER_INCHES.value(), Imperial.get());
    }

    public Pocket(double xPixel, double yPixel) {
        this();
        position.setLocation(new Quantity<>(xPixel, Pixels.get()), new Quantity<>(yPixel, Pixels.get()));
    }

    @Override
    public Polygon2D getSelectionBox() {
        return getCircleSelectionPolygon(position.getXPixel(), position.getYPixel(), diameter.toPixels() / 2, 70);
    }

    public Quantity<Imperial> getDiameter() {
        return diameter;
    }

    public <U extends Unit> void setDiameter(Quantity<U> d) {
        if (d.value() > MINIMUM_POCKET_DIAMETER.value()) {
            diameter.set(d);
        }
    }

    /**
     * Constructeur de copie de la classe Pocket
     *
     * @param pocket La Pocket a copier
     */
    protected Pocket(Pocket pocket) {
        super(pocket);
        this.diameter = pocket.diameter;
    }

    /**
     * Fait une "deep" copie de Pocket
     *
     * @return La copie de Pocket
     */
    @Override
    public Element copy() {
        return null;
    }
}
