package ca.ulaval.glo2004.domain.controller;

/**
 * Classe qui veut être notifiée quand un élément de l'application doit être réaffiché
 */
public interface ControllerObserver {
    void notifyListeners();
}
