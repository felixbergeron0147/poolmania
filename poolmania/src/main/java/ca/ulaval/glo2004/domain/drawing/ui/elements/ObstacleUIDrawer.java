package ca.ulaval.glo2004.domain.drawing.ui.elements;

import ca.ulaval.glo2004.domain.controller.Controller;
import ca.ulaval.glo2004.domain.elements.Obstacle;
import ca.ulaval.glo2004.domain.measure.Metric;
import ca.ulaval.glo2004.domain.measure.Quantity;
import ca.ulaval.glo2004.domain.measure.Unit;
import ca.ulaval.glo2004.gui.widgets.NumberInput;
import ca.ulaval.glo2004.gui.widgets.WidgetBuilder;

import javax.swing.*;

public class ObstacleUIDrawer extends ElementUIDrawer {
    private final NumberInput widthInput;
    private final NumberInput heightInput;
    private final JLabel rotationLabel;
    private final JSlider rotationSlider;

    public ObstacleUIDrawer(Controller controller) {
        super(controller, "Obstacle", true, true);

        widthInput = new NumberInput(controller, 8, 0.0, false, null, true);
        heightInput = new NumberInput(controller, 8, 0.0, false, null, true);
        rotationLabel = new JLabel();
        rotationSlider = new JSlider(0,360, 0);

        buildUp();
    }

    private void buildUp() {
        registerChildComponent(widthInput);
        content.add(WidgetBuilder.buildLabelInputPanel("Largeur", widthInput));

        registerChildComponent(heightInput);
        content.add(WidgetBuilder.buildLabelInputPanel("Hauteur", heightInput));

        registerChildComponent(rotationSlider);
        content.add(WidgetBuilder.buildSliderWithLabel(rotationSlider, rotationLabel, "Rotation : "));
    }

    @Override
    protected void updateReferencedObjectValues() {
        super.updateReferencedObjectValues();

        Unit displayUnit = Controller.getDisplayUnit();
        Obstacle obstacle = (Obstacle) elementReference;
        try {
            obstacle.setWidth(new Quantity<>(displayUnit.toDoubleFromUnitString(widthInput.getValue()), displayUnit));
            obstacle.setHeight(new Quantity<>(displayUnit.toDoubleFromUnitString(heightInput.getValue()), displayUnit));
            obstacle.setRotation(rotationSlider.getValue());
        } catch (NumberFormatException | NullPointerException e) {
            // Rien a faire
        }

        notifyForUpdatedValues();
    }

    @Override
    public void updateUIValues() {
        super.updateUIValues();

        Unit displayUnit = Controller.getDisplayUnit();
        Obstacle obstacle = (Obstacle) elementReference;
        widthInput.setValue(displayUnit.toStringValue(obstacle.getWidth().asUnit(displayUnit)));
        heightInput.setValue(displayUnit.toStringValue(obstacle.getHeight().asUnit(displayUnit)));
        rotationSlider.setValue((int)obstacle.getRotation());
    }
}
